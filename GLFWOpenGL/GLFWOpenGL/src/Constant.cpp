#include "PCH.h"
#include "Constant.h"
#include "Shader/Shader.h"
#include "Materials/DefaultMaterial.h"

// Shaders
const char * const Constants::Shaders::DefaultShader::GetVertexShader()
{
	return "..\\GLFWOpenGL\\Shaders\\coreTextureShadows.vs";
}

const char * const Constants::Shaders::DefaultShader::GetFragmentShader()
{
	return "..\\GLFWOpenGL\\Shaders\\coreTextureShadows.frag";
}

const char * const Constants::Shaders::DefaultShadowShader::GetVertexShader()
{
	return "..\\GLFWOpenGL\\Shaders\\coreShadows.vs";
}

const char * const Constants::Shaders::DefaultShadowShader::GetFragmentShader()
{
	return "..\\GLFWOpenGL\\Shaders\\coreShadows.frag";
}

Graphics::Shader* const Constants::Shaders::GetDefaultShader()
{
	static Graphics::Shader defaultShader = Graphics::Shader(Constants::Shaders::DefaultShader::GetVertexShader(), Constants::Shaders::DefaultShader::GetFragmentShader());
	return &defaultShader;
}
Graphics::Shader* const Constants::Shaders::GetDefaultShadowShader()
{
	static Graphics::Shader shadowShader = Graphics::Shader(Constants::Shaders::DefaultShadowShader::GetVertexShader(), Constants::Shaders::DefaultShadowShader::GetFragmentShader());
	return &shadowShader;
}

// Materials
Graphics::DefaultMaterial* const Constants::Materials::GetDefaultMaterial()
{
	static Graphics::DefaultMaterial defaultMaterial = Graphics::DefaultMaterial();
	return &defaultMaterial;
}

Graphics::Material* const Constants::Materials::GetShadowDefaultMaterial()
{
	static Graphics::Material defaultShadowMaterial = Graphics::Material(Constants::Shaders::GetDefaultShadowShader());
	return &defaultShadowMaterial;
}

// Framebuffers
Graphics::ShadowFramebuffer * const Constants::Framebuffers::GetDefaultShadowFramebuffer(GLuint width, GLuint height)
{
	static Graphics::ShadowFramebuffer defaultShadowFramebuffer = Graphics::ShadowFramebuffer(width, height);
	return &defaultShadowFramebuffer;
}

Cube * const Constants::Meshes::GetCube()
{
	static Cube cube = Cube();
	return &cube;
}

Sphere * const Constants::Meshes::GetSphere()
{
	static Sphere sphere = Sphere();
	return &sphere;
}

Torus * const Constants::Meshes::GetTorus()
{
	static Torus torus = Torus();
	return &torus;
}
