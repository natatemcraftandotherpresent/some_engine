#pragma once
#include "PCH.h"
#include "Light.h"

class GRAPHICS_API DirectionLight : public Light
{
public:
	DirectionLight() = default;
	DirectionLight(const glm::vec3& direction, glm::vec4& ambientColor, const glm::vec4& diffuse, const glm::vec4& specular);
	
	inline static glm::vec3* const GetDirection() { return &direction; }
	void Create(const glm::vec3& direction, const glm::vec4& ambientColor, const glm::vec4& diffuse, const glm::vec4& specular);
private:
	static glm::vec3 direction;
};