#pragma once
#include "PCH.h"
#include "Object/WorldObject.h"
#include "Core.h"

class GRAPHICS_API Light : public WorldObject
{
protected:
	Light() = default;

	struct GRAPHICS_API LightSourceParameters
	{
		glm::vec4 ambient;
		glm::vec4 Diffuse;
		glm::vec4 Specular;
		glm::vec4 Position;
		glm::vec4 HalfVector;
		glm::vec3 Direction;
		float SpotExponent;
		float SpotCutoff;
		float SpotCosCutoff;
	};

	LightSourceParameters lightParameters;
};