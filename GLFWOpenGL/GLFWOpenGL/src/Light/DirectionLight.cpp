#include "PCH.h"
#include "DirectionLight.h"
#include "glm\glm.hpp"

glm::vec3 DirectionLight::direction;

DirectionLight::DirectionLight(const glm::vec3 & direction, glm::vec4 & ambientColor, const glm::vec4 & diffuse, const glm::vec4 & specular)
{
	this->direction = direction;
	lightParameters.Direction = direction;
	lightParameters.ambient = ambientColor;
	lightParameters.Diffuse = diffuse;
	lightParameters.Specular = specular;
	lightParameters.Direction = direction;
}

void DirectionLight::Create(const glm::vec3 & direction, const glm::vec4 & ambientColor, const glm::vec4 & diffuse, const glm::vec4 & specular)
{
	this->direction = direction;
	lightParameters.Direction = direction;
	lightParameters.ambient = ambientColor;
	lightParameters.Diffuse = diffuse;
	lightParameters.Specular = specular;
	lightParameters.Direction = direction;
}
