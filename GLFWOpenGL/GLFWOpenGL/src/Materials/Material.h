#pragma once
#include "PCH.h"
#include "Shader\Shader.h"
#include "Shader\Uniforms.h"
#include "Core.h"

namespace Graphics
{
	enum class GRAPHICS_API MaterialShadows
	{
		Default,
		No
	};

	class GRAPHICS_API Material
	{
	public:
		Material(Graphics::Shader* shader) { this->shader = shader; materialShadows = MaterialShadows::Default; }
		virtual inline void Use() const
		{
			shader->UseShader();
		}

		inline const Graphics::Shader* const GetShader() const { return shader; }

		inline const MaterialShadows* const GetMaterialShadowsInfo() const { return &materialShadows; }

		inline void SetMaterialShadows(MaterialShadows shadowParm) { materialShadows = shadowParm; }

		static Material* const GetDefaultMaterial();

		inline void SetShader(Shader* shader) { this->shader = shader; }

	private:
		Graphics::Shader* shader;
		MaterialShadows materialShadows;
	};
}