#include "PCH.h"
#include "DefaultMaterial.h"
#include "Shader\Uniforms.h"
#include "Constant.h"

namespace Graphics
{
	DefaultMaterial::DefaultMaterial() : Material(Constants::Shaders::GetDefaultShader())
	{

	}
	void DefaultMaterial::Use() const
	{
		Material::Use();
		SetUniform3f(GetShader()->GetShaderID(), AddLightParameter("directionLight", Direction), -2.0f, 4.0f, -1.0f);
		SetUniform3f(GetShader()->GetShaderID(), "cameraPosition", 0, -2, -10);
		SetUniform4f(GetShader()->GetShaderID(), AddLightParameter("directionLight", Ambient), 1, 0, 0, 1.0f);
		SetUniform4f(GetShader()->GetShaderID(), AddLightParameter("directionLight", Diffuse), 0, 0, 1, 1);
		SetUniform4f(GetShader()->GetShaderID(), AddLightParameter("directionLight", Specular), 1, 1.0, 1.0, 1);
		SetUniform4f(GetShader()->GetShaderID(), AddMaterialParameter("material", Ambient), 1, 0, 0, 1);
		SetUniform4f(GetShader()->GetShaderID(), AddMaterialParameter("material", Diffuse), 0, 0, 1, 1);
		SetUniform4f(GetShader()->GetShaderID(), AddMaterialParameter("material", Specular), 1, 0.1, 0.2, 1);
		SetUniform1f(GetShader()->GetShaderID(), AddMaterialParameter("material", Shininess), 8);
	}
}
