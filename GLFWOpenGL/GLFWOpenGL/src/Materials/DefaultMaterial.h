#pragma once
#include "Material.h"

namespace Graphics
{
	class GRAPHICS_API DefaultMaterial : public Material
	{
	public:
		DefaultMaterial();

		virtual void Use() const override;
	};
}
