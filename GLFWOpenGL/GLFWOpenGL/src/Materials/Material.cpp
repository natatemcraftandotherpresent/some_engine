#include "PCH.h"
#include "Material.h"
#include "Constant.h"

namespace Graphics
{
	Material * const Material::GetDefaultMaterial()
	{
		return Constants::Materials::GetDefaultMaterial();
	}
}
