#pragma once
#ifndef RENDER_CORE
#define RENDER_CORE

#ifdef EC_PLATFORM_WINDOWS
	#ifdef RD_EXPORT
		#define GRAPHICS_API __declspec(dllexport)
	#else
		#define GRAPHICS_API __declspec(dllimport)
	#endif
#endif

#endif