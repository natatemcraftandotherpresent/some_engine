#pragma once
#ifndef RENDERING
#define RENDERING
#include "Core.h"
#include "Window/WindowsWindow.h"
namespace Graphics
{
	class GRAPHICS_API Rendering
	{
	public:
		Rendering(unsigned screenWidth, unsigned screenHeight);
		bool Update();

		const Window* GetCurrentWindow() const { return &window; }
	private:
		void StartRender(unsigned screenWidth, unsigned screenHeight);

	private:
#ifdef EC_PLATFORM_WINDOWS
		Graphics::WindowsWindow window;
#endif // EC_PLATFORM_WINDOWS
	};
}
#endif