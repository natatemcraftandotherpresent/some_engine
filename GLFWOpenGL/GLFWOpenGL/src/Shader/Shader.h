#pragma once
#ifndef GRAPHICS_SHADER
#define GRAPHICS_SHADER

#include "PCH.h"
#include "Core.h"
namespace Graphics
{
	class GRAPHICS_API Shader
	{
	public:
		Shader(const GLchar* vertexShaderSource, const GLchar* fragmentShaderSource);
		void inline UseShader() const
		{
			glUseProgram(program);
		}
		GLuint inline GetShaderID() const { return program; }
		void Compile(const GLchar* vertexShaderSource, const GLchar* fragmentShaderSource);
		~Shader();

		static Shader* const GetDefaultShader();
	private:
		void Clear();
		std::string ReadSourceFromFile(const GLchar* fileDir);
		GLuint program;

	};
}

#endif
