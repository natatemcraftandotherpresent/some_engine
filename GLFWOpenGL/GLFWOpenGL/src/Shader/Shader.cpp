#include "PCH.h"
#include "Shader.h"
#include "Constant.h"
#include "InitOpengl.h"

namespace Graphics
{

	Shader::Shader(const GLchar* vertexShaderSource, const GLchar* fragmentShaderSource)
	{
		Compile(ReadSourceFromFile(vertexShaderSource).c_str(), ReadSourceFromFile(fragmentShaderSource).c_str());
	}

	std::string Shader::ReadSourceFromFile(const GLchar* fileDir)
	{
		std::string content;

		std::ifstream filestream(fileDir, std::ios::in);

		if (!filestream.is_open())
		{
			std::cerr << "Can't open the file" << std::endl;
			return nullptr;
		}
		std::string line = "";
		while (!filestream.eof())
		{
			std::getline(filestream, line);
			content.append(line + '\n');
		}

		return content;
	}

	void Shader::Compile(const GLchar* vertexShaderSource, const GLchar* fragmentShaderSource)
	{
		GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexShader, 1, &vertexShaderSource, nullptr);
		glCompileShader(vertexShader);

		GLint succsess;
		GLchar info[512];

		glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &succsess);
		if (!succsess)
		{
			glGetShaderInfoLog(vertexShader, 512, nullptr, info);
			std::cerr << "ERROR: Can't compile vertex shader. " << info << std::endl;
			program = 0;
			return;
		}

		GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentShader, 1, &fragmentShaderSource, nullptr);
		glCompileShader(fragmentShader);

		glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &succsess);
		if (!succsess)
		{
			glGetShaderInfoLog(fragmentShader, 512, nullptr, info);
			std::cerr << "ERROR: Can't compile fragment shader. " << info << std::endl;
			program = 0;
			return;
		}

		program = glCreateProgram();
		glAttachShader(program, vertexShader);
		glAttachShader(program, fragmentShader);
		glLinkProgram(program);

		glGetProgramiv(program, GL_LINK_STATUS, &succsess);
		if (!succsess)
		{
			glGetProgramInfoLog(program, 512, nullptr, info);
			std::cerr << "ERROR: can't link program. " << info << std::endl;
			program = 0;
			return;
		}

		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);
	}

	void Shader::Clear()
	{
		if (program != 0)
		{
			glDeleteProgram(program);
		}
	}

	Shader::~Shader()
	{
		Clear();
	}

	Shader* const Shader::GetDefaultShader()
	{
		return Constants::Shaders::GetDefaultShader();
	}
}

