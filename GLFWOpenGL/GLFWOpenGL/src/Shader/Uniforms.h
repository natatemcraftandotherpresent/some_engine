#pragma once
#ifndef UNIFORMS
#define UNIFORMS
#include "Assassin.h"

// transformations
#define gl_ModelViewMatrix "uModelViewMatrix"
#define gl_ModelViewProjectionMatrix "gl_ModelViewProjectionMatrix"
#define gl_ProjectionMatrix "uProjectionMatrix"
#define gl_NormalMatrix "uNormalMatrix"

#define AddMaterialParameter(materialName, PARAMETER) (materialName +std::string(".") + MaterialParameters::PARAMETER).c_str()
#define AddLightParameter(lightName, PARAMETER) (lightName +std::string(".") + LightSourceParameters::PARAMETER).c_str()

// materials
struct MaterialParameters
{
	static std::string Emision;
	static std::string Ambient;
	static std::string Diffuse;
	static std::string Specular;
	static std::string Shininess;
};

// lights
struct LightSourceParameters
{
	static std::string Ambient;
	static std::string Diffuse;
	static std::string Specular;
	static std::string Position;
	static std::string HalfVector;
	static std::string Direction;
	static std::string SpotExponent;
	static std::string SpotCutoff;
	static std::string SpotCosCutoff;
};

// texCoords
#define aTexCoord0 "MultiTexCoord0"
#define aTexCoord1 "MultiTexCoord1"
#define aTexCoord2 "MultiTexCoord2"
#define aTexCoord3 "MultiTexCoord3"
#define aTexCoord4 "MultiTexCoord4"
#define aTexCoord5 "MultiTexCoord5"
#define aTexCoord6 "MultiTexCoord6"
#define aTexCoord7 "MultiTexCoord7"


void SetUniform1f(const GLuint& program, const char* unName, const float value);

void SetUniform3f(const GLuint& program, const char* unName, const float x, const float y, const float z);
void SetUniform3f(const GLuint & program, const char * unName, Vector3& vec);

void SetUniform4f(const GLuint& program, const char* unName, const float x, const float y, const float z, const float w);

void SetUniformMatrix4f(const GLuint& program, const GLuint count, const GLboolean transpose, const char* unName, GLfloat* ptr);
void SetUniformMatrix4f(const GLuint& program, const GLuint count, const GLboolean transpose, const char* unName, const GLfloat* ptr);

void SetUniformMatrix3f(const GLuint& program, const GLuint count, const GLboolean transpose, const char* unName, GLfloat* ptr);
void SetUniformMatrix3f(const GLuint& program, const GLuint count, const GLboolean transpose, const char* unName, const GLfloat* ptr);
#endif