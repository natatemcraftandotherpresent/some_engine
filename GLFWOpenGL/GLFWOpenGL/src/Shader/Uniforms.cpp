#include "PCH.h"
#include "Uniforms.h"
#include "Assassin.h"

std::string MaterialParameters::Emision = "emission";
std::string MaterialParameters::Ambient = "ambient";
std::string MaterialParameters::Diffuse = "diffuse";
std::string MaterialParameters::Specular = "specular";
std::string MaterialParameters::Shininess = "shininess";

std::string LightSourceParameters::Ambient = "ambient";
std::string LightSourceParameters::Diffuse = "diffuse";
std::string LightSourceParameters::HalfVector = "halfVector";
std::string LightSourceParameters::Position = "position";
std::string LightSourceParameters::Specular = "specular";
std::string LightSourceParameters::SpotCosCutoff = "spotCosCutoff";
std::string LightSourceParameters::SpotCutoff = "spotCutoff";
std::string LightSourceParameters::Direction = "direction";
std::string LightSourceParameters::SpotExponent = "spotExponent";

void SetUniform1f(const GLuint & program, const char * unName, const float value)
{
	glUniform1f(glGetUniformLocation(program, unName), value);
}

void SetUniform3f(const GLuint & program, const char * unName, const float x, const float y, const float z)
{
	glUniform3f(glGetUniformLocation(program, unName), x, y, z);
}
void SetUniform3f(const GLuint & program, const char * unName, Vector3& vec)
{
	glUniform3f(glGetUniformLocation(program, unName), vec.x, vec.y, vec.z);
}

void SetUniform4f(const GLuint& program, const char* unName, const float x, const float y, const float z, const float w)
{
	glUniform4f(glGetUniformLocation(program, unName), x, y, z, w);
}

void SetUniformMatrix4f(const GLuint & program, const GLuint count, const GLboolean transpose, const char * unName, GLfloat * ptr)
{
	glUniformMatrix4fv(glGetUniformLocation(program, unName), count, transpose, ptr);
}
void SetUniformMatrix4f(const GLuint & program, const GLuint count, const GLboolean transpose, const char * unName, const GLfloat * ptr)
{
	glUniformMatrix4fv(glGetUniformLocation(program, unName), count, transpose, ptr);
}

void SetUniformMatrix3f(const GLuint & program, const GLuint count, const GLboolean transpose, const char * unName, GLfloat * ptr)
{
	glUniformMatrix3fv(glGetUniformLocation(program, unName), count, transpose, ptr);
}
void SetUniformMatrix3f(const GLuint & program, const GLuint count, const GLboolean transpose, const char * unName, const GLfloat * ptr)
{
	glUniformMatrix3fv(glGetUniformLocation(program, unName), count, transpose, ptr);
}

