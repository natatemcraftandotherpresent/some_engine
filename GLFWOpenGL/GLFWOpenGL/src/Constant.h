#pragma once
#include "Assassin.h"
#include "Shader\Shader.h"
#include "Materials\DefaultMaterial.h"
#include "FrameBuffer\ShadowFramebuffer.h"
#include "Mesh\MeshFilter\Cube.h"
#include "Mesh\MeshFilter\Sphere.h"
#include "Mesh\MeshFilter\Torus.h"
#include "Core.h"
class GRAPHICS_API Constants
{
public:
	class GRAPHICS_API Shaders
	{
	public:

		struct GRAPHICS_API DefaultShader
		{
			static const char* const GetVertexShader();
			static const char* const GetFragmentShader();
		};

		struct GRAPHICS_API DefaultShadowShader
		{
			static const char* const GetVertexShader();
			static const char* const GetFragmentShader();
		};

		static Graphics::Shader* const GetDefaultShader();
		static Graphics::Shader* const GetDefaultShadowShader();

	};

	class GRAPHICS_API Physics
	{
	public:
		inline static const Vector3 GetGravity() { return Vector3::GetGravity(); }
	};

	class GRAPHICS_API Materials
	{
	public:
		static Graphics::DefaultMaterial* const GetDefaultMaterial();
		static Graphics::Material* const GetShadowDefaultMaterial();
	};

	class GRAPHICS_API Framebuffers
	{
	public:
		static Graphics::ShadowFramebuffer* const GetDefaultShadowFramebuffer(const GLuint width = 1024, const GLuint height = 1024);
	};

	class GRAPHICS_API Meshes
	{
	public:
		static Cube* const GetCube();
		static Sphere* const GetSphere();
		static Torus* const GetTorus();
	};
};