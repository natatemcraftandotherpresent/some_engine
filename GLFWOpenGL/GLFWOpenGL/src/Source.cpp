#include "PCH.h"
//#include "Window\Window.h"
//#include "Precision.h"
//
//#include "Time/Time.h"
//
//#include "Assassin.h"
//#include "TestObject.h"
//#include "TestObject2.h"
//#include "Camera\Camera.h"
//#include "Mesh\MeshRegistry.h"
//#include "Light\DirectionLight.h"
//#include "Camera\EngineCamera.h"
//#include "EngineCore.h"
//#include "TestManager.h"
//
//
//const char* vertexShader = "Shaders\\coreTexture.vs";
//
//const char* fragmentShader = "Shaders\\coreTexture.frag";
//
//const char* model = "Models\\IronMan.obj";
//
//
//int main()
//{
//	//Window window = Window({ 800, 600, "First" });
//
//	//float value = 5000 * 65536 / (2 * 3.14159);
//	//value = (int)value << 16;
//	//value = (int)value >> 16;
//	//value *= 2 * 3.14159 / 65536;
//	//
//	//std::cout << std::sin(5000) << std::endl;
//	//std::cout << std::sin(value) << std::endl;
//
//	//EC_LOG(INFO,"%d, %s", LOG_DEFAULT_INDEX, 1, "KE:");
//	//EC_LOG(INFO, "%s", LOG_DEFAULT_INDEX, "Hello");
//
//	//glEnable(GL_DEPTH_TEST);
//	//auto aspect = window.GetAspect();
//
//	(*EngineCore::EngineObjectRegistry::GetSingleton().CreateObject<DirectionLight>())->Create(glm::vec3(0, -7, 1), glm::vec4(1, 0, 0, 1.0f), glm::vec4(0, 0, 1, 1), glm::vec4(1, 1.0, 1.0, 1));
//
//	auto engineCamera = EngineCore::EngineObjectRegistry::GetSingleton().CreateObject<EngineCamera>();
//	//Camera::SetProjectionMatrix(glm::perspective(glm::radians(45.f), aspect, 0.1f, 1000.0f));
//
//	auto obj = EngineCore::EngineObjectRegistry::GetSingleton().CreateObject<TestObject>();
//	auto obj2 = EngineCore::EngineObjectRegistry::GetSingleton().CreateObject<TestObject2>();
//
//	while (!glfwWindowShouldClose(window.GetWindow()))
//	{
//		ProfilerMgr.ProfileBegin("Main Loop", glfwGetTime);
//
//		glClearColor(0.1f, .1f, 0.1f, 1.f);
//		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//		ProfilerMgr.ProfileBegin("Meshes Loop", glfwGetTime);
//		MeshRegistry::UpdateMeshes(window.GetFramebufferWidth(), window.GetFramebufferHeight());
//		ProfilerMgr.ProfileEnd("Meshes Loop", glfwGetTime);
//
//		ProfilerMgr.ProfileBegin("Objects Loop", glfwGetTime);
//		EngineCore::EngineObjectRegistry::GetSingleton().Update(Time::deltaTime());
//		ProfilerMgr.ProfileEnd("Objects Loop", glfwGetTime);
//
//		window.Update();
//		if (begin >= 4 && use)
//		{
//			EngineCore::EngineObjectRegistry::GetSingleton().Remove((EngineCore::EngineObject**)obj);
//			//EngineCore::EngineObjectRegistry::GetSingleton().Remove((EngineCore::EngineObject**)obj);
//			use = false;
//		}
//		begin += Time::deltaTime();
//		ProfilerMgr.ProfileBegin("Physics Loop", glfwGetTime);
//		Physics::Update(Time::deltaTime());
//		ProfilerMgr.ProfileEnd("Physics Loop", glfwGetTime);
//
//		glUseProgram(0);
//
//		ProfilerMgr.ProfileEnd("Main Loop", glfwGetTime);
//		ProfilerMgr.ProfileDumpOutputToBuffer(glfwGetTime, Time::deltaTime);
//		EC_LOG(INFO, "%.6f", LOG_DEFAULT_INDEX, begin);
//		ConsoleBufferMgr.GetSingleton().UpdateBuffer();
//	}
//	//EngineCore::EngineObjectRegistry::RemoveAll();
//	return 0;
//}