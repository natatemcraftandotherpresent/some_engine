#include "PCH.h"
#include <SOIL2/SOIL2.h>
#include "EngineCore.h"
#include <GL/glew.h>
#include "Texture.h"
namespace Graphics
{
	Texture::Texture(const char* texturePath)
	{
		Loadtexture(texturePath);
	}


	void Texture::Loadtexture(const char* texturePath)
	{
		textureID = SOIL_load_OGL_texture(texturePath, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);

		if (!textureID)
		{
			EC_LOG(ERROR, "ERROR: can't load texture %s", LOG_DEFAULT_INDEX, texturePath);
		}
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glGenerateMipmap(GL_TEXTURE_2D);
		if (glewIsSupported("GL_EXT_texture_filter_anisotropic"))
		{
			GLfloat anisoSet = 0.0f;
			glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &anisoSet);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisoSet);
		}

		glBindTexture(GL_TEXTURE_2D, 0);
	}
}
