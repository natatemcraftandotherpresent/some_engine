#pragma once
#ifndef GRAPHICS_TEXTURE
#define GRAPHICS_TEXTURE
#include "PCH.h"
#include "Core.h"

namespace Graphics
{
	class GRAPHICS_API Texture
	{
	public:
		Texture(const char* texturePath);

		inline const GLuint GetTexture() const { return textureID; }
	private:

		void Loadtexture(const char* texturePath);

		GLuint textureID;
	};
}
#endif // GRAPHICS_TEXTURE
