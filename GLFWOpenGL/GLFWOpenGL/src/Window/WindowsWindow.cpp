#include "PCH.h"
#include "WindowsWindow.h"
#include "Camera/Camera.h"
#include "Systems\Input\InputSystem.h"
static bool GLFWInit = false;
static bool GLEWInit = false;

namespace Graphics
{
	WindowsWindow::WindowsWindow(WindowData&& data) : Window(std::forward<WindowData>(data))
	{
		frameBufferWidth = 0;
		frameBufferHeight = 0;
		Init();
	}

	void WindowsWindow::OnUpdate()
	{
		if (currentWindow)
		{
			glfwPollEvents();
			glfwSwapBuffers(currentWindow);
		}
	}

	void WindowsWindow::Shutdown()
	{
		if (currentWindow)
		{
			glfwDestroyWindow(currentWindow);
		}
	}

	void WindowsWindow::Init()
	{
		if (!GLFWInit)
		{
			if (!glfwInit())
			{
				std::cerr << "ERROR: Can't initialise GLFW" << std::endl;
				glfwTerminate();
				return;
			}
	
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
			glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	
			GLFWInit = true;
		}
	
		currentWindow = glfwCreateWindow(winData.width, winData.height, winData.titleName, nullptr, nullptr);
	
		if (currentWindow == nullptr)
		{
			std::cerr << "ERROR: Can't create window" << std::endl;
			glfwTerminate();
			return;
		}
	
		glfwMakeContextCurrent(currentWindow);
		glfwGetFramebufferSize(currentWindow, &frameBufferWidth, &frameBufferHeight);
	
		if (!GLEWInit)
		{
			if (glewInit() != GLEW_OK)
			{
				std::cerr << "ERROR: Can't initialize GLEW" << std::endl;
				glfwDestroyWindow(currentWindow);
				glfwTerminate();
				return;
			}
			GLEWInit = true;
		}
		glViewport(0, 0, frameBufferWidth, frameBufferHeight);
	
		SetCallbacks();

		//Graphics::Camera::GetActiveCamera()->SetProjectionMatrix(glm::perspective(glm::radians(45.f), GetAspect(), 0.1f, 1000.0f));
	}

	void WindowsWindow::Clear()
	{
		Shutdown();
	}

	void WindowsWindow::SetCallbacks()
	{
		InputSystem::ResetInputSystem();
		glfwSetWindowUserPointer(currentWindow, this);
	
		glfwSetWindowSizeCallback(currentWindow, [](GLFWwindow* win, int newWdith, int newHeight)
		{
			Window* window = static_cast<Window*>(glfwGetWindowUserPointer(win));
			window->Resize();
			//Camera::SetProjectionMatrix(glm::perspective<float>(glm::radians(60.f), window->GetAspect(), 0.1f, 1000.f));
		});
	
		glfwSetKeyCallback(currentWindow, InputSystem::HandleKeys);
		glfwSetCursorPosCallback(currentWindow, InputSystem::HandleMouse);
		glfwSetInputMode(currentWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	}
}