#pragma once
#include "PCH.h"
#include "Core.h"
#include "Events/Event.h"
#include "Types/Types.h"
namespace Graphics
{
	struct WindowData
	{
	
		GLuint width;
		GLuint height;
		GLchar* titleName;

		using EventCallbackFn = void(*)(EngineCore::Event&);
		EventCallbackFn callback;
	
		WindowData(GLuint width = 800, GLuint height = 600, GLchar* titleName = (GLchar*)"Default window") : width(width), height(height), titleName(titleName){};
		WindowData(WindowData&& windowData)
		{
			width = windowData.width;
			height = windowData.height;
			titleName = windowData.titleName;
			callback = windowData.callback;
	
			windowData.width = 0;
			windowData.height = 0;
			windowData.titleName = 0;
			windowData.callback = 0;
		}
		WindowData(const WindowData& windowData)
		{
			width = windowData.width;
			height = windowData.height;
			titleName = windowData.titleName;
			callback = windowData.callback;
		}
	};

	class GRAPHICS_API Window
	{
	public:
		using EventCallbackFn = void(*)(EngineCore::Event&);

		Window(WindowData&& data) : winData(std::forward<WindowData>(data))
		{

		}

		virtual void OnUpdate() = 0;

		virtual void SetEventCallback(const EventCallbackFn& callback) = 0;
		virtual void SetVSync(bool enabled) = 0;
		virtual bool IsVSync() const = 0;

		virtual ~Window() = default;

		inline GLFWwindow* const GetWindow() const { return currentWindow; }
		inline const GLuint GetFramebufferWidth() const { return frameBufferWidth; }
		inline const GLuint GetFramebufferHeight() const { return frameBufferHeight; }
		inline GLfloat GetAspect() const { return GLfloat(frameBufferWidth) / GLfloat(frameBufferHeight); }

		void inline Resize()
		{
			if (currentWindow)
			{
				glfwGetFramebufferSize(currentWindow, &frameBufferWidth, &frameBufferHeight);
				glViewport(0, 0, frameBufferWidth, frameBufferHeight);
			}
		}

	protected:
		inline WindowData& GetWindowData() { return winData; }
	protected:
		int32 frameBufferWidth, frameBufferHeight;
		GLFWwindow* currentWindow;
		WindowData winData;
	};
	//class GRAPHICS_API Window
	//{
	//private:
	//	struct WindowData
	//	{
	//
	//		GLuint width;
	//		GLuint height;
	//		GLchar* titleName;
	//
	//		WindowData(GLuint width = 800, GLuint height = 600, GLchar* titleName = (GLchar*)"Default window") : width(width), height(height), titleName(titleName) {};
	//		WindowData(WindowData&& windowData)
	//		{
	//			width = windowData.width;
	//			height = windowData.height;
	//			titleName = windowData.titleName;
	//
	//			windowData.width = 0;
	//			windowData.height = 0;
	//			windowData.titleName = 0;
	//		}
	//		WindowData(const WindowData& windowData)
	//		{
	//			width = windowData.width;
	//			height = windowData.height;
	//			titleName = windowData.titleName;
	//		}
	//	};
	//
	//public:
	//	Window(WindowData&& data);
	//
	//	void Update();
	//
	//	GLfloat inline GetAspect() const { return float(frameBufferWidth) / float(frameBufferHeight); }
	//	inline GLFWwindow* const GetWindow() const { return currentWindow; }
	//	inline const GLuint GetFramebufferWidth() const { return frameBufferWidth; }
	//	inline const GLuint GetFramebufferHeight() const { return frameBufferHeight; }
	//	void inline Resize()
	//	{
	//		glfwGetFramebufferSize(currentWindow, &frameBufferWidth, &frameBufferHeight);
	//		glViewport(0, 0, frameBufferWidth, frameBufferHeight);
	//	}
	//
	//	~Window();
	//
	//private:
	//
	//	void Init();
	//	void Clear();
	//	WindowData winData;
	//	GLFWwindow* currentWindow;
	//
	//	GLint frameBufferWidth;
	//	GLint frameBufferHeight;
	//
	//	void SetCallbacks();
	//	void static window_resize_callback(GLFWwindow* win, int newWdith, int newHeight);
	//};

}


