#pragma once
#ifndef GRAPHICS_WINDOWS_WINDOW
#define GRAPHICS_WINDOWS_WINDOW

#include "Window.h"
namespace Graphics
{
	class WindowsWindow : public Window
	{
	public:
		WindowsWindow(WindowData&& data);

		virtual void OnUpdate();

		virtual void SetEventCallback(const EventCallbackFn& callback) override 
		{
			GetWindowData().callback = callback;
		}
		virtual void SetVSync(bool enabled) override { VSync = enabled; glfwSwapInterval(enabled); }
		virtual bool IsVSync() const override { return VSync; }

		void Shutdown();
	private:
		void Init();
		void Clear();
		void SetCallbacks();
	private:
		bool VSync = false;
	};
}

#endif // GRAPHICS_WINDOWS_WINDOW

