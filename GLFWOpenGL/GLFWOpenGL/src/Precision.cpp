#include "PCH.h"
#include "Precision.h"

glm::vec3 ConvertVector(const Vector3 & vector)
{
	return glm::vec3(vector.x, vector.y, vector.z);
}

Vector3 ConvertVector(const glm::vec3& vector)
{
	return Vector3(vector.x, vector.y, vector.z);
}

float MapValue(float x, float in_min, float in_max, float out_min, float out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


