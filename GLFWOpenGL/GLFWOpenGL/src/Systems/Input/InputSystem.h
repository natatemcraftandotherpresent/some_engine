#pragma once
#ifndef GRAPHICS_INPUT
#define GRAPHICS_INPUT

#include "..\..\PCH.h"
#include "Addons/Singleton.h"
#include "Core.h"

namespace Graphics
{
	class GRAPHICS_API InputSystem : public EngineCore::Singleton<InputSystem>
	{
	public:
		static void CreateDevice();

		static void HandleKeys(GLFWwindow* window, GLint key, GLint code, GLint action, GLint mode);
		static void HandleMouse(GLFWwindow* window, double mouseX, double mouseY);
		static void ResetInputSystem();

		inline static const bool* const GetKeyArray() { return keys; }
		inline static const GLfloat GetXMouseChange()
		{
			GLfloat tempVal = xChange;
			xChange = 0;
			return tempVal;
		}
		inline static const GLfloat GetYMouseChange()
		{
			GLfloat tempVal = yChange;
			yChange = 0;
			return tempVal;
		}
	private:

		//static LPDIRECTINPUT8 directInput;
		//static LPDIRECTINPUTDEVICE8W keyBoard;

		static bool keys[256];

		static GLfloat lastX;
		static GLfloat lastY;
		static GLfloat xChange;
		static GLfloat yChange;
		static bool mouseFirstMoved;
	};
}

#endif