#include "PCH.h"
#include "InputSystem.h"
#include "Window\Window.h"

namespace Graphics
{
	bool InputSystem::keys[256] = { false };

	GLfloat InputSystem::lastX;
	GLfloat InputSystem::lastY;
	GLfloat InputSystem::xChange;
	GLfloat InputSystem::yChange;
	//LPDIRECTINPUT8 directInput = NULL;
	//LPDIRECTINPUTDEVICE8W keyBoard;
	bool InputSystem::mouseFirstMoved = true;

	void InputSystem::CreateDevice()
	{
		//HRESULT hr = DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&directInput, NULL);
		//HRESULT crhr = directInput->CreateDevice(GUID_SysKeyboard, &keyBoard, NULL);
		//HRESULT dthr = keyBoard->SetDataFormat(&c_dfDIKeyboard);
	}

	void InputSystem::HandleKeys(GLFWwindow* window, GLint key, GLint code, GLint action, GLint mode)
	{
		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		{
			glfwSetWindowShouldClose(window, GL_TRUE);
		}

		if (key >= 0 && key < 256)
		{
			if (action == GLFW_PRESS)
			{
				keys[key] = true;
			}
			else if (action == GLFW_RELEASE)
			{
				keys[key] = false;
			}
		}
	}

	void InputSystem::HandleMouse(GLFWwindow* window, double mouseX, double mouseY)
	{
		if (mouseFirstMoved)
		{
			lastX = mouseX;
			lastY = mouseY;
			mouseFirstMoved = false;
		}

		xChange = mouseX - lastX;
		yChange = lastY - mouseY;

		lastX = mouseX;
		lastY = mouseY;
	}

	void InputSystem::ResetInputSystem()
	{
		mouseFirstMoved = true;

		lastX = 0;
		lastY = 0;
		xChange = 0;
		yChange = 0;

		for (size_t i = 0; i < 256; i++)
		{
			keys[i] = 0;
		}
	}
}

