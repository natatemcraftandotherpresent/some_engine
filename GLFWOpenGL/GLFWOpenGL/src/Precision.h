#pragma once
#include "Assassin.h"
#include "glm\glm.hpp"

glm::vec3 ConvertVector(const Vector3& vector);
Vector3 ConvertVector(const glm::vec3& vector);

float MapValue(float a0, float a1, float b0, float b1, float a);
