#pragma once

#ifdef EC_PLATFORM_WINDOWS
#include <Windows.h>
#include <dinput.h>
#endif // EC_PLATFORM_WINDOWS

#include <iostream>
#include <stdint.h>

#include <fstream>
#include <sstream>

#include <string>
#include <stack>
#include <vector>
#include <map>
#include <unordered_map>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <SOIL2/SOIL2.h>

#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\glm.hpp"
#include "glm\gtx\projection.hpp"