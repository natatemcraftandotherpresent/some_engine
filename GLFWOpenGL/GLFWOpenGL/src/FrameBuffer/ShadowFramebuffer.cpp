#include "PCH.h"
#include "ShadowFramebuffer.h"

namespace Graphics
{
	void ShadowFramebuffer::Write()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, GetBufferData()->framebufferID);
	}

	void ShadowFramebuffer::Read(GLenum textureID)
	{
		glActiveTexture(textureID);
		glBindTexture(GL_TEXTURE_2D, GetBufferData()->framebufferTex);
	}

	void ShadowFramebuffer::CreateBuffer(const GLuint bufferWidth, const GLuint bufferHeight)
	{
		glGenFramebuffers(1, &GetBufferData()->framebufferID);

		glGenTextures(1, &GetBufferData()->framebufferTex);
		glBindTexture(GL_TEXTURE_2D, GetBufferData()->framebufferTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, bufferWidth, bufferHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, GetBufferData()->framebufferID);
		glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, GetBufferData()->framebufferTex, 0);

		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);

		GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

		if (Status != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Framebuffer error: %s\n", Status);
		}
	}
}
