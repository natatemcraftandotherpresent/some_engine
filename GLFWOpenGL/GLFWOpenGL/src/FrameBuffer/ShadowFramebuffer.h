#pragma once
#include "PCH.h"
#include "Framebuffer.h"

namespace Graphics
{
	class GRAPHICS_API ShadowFramebuffer : public Framebuffer
	{
	public:
		ShadowFramebuffer(const GLuint bufferWidth, const GLuint bufferHeight)
		{
			CreateBuffer(bufferWidth, bufferHeight);
		}

		virtual void Read(GLenum textureID) override;


		virtual void Write() override;

	protected:
		virtual void CreateBuffer(const GLuint bufferWidth, const GLuint bufferHeight) override;

	};
}