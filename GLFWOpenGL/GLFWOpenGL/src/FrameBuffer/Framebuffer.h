#pragma once
#include "PCH.h"
#include "Core.h"

namespace Graphics
{
	class GRAPHICS_API Framebuffer
	{
	private:
		struct GRAPHICS_API BufferData
		{
			GLuint framebufferID;
			GLuint framebufferTex;
		};

	public:
		virtual ~Framebuffer() = default;

		inline BufferData* const GetBufferData() { return &bufferData; }
		virtual void Read(GLenum textureID) = 0;
		virtual void Write() = 0;
	protected:
		virtual void CreateBuffer(const GLuint bufferWidth, const GLuint bufferHeight) = 0;
	private:
		BufferData bufferData;
	};
}