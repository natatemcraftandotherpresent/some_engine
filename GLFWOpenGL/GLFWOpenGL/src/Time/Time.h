#pragma once
#ifndef TIME
#define TIME
#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "Core.h"
class GRAPHICS_API Time
{
public:
	static const GLfloat deltaTime()
	{
		pastTime = currentTime;
		currentTime = static_cast<GLfloat>(glfwGetTime());
		return 10 * (currentTime - pastTime);
	}

	static inline const GLfloat fixedTime() { return fixedDeltaTime; }

	static inline void SetFixedTime(const float time) { fixedDeltaTime = time; }

private:
	static GLfloat pastTime;
	static GLfloat currentTime;
	static GLfloat fixedDeltaTime;
};


#endif // !TIME