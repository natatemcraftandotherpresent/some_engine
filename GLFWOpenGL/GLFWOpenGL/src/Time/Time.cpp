#include "PCH.h"
#include "Time.h"

GLfloat Time::pastTime = 0;
GLfloat Time::currentTime = 0;
GLfloat Time::fixedDeltaTime = 0.01;