#include "PCH.h"
#include "InstanceMesh.h"

void Graphics::InstanceMesh::Draw(GLenum mode)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, GetMeshDate().textureID);
	glActiveTexture(0);

	glBindVertexArray(GetMeshDate().VAO);
	glDrawArraysInstanced(mode, 0, GetMeshFilter()->GetNumIndices(), transforms.size());
	//glDrawElements(mode, GetMeshFilter()->GetNumIndices(), GL_UNSIGNED_INT, &(*GetMeshFilter()->GetIndices())[0]);

	glBindVertexArray(0);
}
