#include "PCH.h"
#include "MeshRegistry.h"
#include "Shader\Uniforms.h"
#include "Precision.h"
#include "Camera\Camera.h"
#include "Constant.h"
#include "Matrices\Materices.h"
#include "Light\DirectionLight.h"

namespace Graphics
{
	void MeshRegistry::UpdateMeshes(const GLuint width, const GLuint height)
	{
		auto mMat = glm::mat4(1.0f);
		glm::mat4 vmMat = glm::mat4(1);

		glm::mat4 lightVMatrix = glm::lookAt(-*DirectionLight::GetDirection(), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
		glm::mat4 lightPMatrix = glm::ortho(-20.0f, 20.0f, -20.0f, 20.f, 0.1f, 100.f);
		glm::mat4 shadowMVP1;
		glm::mat4 shadowMVP2;

		glEnable(GL_POLYGON_OFFSET_FILL);
		glPolygonOffset(3, 8.0f);
		glViewport(0, 0, 1024, 1024);
		Constants::Framebuffers::GetDefaultShadowFramebuffer(1024, 1024)->Write();
		glClear(GL_DEPTH_BUFFER_BIT);
		size_t index = 0;
		for (auto mesh = meshes.begin(); mesh != meshes.end(); ++mesh, ++index)
		{

			if ((**mesh)->GetMeshType() == MeshType::Instance)
			{

			}



			if ((**mesh)->material)
			{
				CalculateModelMatrix(mesh, mMat, index);
				if (*((**mesh)->material->GetMaterialShadowsInfo()) == Graphics::MaterialShadows::Default)
				{
					Constants::Materials::GetShadowDefaultMaterial()->Use();

					shadowMVP1 = lightPMatrix * lightVMatrix * mMat;
					SetUniformMatrix4f(Constants::Materials::GetShadowDefaultMaterial()->GetShader()->GetShaderID(), 1, GL_FALSE, "shadowMVP", glm::value_ptr(shadowMVP1));
					glDepthFunc(GL_LEQUAL);
					(**mesh)->Draw(GL_TRIANGLES);
				}
			}
		}
		index = 0;
		glDisable(GL_POLYGON_OFFSET_FILL);
		glViewport(0, 0, width, height);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		for (auto mesh = meshes.begin(); mesh != meshes.end(); ++mesh, ++index)
		{
			if ((**mesh)->material)
			{
				CalculateModelMatrix(mesh, mMat, index);
				(**mesh)->material->Use();
				if (*(**mesh)->material->GetMaterialShadowsInfo() == Graphics::MaterialShadows::Default)
				{
					Constants::Framebuffers::GetDefaultShadowFramebuffer()->Read(GL_TEXTURE1);
					shadowMVP2 = lightPMatrix * lightVMatrix * mMat;
					SetUniformMatrix4f((**mesh)->material->GetShader()->GetShaderID(), 1, GL_FALSE, "shadowMVP", glm::value_ptr(shadowMVP2));
				}
				vmMat = *Camera::GetActiveCamera()->GetViewMatrix() * mMat;
				SetUniformMatrix4f((**mesh)->material->GetShader()->GetShaderID(), 1, GL_FALSE, gl_ModelViewMatrix, glm::value_ptr(vmMat));
				SetUniformMatrix4f((**mesh)->material->GetShader()->GetShaderID(), 1, GL_FALSE, gl_ProjectionMatrix, glm::value_ptr(*Camera::GetActiveCamera()->GetProjectionMatrix()));
				(**mesh)->Draw(GL_TRIANGLES);
			}
		}

#pragma opm parallel for
		for (auto it = matrices.begin(); it != matrices.end(); ++it)
		{
			it->operator=(emptyMatrix);
		}
	}

	inline void MeshRegistry::CalculateModelMatrix(std::vector<Mesh**>::iterator mesh, glm::mat4& mMat, const GLuint index)
	{
		if (matrices[index] == glm::mat4(0))
		{
			auto transform = (**mesh)->GetTransform();
			mMat = glm::translate(glm::mat4(1), ConvertVector(transform->GetPosition()));
			mMat = glm::rotate(mMat, transform->GetRotation().x, glm::vec3(1, 0, 0));
			mMat = glm::rotate(mMat, transform->GetRotation().y, glm::vec3(0, 1, 0));
			mMat = glm::rotate(mMat, transform->GetRotation().z, glm::vec3(0, 0, 1));
			mMat = glm::scale(mMat, ConvertVector(transform->GetScale()));
			matrices[index] = mMat;
		}
		else
		{
			mMat = matrices[index];
		}
	}

	std::vector<Mesh**> MeshRegistry::meshes;
	std::vector<glm::mat4> MeshRegistry::matrices;

	const glm::mat4 MeshRegistry::emptyMatrix = glm::mat4(0);

}