#pragma once
#include "PCH.h"
#include "Assassin.h"
#include "MeshFilter\MeshFilter.h"
#include "Core.h"
#include "Materials\Material.h"

namespace Graphics
{
	enum class MeshType
	{
		Static = 0,
		Instance
	};

	struct GRAPHICS_API Mesh : public Component
	{
		struct MeshData
		{
			MeshData() : VAO(0), index(0) {};

			GLuint VAO;
			std::stack<GLuint> VBOS;
			GLuint index;
			GLuint textureID;
		};
	public:
		Mesh();
		virtual void Draw(GLenum mode);

		void SetData(void* parent, const EngineCore::Handle<tagType>& handle, EngineCore::Memory::MemoryStorage* stor) override;

		void AddTexture(GLuint texture);

		void AddMeshFilter(MeshFilter* filter)
		{
			Clear();
			meshFilter = filter;
			CreateMesh();
		}
		Graphics::Material* material;
		virtual void SetParent(void* parent);
		inline Transform* const GetTransform() const { return transform; }
		virtual void Dispose() override;


		inline void SetMeshType(MeshType type) { meshType = type; }
		inline MeshType GetMeshType() const { return meshType; }
	protected:
		void CreateMesh();
		inline MeshData& GetMeshDate() { return meshData; }
		inline MeshFilter* GetMeshFilter() { return meshFilter; }
	private:
		void Clear();

		MeshData meshData;
		MeshType meshType;
		MeshFilter* meshFilter;
		Transform* transform;

	};
}
