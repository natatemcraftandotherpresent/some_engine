#include "PCH.h"
#include "Torus.h"
#include "glm\glm.hpp"
#include "Precision.h"
#include "glm\ext\matrix_transform.hpp"

int Torus::numVertices;
int Torus::numIndices;

std::vector<float> Torus::vertices;
std::vector<float> Torus::normals;
std::vector<float> Torus::texCoords;
std::vector<GLuint> Torus::indices;

int Torus::prec = 48;
float Torus::inner = 0.5f;
float Torus::outer = 0.2f;

void Torus::CreateData()
{
	if (vertices.size() > 0) return;

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec3> sTangents;
	std::vector<glm::vec3> tTangents;
	std::vector<glm::vec2> texCoords;
	numVertices = (prec + 1) * (prec + 1);
	numIndices = prec * prec * 6;

	for (int i = 0; i < numVertices; i++) { vertices.push_back(glm::vec3()); }
	for (int i = 0; i < numVertices; i++) { texCoords.push_back(glm::vec2()); }
	for (int i = 0; i < numVertices; i++) { normals.push_back(glm::vec3()); }
	for (int i = 0; i < numVertices; i++) { sTangents.push_back(glm::vec3()); }
	for (int i = 0; i < numVertices; i++) { tTangents.push_back(glm::vec3()); }
	for (int i = 0; i < numIndices; i++) { indices.push_back(0); }

	// calculate first ring
	for (int i = 0; i <= prec; i++)
	{
		float amt = toRadians(static_cast<double>(i * 360.0f / prec));
		glm::mat4 rMat = glm::rotate(glm::mat4(1), amt, glm::vec3(0, 0, 1));

		glm::vec3 initPos(rMat * glm::vec4(outer, 0, 0, 1));
		vertices[i] = glm::vec3(initPos + glm::vec3(inner, 0, 0));

		texCoords[i] = glm::vec2(0.0, (float)i / (float)prec);

		rMat = glm::rotate(glm::mat4(1), amt, glm::vec3(0, 0, 1));
		tTangents[i] = glm::vec3(rMat * glm::vec4(0, -1, 0, 1));
		sTangents[i] = glm::vec3(glm::vec3(0, 0, -1));

		normals[i] = glm::cross(tTangents[i], sTangents[i]);
	}

	for (int ring = 1; ring < prec + 1; ring++)
	{
		for (int vert = 0; vert < prec + 1; vert++)
		{
			float amt = toRadians(static_cast<double>(ring * 360.0f / prec));
			glm::mat4 rMat = glm::rotate(glm::mat4(1), amt, glm::vec3(0, 1, 0));
			vertices[ring*(prec + 1) + vert] = glm::vec3(rMat * glm::vec4(vertices[vert], 1.0));

			texCoords[ring * (prec + 1) + vert] = glm::vec2((float)ring * 2.0f / prec, texCoords[vert].t);
			if (texCoords[ring * (prec + 1) + vert].s > 1.0) texCoords[ring * (prec + 1) + vert].s -= 1.0;

			rMat = glm::rotate(glm::mat4(1), amt, glm::vec3(0, 1, 0));
			sTangents[ring * (prec + 1) + vert] = glm::vec3(rMat * glm::vec4(sTangents[vert], 1.0));
			rMat = glm::rotate(glm::mat4(1), amt, glm::vec3(0, 1, 0));
			tTangents[ring * (prec + 1) + vert] = glm::vec3(rMat * glm::vec4(tTangents[vert], 1.0));

			rMat = glm::rotate(glm::mat4(1), amt, glm::vec3(0, 1, 0));
			normals[ring * (prec + 1) + vert] = glm::vec3(rMat * glm::vec4(normals[vert], 1.0));
		}
	}

	for (int ring = 0; ring < prec; ring++)
	{
		for (int vert = 0; vert < prec; vert++)
		{
			indices[((ring * prec + vert) * 2) * 3 + 0] = ring * (prec + 1) + vert;
			indices[((ring * prec + vert) * 2) * 3 + 1] = ring * (prec + 1) + vert + 1;
			indices[((ring * prec + vert) * 2) * 3 + 2] = (ring + 1)* (prec + 1) + vert;
			indices[((ring * prec + vert) * 2 + 1) * 3 + 0] = (ring + 1)* (prec + 1) + vert;
			indices[((ring * prec + vert) * 2 + 1) * 3 + 1] = ring * (prec + 1) + vert + 1;
			indices[((ring * prec + vert) * 2 + 1) * 3 + 2] = (ring + 1)* (prec + 1) + vert + 1;
		}
	}

	for (int i = 0; i < numVertices; i++)
	{
		this->vertices.push_back((vertices[i]).x);
		this->vertices.push_back((vertices[i]).y);
		this->vertices.push_back((vertices[i]).z);

		this->normals.push_back((normals[i].x));
		this->normals.push_back((normals[i].y));
		this->normals.push_back((normals[i].z));

		this->texCoords.push_back((texCoords[i].x));
		this->texCoords.push_back((texCoords[i].y));
	}
}

