#include "PCH.h"
#include "Sphere.h"
#include "GL\glew.h"
#include "glm\glm.hpp"
#include "Precision.h"


void Sphere::CreateData()
{
	if (vertices.size() <= 0)
	{
		numIndices = prec * prec * 6;
		std::vector<glm::vec3> vertices;
		std::vector<glm::vec2> texCoords;

		numVertices = (prec + 1) * (prec + 1);

		for (int i = 0; i < numVertices; i++)
		{
			vertices.push_back(glm::vec3(0));
		}
		for (int i = 0; i < numVertices; i++)
		{
			texCoords.push_back(glm::vec2(0));
		}
		for (int i = 0; i < numIndices; i++)
		{
			indices.push_back(0);
		}

		//#pragma omp parallel for
		for (int i = 0; i <= prec; i++)
		{
			for (int j = 0; j <= prec; j++)
			{
				float y = (float)std::cosf(toRadians(static_cast<double>(180.0f - i * 180.0f / prec)));
				float x = -(float)std::cosf(toRadians(static_cast<double>(j * 360.f / prec))) * (float)std::fabs(std::cosf(std::asinf(y)));
				float z = (float)std::sin(toRadians(static_cast<double>(j * 360.f / prec))) * (float)std::fabs(std::cosf(std::asinf(y)));

				vertices[i *(prec + 1) + j] = glm::vec3(x, y, z);
				texCoords[i *(prec + 1) + j] = glm::vec2((float)j / prec, (float)i / prec);
			}
		}
		//#pragma omp parallel for
		for (int i = 0; i < prec; i++)
		{
			for (int j = 0; j < prec; j++)
			{
				indices[6 * (i * prec + j) + 0] = i * (prec + 1) + j;
				indices[6 * (i * prec + j) + 1] = (i + 1) * (prec + 1) + j;
				indices[6 * (i * prec + j) + 2] = i * (prec + 1) + j + 1;
				indices[6 * (i * prec + j) + 3] = i * (prec + 1) + j + 1;
				indices[6 * (i * prec + j) + 4] = (i + 1) * (prec + 1) + j;
				indices[6 * (i * prec + j) + 5] = (i + 1) * (prec + 1) + j + 1;
			}
		}
		for (int i = 0; i < numVertices; i++)
		{
			this->vertices.push_back((vertices[i]).x);
			this->vertices.push_back((vertices[i]).y);
			this->vertices.push_back((vertices[i]).z);

			this->texCoords.push_back((texCoords[i]).x);
			this->texCoords.push_back((texCoords[i]).y);
		}
		this->normals = this->vertices;
	}
}

std::vector<float> Sphere::vertices;
std::vector<float> Sphere::normals;
std::vector<float> Sphere::texCoords;
std::vector<GLuint> Sphere::indices;
int Sphere::numIndices;
int Sphere::numVertices;
const int Sphere::prec = 150;
