#pragma once
#include "MeshFilter.h"
#include "glm\glm.hpp"
#include "ICreateMeshData.h"
#include "GL\glew.h"

struct GRAPHICS_API Torus : public MeshFilter, ICreateMeshData
{
public:
	Torus() { CreateData(); }

	virtual const std::vector<float>* GetVertices() const override { return &vertices; }
	virtual const std::vector<float>* GetNormals() const override { return &normals; }
	virtual const std::vector<float>* GetTexCoords() const override { return &texCoords; }
	virtual const std::vector<GLuint>* GetIndices() const override { return &indices; }

	virtual const size_t GetNumVertices() const { return numVertices; }
	virtual const size_t GetNumIndices() const { return numIndices; }
protected:
	virtual void CreateData() override;
private:
	static int prec;
	static float inner;
	static float outer;
	
	static int numVertices;
	static int numIndices;
	static std::vector<float> vertices;
	static std::vector<float> normals;
	static std::vector<float> texCoords;
	static std::vector<GLuint> indices;
};