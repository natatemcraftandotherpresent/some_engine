#pragma once
#include "PCH.h"
#include "EngineCoreDLL.h"
#include "GL\glew.h"
#include "Core.h"

struct GRAPHICS_API MeshFilter
{
public:

	MeshFilter(const std::vector<float>& vertices, const std::vector<float>& normals, const std::vector<float>& texCoords) :
		vertices(vertices), normals(normals), texCoords(texCoords) {};
	MeshFilter() = default;

	virtual const std::vector<float>* GetVertices() const { return &vertices; }
	virtual const std::vector<float>* GetNormals() const { return &normals; }
	virtual const std::vector<float>* GetTexCoords() const { return &texCoords; }
	virtual const std::vector<GLuint>* GetIndices() const { return &indices; }

	virtual inline const size_t GetNumVertices() const { return vertices.size() / 3; }
	virtual inline const size_t GetNumIndices() const { return indices.size(); }

protected:
	ECMemory::MemoryStorage* memory;
private:

	std::vector<float> vertices;
	std::vector<float> normals;
	std::vector<float> texCoords;
	std::vector<GLuint> indices;
};

