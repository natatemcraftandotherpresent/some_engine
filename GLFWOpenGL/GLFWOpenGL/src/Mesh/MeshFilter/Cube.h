#pragma once
#include "MeshFilter.h"

struct GRAPHICS_API Cube : public MeshFilter
{
public:
	virtual const std::vector<float>* GetVertices() const override { return &vertices; }
	virtual const std::vector<float>* GetTexCoords() const override { return &texCoords; }
	virtual const std::vector<float>* GetNormals() const override { return &normals; }
	virtual const std::vector<GLuint>* GetIndices() const override { return &indices; }
	virtual const size_t GetNumIndices() const override { return indices.size(); }
	virtual const size_t GetNumVertices() const override { return vertices.size(); }
private:
	static std::vector<float> vertices;
	static std::vector<float> normals;
	static std::vector<float> texCoords;
	static std::vector<GLuint> indices;
};