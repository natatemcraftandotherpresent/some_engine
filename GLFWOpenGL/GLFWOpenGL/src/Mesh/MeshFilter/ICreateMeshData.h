#pragma once
#include "Core.h"
struct GRAPHICS_API ICreateMeshData
{
protected:
	virtual void CreateData() = 0;
};