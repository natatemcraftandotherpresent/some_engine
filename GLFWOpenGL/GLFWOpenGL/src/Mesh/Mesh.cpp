#include "PCH.h"
#include "Mesh.h"
#include "MeshRegistry.h"

namespace Graphics
{
	Mesh::Mesh()
	{

	}

	void Mesh::AddTexture(GLuint texture)
	{
		meshData.textureID = texture;
	}
	void Mesh::CreateMesh()
	{
		meshData.VBOS.push(0);
		GLuint* VBO = &meshData.VBOS.top();
		glGenVertexArrays(1, &meshData.VAO);
		glBindVertexArray(meshData.VAO);
		glGenBuffers(1, VBO);

		glBindBuffer(GL_ARRAY_BUFFER, *VBO);
		auto vertices = meshFilter->GetVertices();
		glBufferData(GL_ARRAY_BUFFER, sizeof((*vertices)[0]) * vertices->size(), &(*vertices)[0], GL_STATIC_DRAW);
		glVertexAttribPointer(meshData.index, 3, GL_FLOAT, false, 0, nullptr);
		glEnableVertexAttribArray(meshData.index++);

		meshData.VBOS.push(0);
		VBO = &meshData.VBOS.top();
		glGenBuffers(1, VBO);

		glBindBuffer(GL_ARRAY_BUFFER, *VBO);
		auto normals = meshFilter->GetNormals();
		glBufferData(GL_ARRAY_BUFFER, sizeof((*normals)[0]) * normals->size(), &(*normals)[0], GL_STATIC_DRAW);
		glVertexAttribPointer(meshData.index, 3, GL_FLOAT, false, 0, nullptr);
		glEnableVertexAttribArray(meshData.index++);

		meshData.VBOS.push(0);
		VBO = &meshData.VBOS.top();
		glBindVertexArray(meshData.VAO);
		glGenBuffers(1, VBO);
		glBindBuffer(GL_ARRAY_BUFFER, *VBO);
		auto textures = meshFilter->GetTexCoords();
		glBufferData(GL_ARRAY_BUFFER, sizeof((*textures)[0]) * textures->size(), &((*textures)[0]), GL_STATIC_DRAW);
		glVertexAttribPointer(meshData.index, 2, GL_FLOAT, false, 0, nullptr);
		glEnableVertexAttribArray(meshData.index++);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		if (meshFilter->GetNumVertices() > 0)
		{
			meshData.VBOS.push(0);
			GLuint* VBO = &meshData.VBOS.top();
			glGenBuffers(1, VBO);
			auto indices = meshFilter->GetIndices();
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *VBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof((*indices)[0]) * indices->size(), &(*indices)[0], GL_STATIC_DRAW);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		}

		glBindVertexArray(0);
	}

	void Mesh::Draw(GLenum mode)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, meshData.textureID);
		glActiveTexture(0);

		glBindVertexArray(meshData.VAO);
		glDrawElements(mode, meshFilter->GetNumIndices(), GL_UNSIGNED_INT, &(*meshFilter->GetIndices())[0]);

		glBindVertexArray(0);
	}

	void Mesh::Clear()
	{
		if (meshData.VAO != 0)
		{
			glDeleteVertexArrays(1, &meshData.VAO);
		}
		while (meshData.VBOS.size() > 0)
		{
			GLuint* buffer = &meshData.VBOS.top();
			if (buffer != 0)
			{
				glDeleteBuffers(1, buffer);
			}
			meshData.VBOS.pop();
		}
		meshData.index = 0;
	}

	void Mesh::SetData(void* parent, const EngineCore::Handle<tagType>& handle, EngineCore::Memory::MemoryStorage* stor)
	{
		Component::SetData(parent, handle, stor);
		MeshRegistry::AddMesh((Mesh**)(storage->GetPointerMemoryPointer()));
	}

	void Mesh::SetParent(void* parent)
	{
		Component::SetParent(parent);
		transform = static_cast<EngineObject*>(parent)->GetComponent<Transform>();
		//storage = static_cast<EngineObject*>(parent)->GetComponentPoint<Mesh>();

	}

	void Mesh::Dispose()
	{
		//Component::Dispose();
		MeshRegistry::Remove((Mesh**)(storage->GetPointerMemoryPointer()));
		Clear();
	}

}