#pragma once
#ifndef GRAPHICS_INSTANCE_MESH
#define GRAPHICS_INSTANCE_MESH

#include "PCH.h"
#include "Mesh.h"
namespace Graphics
{
	class InstanceMesh : public Mesh
	{
	public:
		InstanceMesh() : Mesh() {}

		virtual void Draw(GLenum mode) override;

	public:
		std::vector<Transform> transforms;
	};
}

#endif // GRAPHICS_INSTANCE_MESH