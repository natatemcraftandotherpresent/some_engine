#pragma once
#ifndef MESH_REGISTRY
#define MESH_REGISTRY
#include "PCH.h"
#include "Mesh.h"
namespace Graphics
{
	class MeshRegistry
	{
	public:
		static void AddMesh(Mesh** mesh)
		{
			matrices.push_back(emptyMatrix);
			meshes.push_back(mesh);
		}
		static void Remove(Mesh** mesh)
		{
			int index = 0;
			auto it = std::find_if(meshes.cbegin(), meshes.cend(), [&index, &mesh](const auto& first)
				{
					index++;
					if (first == mesh)
					{
						return true;
					}
					return false;
				});
			if (it != meshes.cend())
			{
				matrices.erase(matrices.begin() + index - 1);
				meshes.erase(it);
			}
		}
		static void UpdateMeshes(const GLuint width, const GLuint height);

		inline static void CalculateModelMatrix(std::vector<Mesh**>::iterator mesh, glm::mat4& mMat, const GLuint index = 0);

	private:
		static std::vector<Mesh**> meshes;
		static std::vector<glm::mat4> matrices;

		static const glm::mat4 emptyMatrix;
	};

}
#endif