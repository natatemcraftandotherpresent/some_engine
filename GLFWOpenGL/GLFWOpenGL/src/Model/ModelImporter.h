#pragma once
#include "PCH.h"
#include "Mesh\MeshFilter\MeshFilter.h"
#include "Core.h"
class GRAPHICS_API ModelImporter
{
public:

	static MeshFilter ParseOBJ(const char* path);

private:

	static std::vector<float> vertices;
	static std::vector<float> texCoords;
	static std::vector<float> normals;
};