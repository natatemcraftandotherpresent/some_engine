#include "PCH.h"
#include "ModelImporter.h"
#include <string>

std::vector<float> ModelImporter::vertices;
std::vector<float> ModelImporter::texCoords;
std::vector<float> ModelImporter::normals;

MeshFilter ModelImporter::ParseOBJ(const char * path)
{
	std::vector<float> verticesOBJ;
	std::vector<float> texCoordsOBJ;
	std::vector<float> normalsOBJ;

	float x, y, z;
	std::string content;

	std::ifstream fileStream(path, std::ios::in);
	if (fileStream.is_open())
	{
		std::string line = "";

		while (!fileStream.eof())
		{
			std::getline(fileStream, line);
			if (line.compare(0, 2, "v ") == 0)
			{
				std::stringstream ss(line.erase(0, 2));
				ss >> x;
				ss >> y;
				ss >> z;
				verticesOBJ.push_back(x);
				verticesOBJ.push_back(y);
				verticesOBJ.push_back(z);
			}
			else if (line.compare(0, 2, "vt") == 0)
			{
				std::stringstream ss(line.erase(0, 2));
				ss >> x;
				ss >> y;
				texCoordsOBJ.push_back(x);
				texCoordsOBJ.push_back(y);
			}
			else if (line.compare(0, 2, "vn") == 0)
			{
				std::stringstream ss(line.erase(0, 2));
				ss >> x;
				ss >> y;
				ss >> z;
				normalsOBJ.push_back(x);
				normalsOBJ.push_back(y);
				normalsOBJ.push_back(z);
			}
			else if (line.compare(0, 2, "f ") == 0)
			{
				std::string oneCorner, v, t, n;
				std::stringstream ss(line.erase(0, 2));
				for (int i = 0; i < 3; i++)
				{
					std::getline(ss, oneCorner, ' ');
					std::stringstream oneCornerSS(oneCorner);
					std::getline(oneCornerSS, v, '/');
					std::getline(oneCornerSS, t, '/');
					std::getline(oneCornerSS, n, '/');

					if (v != "")
					{
						int vertRef = (std::stoi(v) - 1) * 3;
						vertices.push_back(verticesOBJ[vertRef]);
						vertices.push_back(verticesOBJ[vertRef + 1]);
						vertices.push_back(verticesOBJ[vertRef + 2]);
					}
					if (t != "")
					{
						int tcRef = (std::stoi(t) - 1) * 2;
						texCoords.push_back(texCoordsOBJ[tcRef]);
						texCoords.push_back(texCoordsOBJ[tcRef + 1]);
					}
					if (n != "")
					{
						int normRef = (std::stoi(n) - 1) * 3;
						normals.push_back(normalsOBJ[normRef]);
						normals.push_back(normalsOBJ[normRef + 1]);
						normals.push_back(normalsOBJ[normRef + 2]);
					}
				}
			}
		}
		return MeshFilter(vertices, normals, texCoords);
	}
	return MeshFilter();
}
