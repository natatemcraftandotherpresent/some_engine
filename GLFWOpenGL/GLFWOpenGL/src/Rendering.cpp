#include "PCH.h"
#include "Rendering.h"
#include "Window/Window.h"
#include "Camera/Camera.h"
#include "Mesh/MeshRegistry.h"

namespace Graphics
{
	Rendering::Rendering(unsigned screenWidth, unsigned screenHeight) : window({ screenWidth, screenHeight, "First" })
	{
		StartRender(screenWidth, screenHeight);
	}

	bool Rendering::Update()
	{
		if (!glfwWindowShouldClose(window.GetWindow()))
		{
			glClearColor(0.1f, .1f, 0.1f, 1.f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			MeshRegistry::UpdateMeshes(window.GetFramebufferWidth(), window.GetFramebufferHeight());

			window.OnUpdate();

			glUseProgram(0);
		}
		return !glfwWindowShouldClose(window.GetWindow());
	}

	void Rendering::StartRender(unsigned screenWidth, unsigned screenHeight)
	{
		glEnable(GL_DEPTH_TEST);
		glewExperimental = GL_TRUE;
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
	}
}