#pragma once
#ifndef GRAPHICS_CAMERA
#define GRAPHICS_CAMERA

#include "PCH.h"
#include "GL/glew.h"
#include "Object/WorldObject.h"
#include "Assassin.h"
#include "Precision.h"
#include "Core.h"
namespace Graphics
{
	struct CameraData
	{
		float FOV;
		float aspect;
		float nearPlane;
		float farPlane;

		Vector3 rotation;
		Vector3 position;

		Vector3 upVector;
		Vector3 rightVector;

		Vector3 GetForwardVector() const
		{
				cacheForward = Vector3(std::cosf(toRadians(rotation.y)) * std::cosf(toRadians(rotation.x)),
					std::sinf(toRadians(rotation.x)),
					std::sin(toRadians(rotation.y)) * std::cosf(toRadians(rotation.x)));
				cacheForward.normalize();
			return cacheForward;
		}
		inline void ResetCache() { cacheForward = 0.f; }
	private:
		mutable Vector3 cacheForward = 0.f;
	};
	class GRAPHICS_API Camera : public WorldObject
	{
	public:
		Camera() = default;
		Camera(const GLfloat yaw, const GLfloat pitch, const GLfloat moveSpeed, const GLfloat turnSpeed);
		glm::mat4* const GetViewMatrix() { return &viewMat; }
		glm::mat4* const GetProjectionMatrix() { return &projMat; }
		glm::vec3 const GetCameraPosition() { return ConvertVector(cameraData.position); }

		inline const GLfloat GetMoveSpeed() const { return moveSpeed; }
		inline const GLfloat GetTurnSpeed() const { return turnSpeed; }

		void SetProjectionMatrix(glm::mat4&& proj) { projMat = proj; }
		void SetCameraPosition(Vector3&& position) { cameraData.position = position; }
		static Camera* GetActiveCamera() { return activeCamera; }
		static void SetActiveCamera(Camera* camera) { activeCamera = camera; }

		void SetUp(const GLfloat yaw, const GLfloat pitch, const GLfloat moveSpeed, const GLfloat turnSpeed);
	protected:
		//inline Transform* const GetTransform() const { return transform; }

		inline CameraData& GetCameraData() 
		{ 
			return cameraData; 
		}

		inline void SetViewMatrix(glm::mat4& view) { viewMat = view; }
	private:

		glm::mat4 viewMat;
		glm::mat4 projMat;

		static Camera* activeCamera;

		GLfloat moveSpeed;
		GLfloat turnSpeed;

		CameraData cameraData;
	};
}
#endif // GRAPHICS_CAMERA