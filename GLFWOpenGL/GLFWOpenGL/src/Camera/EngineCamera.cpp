#include "PCH.h"
#include "EngineCamera.h"
#include "Systems\Input\InputSystem.h"
#include "Assassin.h"

namespace Graphics
{
	EngineCamera::EngineCamera(glm::vec3& up, const GLfloat yaw, const GLfloat pitch, const GLfloat moveSpeed, const GLfloat turnSpeed) : Camera(yaw, pitch, moveSpeed, turnSpeed)
	{
		
	}

	void EngineCamera::Update(float duration)
	{
		auto& cameraData = GetCameraData();
		CheckMousePosition(InputSystem::GetXMouseChange(), InputSystem::GetYMouseChange(), cameraData);
		CheckKeys(duration, cameraData);
		auto front = cameraData.GetForwardVector();


		cameraData.rightVector = Vector3::CrossProduct(front, Transform::GetWorldUpVector()).normalize();

		cameraData.upVector = Vector3::CrossProduct(cameraData.rightVector, front).normalize();

		SetViewMatrix(glm::lookAt(ConvertVector(cameraData.position), ConvertVector(cameraData.position + cameraData.GetForwardVector()), ConvertVector(cameraData.upVector)));
	}

	void EngineCamera::Start()
	{
		SetUp(-90, 0, 5, 1);
		SetActiveCamera(this);
	}

	void EngineCamera::CheckKeys(float duration, CameraData& cameraData)
	{
		if (InputSystem::GetKeyArray()[GLFW_KEY_W])
		{
			cameraData.position += (cameraData.GetForwardVector() * GetMoveSpeed() * duration);
		}

		if (InputSystem::GetKeyArray()[GLFW_KEY_S])
		{
			cameraData.position -= (cameraData.GetForwardVector() * GetMoveSpeed() * duration);
		}

		if (InputSystem::GetKeyArray()[GLFW_KEY_A])
		{
			cameraData.position -= (cameraData.rightVector * GetMoveSpeed() * duration);
		}

		if (InputSystem::GetKeyArray()[GLFW_KEY_D])
		{
			cameraData.position += (cameraData.rightVector * GetMoveSpeed() * duration);
		}
	}

	void EngineCamera::CheckMousePosition(GLfloat xChange, GLfloat yChange, CameraData& cameraData)
	{
		xChange *= GetTurnSpeed();
		yChange *= GetTurnSpeed();
		if (xChange != 0.f && yChange != 0.f)
		{
			cameraData.ResetCache();
		}
		cameraData.rotation.x += yChange;
		cameraData.rotation.y += xChange;

		if (cameraData.rotation.x > 89.0f || cameraData.rotation.x < -89.0f)
		{
			cameraData.rotation.x -= yChange;
		}
	}
}
