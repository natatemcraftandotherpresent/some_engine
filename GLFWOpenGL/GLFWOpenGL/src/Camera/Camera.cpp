#include "PCH.h"
#include "Camera.h"
#include "Assassin.h"
#include "Precision.h"

namespace Graphics
{
	Camera* Camera::activeCamera;
	Camera::Camera(const GLfloat yaw, const GLfloat pitch, const GLfloat moveSpeed, const GLfloat turnSpeed)
	{
		SetUp(yaw, pitch, moveSpeed, turnSpeed);
	}

	void Camera::SetUp(const GLfloat yaw, const GLfloat pitch, const GLfloat moveSpeed, const GLfloat turnSpeed)
	{
		cameraData.rotation.y = yaw;
		cameraData.rotation.x = pitch;
		this->turnSpeed = turnSpeed;
		this->moveSpeed = moveSpeed;
	}
}
