#pragma once
#include "Camera.h"
#include "Core.h"

namespace Graphics
{

	class Assassin::Transform;
	class GRAPHICS_API EngineCamera : public Camera
	{
	public:
		EngineCamera() {
			SetCameraPosition(Vector3(0, 2, 10));
		};
		EngineCamera(glm::vec3& up, const GLfloat yaw, const GLfloat pitch, const GLfloat moveSpeed, const GLfloat turnSpeed);
		virtual void Update(float duration) override;
		virtual void Start() override;
	private:
		void CheckKeys(float duration, CameraData& cameraData);
		void CheckMousePosition(GLfloat xChange, GLfloat yChange, CameraData& cameraData);
	};
}