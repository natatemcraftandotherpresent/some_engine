#pragma once
#include "glm\mat4x4.hpp"

const glm::mat4 BuildTranslate(float x, float y, float z)
{
	return glm::mat4(1.0f, 0.0f, 0.0f, 0.0f,
					 0.0f, 1.0f, 0.0f, 0.0f,
					 0.0f, 0.0f, 1.0f, 0.0f,
					 x, y, z, 1.0f);
}

const glm::mat4 BuildRotateX(float rad)
{
	return glm::mat4(1.0f, 0.0f, 0.0f, 0.0f,
					 0.0f, std::cosf(rad), -std::sinf(rad), 0.0f,
					 0.0f, std::sinf(rad), std::cosf(rad), 0.0f,
					 0.0f, 0.0f, 0.0f, 1.0f);
}

const glm::mat4 BuildRoateY(float rad)
{
	return glm::mat4(std::cosf(rad), 0.0f, std::sinf(rad), 0.0f,
					 0.0f, 1.0f, 0.0f, 0.0f,
					 -std::sinf(rad), 0.0f, std::cosf(rad), 0.0f,
					 0.0f, 0.0f, 0.0f, 1.0f);
}

const glm::mat4 BuildRotateZ(float rad)
{
	return glm::mat4(std::cosf(rad), -std::sinf(rad), 0.0f, 0.0f,
					 std::sinf(rad), std::cosf(rad), 0.0f, 0.0f,
					 0.0f, 0.0f, 1.0f, 0.0f,
					 0.0f, 0.0f, 0.0f, 1.0f);
}

const glm::mat4 BuildScale(float x, float y, float z)
{
	return glm::mat4(x, 0.f, 0.0f, 0.0f,
					 0.0f, y, 0.0f, 0.0f,
					 0.0f, 0.0f, z, 0.0f,
					 0.0f, 0.0f, 0.0f, 1.0f);
}

const glm::mat4 Biases()
{
	return glm::mat4(0.5, 0, 0, 0,
					 0, 0.5, 0, 0,
					 0, 0, 0.5, 0,
					 0.5, 0.5, 0.5, 1);
}