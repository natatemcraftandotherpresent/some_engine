#version 330			

//#include "Build-in-Uniforms"

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 color;

out vec3 aColor;

//#define uModelViewMatrix gl_ModelViewMatrix
#define uProjectionMatrix gl_ProjectionMatrix

//uniform gl_ModelViewMatrix;
uniform mat4 mv_mat;

void main()						   
{		
	gl_Position = gl_ProjectionMatrix * mv_mat * vec4(pos,1);
	aColor = clamp(color, 0, 1);
}   
