#pragma once
#ifndef OPENGL_BUILD_IN_UNIFORMS
#define OPENGL_BUILD_IN_UNIFORMS

// transformations
#define uModelViewMatrix gl_ModelViewMatrix
#define uProjectionMatrix gl_ProjectionMatrix
#define uModelViewProjectionMatrix gl_ModelViewProjectionMatrix
#define uNormalMatrix gl_NormalMatrix
#define uModelViewMatrixInverse gl_ModelViewMatrixInvers

#define aColor gl_Color
#define aNormal gl_Normal
#define aVertex gl_Vertex

// texCoords
#define aTexCoord0 gl_MultiTexCoord0
#define aTexCoord1 gl_MultiTexCoord1
#define aTexCoord2 gl_MultiTexCoord2
#define aTexCoord3 gl_MultiTexCoord3
#define aTexCoord4 gl_MultiTexCoord4
#define aTexCoord5 gl_MultiTexCoord5
#define aTexCoord6 gl_MultiTexCoord6
#define aTexCoord7 gl_MultiTexCoord7

// materials
struct gl_MaterialParameters
{
	vec4 Emision;
	vec4 Ambient;
	vec4 Diffuse;
	vec4 Specular;
	float Shininess;
};

// light
struct gl_LightSourceParameters
{
	vec4 Ambient;
	vec4 Diffuse;
	vec4 Specular;
	vec4 Position;
	vec4 HalfVector;
	vec3 SpotDirection;
	float SpotExponent;
	float SpotCutoff;
	float SpotCosCutoff;
};

#endif