#version 330			

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

layout(binding = 0) uniform sampler2D samp;
layout(binding = 1) uniform sampler2DShadow shTex;

struct FragmentParams
{
	vec4 WorldPosition;
	vec2 TexCoord;
	vec3 Normal;
	
	vec4 color;
};

struct LightSourceParameters
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec3 position;
	vec3 halfVector;
	vec3 direction;
	float spotExponent;
	float spotCutoff;
	float spotCosCutoff;
};

uniform LightSourceParameters directionLight;

out FragmentParams fragmentParams;
out vec3 halfVector;
out vec4 shadow_coord;

uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;
uniform mat4 shadowMVP;
void main()						   
{		
	fragmentParams.WorldPosition = uModelViewMatrix * vec4(pos,1);
	fragmentParams.TexCoord = texCoord;
	fragmentParams.Normal = normal;
	
	halfVector = normalize(directionLight.direction + -fragmentParams.WorldPosition.xyz);
	
	shadow_coord = shadowMVP * vec4(pos, 1.0);
	gl_Position = uProjectionMatrix * uModelViewMatrix * vec4(pos,1);
}   
