#version 330						   
                                      
out vec4 color;	
		
struct FragmentParams
{
	vec4 WorldPosition;
	vec2 TexCoord;
	vec3 Normal;
	
	vec4 color;
};
				 
in FragmentParams fragmentParams;
in vec3 halfVector;
layout(binding = 0) uniform sampler2D samp;

struct MaterialParameters
{
	vec4 emision;
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

struct LightSourceParameters
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec3 position;
	vec3 halfVector;
	vec3 direction;
	float spotExponent;
	float spotCutoff;
	float spotCosCutoff;
};

uniform LightSourceParameters directionLight;
uniform MaterialParameters material;
uniform vec3 cameraPosition;

void main()						  
{	
								   
	color = texture(samp, fragmentParams.TexCoord);
	vec3 ambient = material.ambient.rgb * directionLight.ambient.rgb * material.ambient.a * directionLight.ambient.a;
	vec3 diffuse = material.diffuse.rgb * directionLight.diffuse.rgb * material.diffuse.a * directionLight.diffuse.a * max(dot(directionLight.direction, fragmentParams.Normal), 0);
	vec3 specular = material.specular.rgb * directionLight.specular.rgb * material.specular.a * directionLight.specular.a * pow(max(dot(halfVector, fragmentParams.Normal), 0), material.shininess * 3.0);
	
	color.rgb *= ambient + diffuse + specular;
	//color = vec4(1); 
}	