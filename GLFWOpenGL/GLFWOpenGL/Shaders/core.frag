#version 330						   
                                      
out vec4 color;					   
in vec3 aColor;

void main()						  
{									   
	color = vec4(aColor, 1.0); 
	//color = vec4(1); 
}	