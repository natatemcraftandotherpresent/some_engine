#version 330			

layout(location = 0) in vec3 pos;

uniform mat4 shadowMVP;

void main()						   
{		
	gl_Position = shadowMVP * vec4(pos,1);
}   
