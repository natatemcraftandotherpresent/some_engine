#version 330						   
                                      
out vec4 color;	
		
struct FragmentParams
{
	vec4 WorldPosition;
	vec2 TexCoord;
	vec3 Normal;
	
	vec4 color;
};
				 
in FragmentParams fragmentParams;
in vec3 halfVector;
in vec4 shadow_coord;

layout(binding = 0) uniform sampler2D samp;
layout(binding = 1) uniform sampler2D shTex;

struct MaterialParameters
{
	vec4 emision;
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

struct LightSourceParameters
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec3 position;
	vec3 halfVector;
	vec3 direction;
	float spotExponent;
	float spotCutoff;
	float spotCosCutoff;
};

uniform LightSourceParameters directionLight;
uniform MaterialParameters material;

float CalcDirectionShadowFactor()
{
	vec3 projCoords = shadow_coord.xyz / shadow_coord.w;
	projCoords = (projCoords * 0.5) + 0.5;
	float current = projCoords.z;
	
	float shadow = 0;
	vec2 texelSize = 1.0 / textureSize(shTex, 0);
	
	for(int x = -2; x <= 2; ++x)
	{
		for(int y = -2; y <= 2; ++y)
		{
			float pcfDepth = texture(shTex, projCoords.xy + vec2(x, y) * texelSize).r;
			shadow += current > pcfDepth && current <= 1 ? 0.0 : 1.0;
		}
	}
	
	shadow /= 25.0;
	
	return shadow;
}

void main()						  
{	
	vec3 ambient = vec3(0);
	vec3 diffuse = vec3(0);
	vec3 specular = vec3(0);
	color = texture(samp, fragmentParams.TexCoord);
	ambient = material.ambient.rgb * directionLight.ambient.rgb * material.ambient.a * directionLight.ambient.a;
	
	vec3 e = vec3(0, 0, -1);
	
	float notInShadow = CalcDirectionShadowFactor();
	if(notInShadow)
	{
		float dl = dot(e, normalize(directionLight.direction));
		float de = dot(e, normalize(-fragmentParams.WorldPosition.xyz));
		diffuse = material.diffuse.rgb * directionLight.diffuse.rgb * material.diffuse.a * directionLight.diffuse.a * max(dot(normalize(directionLight.direction), normalize(fragmentParams.Normal)), 0) * notInShadow;
		specular = material.specular.rgb * directionLight.specular.rgb * material.specular.a * directionLight.specular.a * pow(dl*de + sqrt(1. - dl*dl) * sqrt(1. - de*de), material.shininess)* notInShadow;
	}
	color.rgb *= ambient + diffuse + specular;
}	