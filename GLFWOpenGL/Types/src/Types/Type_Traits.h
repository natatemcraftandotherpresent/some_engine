#pragma once
#ifndef TYPE_TRAITS
#define TYPE_TRAITS

namespace Types
{
	// is floating point
	//template<typename X, typename = void>
	//struct is_floating_point
	//{
	//	enum
	//	{
	//		constexpr value = false
	//	};
	//};
	//
	//template<typename T>
	//struct is_floating_point<T, std::enable_if_t<!std::is_integral_v<T>>>
	//{
	//	enum
	//	{
	//		constexpr value = true
	//	};
	//};
	//
	//template<typename T>
	//using is_floating_point_v = is_floating_point<T>::value;

	// remove constant
	template<typename X>
	struct remove_constant
	{
		constexpr using type = X;
	};

	template<typename X>
	struct remove_constant<const X>
	{
		constexpr using type = X;
	};

	template<typename X>
	using remove_constant_t = remove_constant<X>::type;

	// casts
	template<typename TO, typename FROM>
	constexpr auto cast_to(FROM value) noexcept -> decltype(static_cast<TO>(value))
	{
		return static_cast<TO>(value);
	}
	template<typename TO, typename FROM>
	constexpr auto cast_to(FROM* value) noexcept -> decltype(dynamic_cast<TO>(value))
	{
		return dynamic_cast<TO>(value);
	}
}

#endif //TYPE_TRAITS