#pragma once
#ifndef ENGINE_TYPES
#define ENGINE_TYPES

#include "PCH.h"

#define BIT(x) (1 << x)
#define random_real(MIN, MAX) MIN + (rand() / ( RAND_MAX / (MAX-MIN) ) )

namespace Types
{
	struct PlatformTypes
	{
		using int8 = signed char;
		using int16 = signed short;
		using int32 = signed int;
		using int64 = signed long long;

		using uint8 = unsigned char;
		using uint16 = unsigned short;
		using uint32 = unsigned int;
		using uint64 = unsigned long long;

		using byte = unsigned char;

#ifndef EC_REAL_DOUBLE
		using real = float;
#else
		using real = double;
#endif // EC_REAL_DOUBLE
	};

	typedef PlatformTypes::int8	 int8;
	typedef PlatformTypes::int16 int16;
	typedef PlatformTypes::int32 int32;
	typedef PlatformTypes::int64 int64;

	typedef PlatformTypes::uint8  uint8;
	typedef PlatformTypes::uint16 uint16;
	typedef PlatformTypes::uint32 uint32;
	typedef PlatformTypes::uint64 uint64;

	typedef PlatformTypes::real real;
	typedef PlatformTypes::byte byte;

	inline int16 sawp_bites(int16 value)
	{
		return (value & 0x00FF << 8) | (value & 0xFF00 >> 8);
	}

	inline int32 sawp_bites(int32 value)
	{
		return (value & 0x000000FF << 24) |
			(value & 0x0000FF00 << 8) |
			(value & 0x00FF0000 >> 8) |
			(value & 0xFF000000 >> 24);
	}

	inline int64 sawp_bites(int64 value)
	{
		return (value & 0x00000000FFFFFFFF) << 32 | (value & 0xFFFFFFFF00000000) >> 32 |
			   (value & 0x0000FFFF0000FFFF) << 16 | (value & 0xFFFF0000FFFF0000) >> 16 |
			   (value & 0x00FF00FF00FF00FF) << 8  | (value & 0xFF00FF00FF00FF00) >> 8;
	}

	inline real SafeFRealDivision(const real a, const real b, const real def)
	{
#ifdef EC_REAL_DOUBLE
		uint64 condition = (uint64)(b != 0.0f);
		uint64 mask = (uint64)0 - condition;

		union RaU
		{
			uint64 uint;
			real rl;
		};
#else
		uint32 condition = (uint32)(b != 0.0f);
		uint32 mask = (uint32)0 - condition;

		union RaU
		{
			uint32 uint;
			real rl;
		};
#endif
		RaU answ;
		answ.rl = a / b;
		RaU defAnsw;
		defAnsw.rl = def;

		return static_cast<real>((answ.uint & mask) | (defAnsw.uint & (~mask)));
	}
}

typedef Types::PlatformTypes::int8	 int8;
typedef Types::PlatformTypes::int16 int16;
typedef Types::PlatformTypes::int32 int32;
typedef Types::PlatformTypes::int64 int64;

typedef Types::PlatformTypes::uint8  uint8;
typedef Types::PlatformTypes::uint16 uint16;
typedef Types::PlatformTypes::uint32 uint32;
typedef Types::PlatformTypes::uint64 uint64;

typedef Types::PlatformTypes::real real;
typedef Types::PlatformTypes::byte byte;

#endif ENGINE_TYPES