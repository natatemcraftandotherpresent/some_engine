#pragma once
#ifndef TYPES_CORE
#define TYPES_CORE

#ifdef EC_PLATFORM_WINDOWS
	#ifdef EC_EXPORT
		#define TYPES_API __declspec(dllexport)
	#else 
		#define TYPES_API __declspec(dllimport)
	#endif // LIB_EXPORT
#else
	#error Types supports Windows X64!
#endif

#endif