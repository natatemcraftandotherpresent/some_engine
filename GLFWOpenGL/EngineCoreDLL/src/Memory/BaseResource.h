#pragma once
#ifndef ENGINE_CORE_MEMORY_BASE_RESOURCE
#define ENGINE_CORE_MEMORY_BASE_RESOURCE

#include "PCH.h"
#include "EngineCoreDLL\Core.h"
#include "EngineCoreDLL\Tags.h"
namespace EngineCore
{
	namespace Memory
	{
		class EC_API BaseResource
		{
		public:
			CREATE_TAG(tagResource)

			enum PriorityType
			{
				RES_LOW_PRIORITY = 0,
				RES_MED_PRIORITY,
				RES_HIGH_PRIORITY
			};

			BaseResource() { Clear(); }
			virtual ~BaseResource() 
			{ 
				Destroy(); 
			}

			virtual void Clear()
			{
				refCount = 0;
				lastAccess = time_t();
				priorityType = RES_LOW_PRIORITY;
			}

			virtual bool Create() { return false; }
			virtual bool Destroy() { return true; };

			//virtual bool Recreate() = 0;
			virtual void Dispose() 
			{
			}
			//
			virtual size_t GetSize() const { return sizeof(this); }
			//virtual bool IsDisposed() = 0;

			inline void SetPriority(PriorityType prType)
			{
				priorityType = prType;
			}
			inline PriorityType GetPriority() const { return priorityType; }

			inline void Lock()
			{
				refCount += 1;
			}
			inline const unsigned GetReferenceCount() const { return refCount; }
			inline bool IsLocked() { return refCount > 0; }
			inline void Unlock()
			{
				if (refCount > 0)
				{
					refCount -= 1;
				}
			}

			inline void SetLastAccess(time_t lastAccess)
			{
				this->lastAccess = lastAccess;
			}
			inline time_t GetLastAccess() const { return lastAccess; }

			virtual bool operator < (BaseResource& container)
			{
				if (GetPriority() < container.GetPriority())
				{
					return true;
				}
				else if (GetPriority() > container.GetPriority())
				{
					return false;
				}
				else
				{
					if (lastAccess < container.GetLastAccess())
					{
						return true;
					}
					else if (lastAccess > container.GetLastAccess())
					{
						return false;
					}
					else
					{
						if (GetSize() < container.GetSize())
						{
							return true;
						}
					}
				}
				return false;
			}

		private:
			PriorityType priorityType;
			unsigned refCount;

			time_t lastAccess;
		};
	}
}
#endif