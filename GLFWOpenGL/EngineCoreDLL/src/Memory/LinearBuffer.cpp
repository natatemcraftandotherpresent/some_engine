#include "PCH.h"
#include "LinearBuffer.h"
#include "MemoryStorage.h"
#include "BaseResource.h"

namespace EngineCore
{
	namespace Memory
	{
		MemoryStorage* LinearBufferAlloc(LinearBuffer * buffer, size_t size)
		{
			if (buffer && size)
			{
				size_t newOffset = buffer->offset + size;
				if (newOffset <= buffer->totalSize)
				{
					void* ptr = buffer->mem + buffer->offset ;
					buffer->ptrs.push_back(std::move(MemoryStorage(ptr, size , buffer->offset )));
					buffer->offset = newOffset;
					return &(buffer->ptrs.back());
				}
			}
			return nullptr;
		}
		void LinearBufferFree(LinearBuffer * buffer, MemoryStorage * storage)
		{
			auto it = std::find(buffer->ptrs.begin(), buffer->ptrs.end(), storage);
			if (it != buffer->ptrs.end())
			{
				static_cast<BaseResource*>(storage->memoryPtr)->Dispose();
				if (it != buffer->ptrs.end() - 1 && (it + 1)->memoryPtr != nullptr)
				{
					if (it != buffer->ptrs.begin())
					{
						buffer->MoveVector(it, (it - 1)->offset + (it - 1)->size);
					}
					else
					{
						buffer->MoveVector(it, 0);
					}
				}
				else
				{
					//memset(it->memoryPtr, 0x0, it->size);
					it->memoryPtr = nullptr;
				}
				buffer->offset -= storage->size;
				storage = nullptr;
				//buffer->ptrs.erase(it);
			}
		}
	}
}
