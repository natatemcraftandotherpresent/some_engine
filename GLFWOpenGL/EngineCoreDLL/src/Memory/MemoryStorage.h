#pragma once
#ifndef ENGINE_CORE_MEMORY_STORAGE
#define ENGINE_CORE_MEMORY_STORAGE
#include "PCH.h"
#include "Managers/Handle/Handle.h"
#include "Memory/BaseResource.h"
#include "EngineCoreDLL\Core.h"
namespace EngineCore
{
	namespace Memory
	{
		struct EC_API MemoryStorage
		{
			friend struct LinearBuffer;
			extern friend EC_API MemoryStorage* LinearBufferAlloc(LinearBuffer* buffer, size_t size);
			extern friend EC_API void LinearBufferFree(LinearBuffer* buffer, MemoryStorage* storage);
		private:
			size_t size;
			size_t offset;
			void* memoryPtr;
			Handle<BaseResource::tagType> handle;

			void Clear()
			{
				size = 0;
				offset = 0;
			}
		public:
			MemoryStorage() : memoryPtr(nullptr), size(0), offset(0), handle() {}
			
			MemoryStorage(void* memoryPtr, size_t size, size_t offset, Handle<BaseResource::tagType> handle = Handle<BaseResource::tagType>()) : memoryPtr(memoryPtr), size(size), offset(offset), handle(handle) {}

			MemoryStorage(const MemoryStorage& storage)
			{
				memoryPtr = storage.memoryPtr;
				size = storage.size;
				offset = storage.offset;
				handle = storage.handle;
			}

			MemoryStorage(MemoryStorage&& storage) noexcept
			{
				memoryPtr = storage.memoryPtr;
				size = storage.size;
				offset = storage.offset;
				handle = storage.handle;

				storage.offset = 0;
				storage.memoryPtr = nullptr;
				storage.size = 0;
			}

			bool operator==(const MemoryStorage* storage)
			{
				return size == storage->size && memoryPtr == storage->memoryPtr && offset == storage->offset;
			}

			MemoryStorage& operator=(MemoryStorage&& storage)
			{
				memoryPtr = storage.memoryPtr;
				size = storage.size;
				offset = storage.offset;
				handle = storage.handle;

				storage.offset = 0;
				storage.memoryPtr = nullptr;
				storage.size = 0;

				return *this;
			}

			MemoryStorage& operator=(const MemoryStorage& storage)
			{
				memoryPtr = storage.memoryPtr;
				size = storage.size;
				offset = storage.offset;
				handle = storage.handle;

				return *this;
			}

			~MemoryStorage()
			{
				Clear();
			}

			inline const size_t GetSize()const { return size; }
			inline const size_t GetOffset() const { return offset; }
			inline Handle<BaseResource::tagType>& GetHandle() { return handle; }
			inline void* const GetMemoryPointer() const { return memoryPtr; }
			inline void** const GetPointerMemoryPointer() { return &memoryPtr; }
			inline MemoryStorage* GetSelfPoint() { return this; }
		};
	}
}
#endif