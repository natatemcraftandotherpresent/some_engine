#pragma once
#ifndef ENGINE_CORE_MEMORY
#define ENGINE_CORE_MEMORY

#include "PCH.h"
#include "Types\Types.h"
#include "MemoryStorage.h"
#include "EngineCoreDLL\Core.h"
namespace EngineCore
{
	namespace Memory
	{
		struct EC_API LinearBuffer
		{
			using MemoryStorage_iterator = std::vector<MemoryStorage>::iterator;
			LinearBuffer() : offset(0), totalSize(0), mem(nullptr) {};
			LinearBuffer(byte* mem, size_t totalSize) : totalSize(totalSize), offset(0), mem(mem) { 
				 ptrs.reserve(totalSize / sizeof(MemoryStorage));
			};

			~LinearBuffer()
			{
			}
			inline void Create(byte* mem, size_t totalSize) { this->totalSize = totalSize; this->mem = mem; 
			 ptrs.reserve(totalSize / sizeof(MemoryStorage));
			}

			inline bool IsFreeMem(size_t& mem) const 
			{
				return (offset + mem) <= totalSize;
			}

			inline std::vector<MemoryStorage>* GetPtrs() { return &ptrs; }

		private:
			inline void MoveVector(MemoryStorage_iterator iterator, const int first_offset = 0)
			{
				memmove_s(iterator->memoryPtr, (iterator + 1)->size, (iterator + 1)->memoryPtr, (iterator + 1)->size);
				(iterator + 1)->offset = first_offset;
				(iterator + 1)->memoryPtr = (byte*)(iterator)->memoryPtr;
				iterator->memoryPtr = nullptr;
				++iterator;
				for (; iterator != this->ptrs.end() - 1; ++iterator)
				{
					memmove_s(((byte*)iterator->memoryPtr + iterator->size), (iterator + 1)->size, (iterator + 1)->memoryPtr, (iterator + 1)->size);
					(iterator + 1)->offset = (iterator)->offset + (iterator)->size;
					(iterator + 1)->memoryPtr = (byte*)(iterator)->memoryPtr + iterator->size;
				}
			}

			byte* mem;
			size_t totalSize;
			size_t offset;
			std::vector<MemoryStorage> ptrs;

			friend MemoryStorage* LinearBufferAlloc(LinearBuffer* buffer, size_t size);
			friend void LinearBufferFree(LinearBuffer* buffer, MemoryStorage* storage);
		};

		EC_API MemoryStorage* LinearBufferAlloc(LinearBuffer* buffer, size_t size);
		EC_API void LinearBufferFree(LinearBuffer* buffer, MemoryStorage* storage);
	}
}
#endif