#pragma once
#ifndef ENGINE_CORE_DLL_EXPORT_CONTENT
#define ENGINE_CORE_DLL_EXPORT_CONTENT

#include "EngineCoreDLL\Precision.h"
#include "Types\Types.h"
#include "Events\Event.h"

#include "EngineCoreDLL\Component.h"

#include "Memory\LinearBuffer.h"
#include "Memory\MemoryStorage.h"

typedef EngineCore::Component Component;

namespace ECMemory = EngineCore::Memory;
#endif