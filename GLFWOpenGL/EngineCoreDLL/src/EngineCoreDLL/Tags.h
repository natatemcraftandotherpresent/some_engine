#pragma once
#ifndef ENGINE_CORE_TAGS
#define ENGINE_CORE_TAGS

#define CREATE_TAG(tagName) struct tagName {}; \
							using tagType = tagName;
#endif