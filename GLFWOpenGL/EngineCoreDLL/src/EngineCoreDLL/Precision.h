#pragma once
#ifndef ENGINE_CORE_PRECISION
#define ENGINE_CORE_PRECISION

//#include "Core.h"

#ifdef EC_DEBUG
	#define ASSERT_PRINT(check, string) printf("%s %s %s %d", #check, string, __FILE__, __LINE__)
	#define debugbreak() __debugbreak()
	#define Assert(check, string) \
				if(check){}\
				else\
				{\
					ASSERT_PRINT(check, string);\
					debugbreak();\
				}
	#define Static_Assert(check, string) static_assert(check, string)
#else
	#define Assert(check, string)
	#define Static_Assert(check, string)
#endif // EC_DEBUG
#endif // ENGINE_CORE_PRECISION