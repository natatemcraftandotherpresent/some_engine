#pragma once
#ifndef ENGINE_CORE_COMPONENT
#define ENGINE_CORE_COMPONENT

#include "PCH.h"
#include "Memory\BaseResource.h"
#include "Memory\MemoryStorage.h"
#include "Managers/Handle/Handle.h"
#include "EngineCoreDLL\Core.h"

namespace EngineCore
{
	class EngineObject;
	struct EC_API Component : public Memory::BaseResource
	{
		using handle = Memory::BaseResource::tagType;
	public:
		virtual ~Component() = default;
		inline virtual void SetParent(void* parent) { this->parent = static_cast<EngineObject*>(parent); }

		virtual void SetData(void* parent, const Handle<handle>& handle, EngineCore::Memory::MemoryStorage* stor)
		{
			if (parent)
			{
				this->parent = static_cast<EngineObject*>(parent);
			}
			if (stor)
			{
				storage = stor;
			}
			handlResource = handle;
		}
	protected:
		EngineObject* parent;
		EngineCore::Memory::MemoryStorage* storage;
		Handle<handle> handlResource;
	};
}
#endif