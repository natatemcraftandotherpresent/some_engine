#pragma once
#ifndef ENGINE_CORE
#define ENGINE_CORE

#ifdef EC_PLATFORM_WINDOWS
	#ifdef EC_EXPORT
		#define EC_API __declspec(dllexport)
	#else
		#define EC_API __declspec(dllimport)
	#endif
#endif

#endif