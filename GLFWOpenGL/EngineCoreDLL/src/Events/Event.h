#pragma once
#ifndef ENGINE_CORE_EVENT
#define ENGINE_CORE_EVENT

#include "PCH.h"
#include "Types/Types.h"
#include "EngineCoreDLL/Core.h"
namespace EngineCore
{
	enum class EventType
	{
		None = 0,
		WindowClose, WindowResize, WindowFocus, WindowLostFocus, WindowMoved,
		AppTick, AppUpdate, AppRender,
		KeyPressed, KeyReleased,
		MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScroll
	};

	enum class EventCategory
	{
		//None = 0,
		EventCategoryApplication = BIT(0),
		EventCategoryInput = BIT(1),
		EventCategoryKeyboard = BIT(2),
		EventCategoryMouse = BIT(3),
		EventCategoryMouseButton = BIT(4)
	};

#define EVENT_CLASS_TYPE(type) static EventType GetStaticType() {return EventType::##type;} \
								virtual EventType GetEventType() const override {return GetStaticType();} \
								virtual const char* GetName() const override {return #type;}

#define EVENT_CLASS_CATEGORY(category) virtual int32 GetCategoryFlags() const override {return category;}

	class EC_API Event
	{
	public:
		virtual EventType GetEventType() const = 0;
		virtual int32 GetCategoryFlags() const = 0;
		virtual const char* GetName() const = 0;

#ifdef EC_DEBUG
		virtual const char* ToString() { return GetName(); }
#endif // EC_DEBUG

		inline bool IsInCategory(EventCategory category) { return GetCategoryFlags() & (int32)category; }
	protected:

		inline void SetHandled(bool handl) { handled = handl; }
		inline bool IsHandled() { return handled; }
	private:
		bool handled = false;
	};
}
#endif // ENGINE_CORE_EVENT