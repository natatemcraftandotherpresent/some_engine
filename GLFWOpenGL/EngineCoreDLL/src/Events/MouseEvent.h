#pragma once
#ifndef ENGINE_CORE_MOUSE_EVENT
#define ENGINE_CORE_MOUSE_EVENT

#include "PCH.h"
#include "EngineCore/Core.h"
#include "Event.h"
namespace EngineCore
{
	class EC_API MouseMovedEvent : public Event
	{
	public:
		EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryInput)
		EVENT_CLASS_TYPE(MouseMoved)

		MouseMovedEvent(float x, float y) : mouseX(x), mouseY(y) {}

		inline float GetX() const { return mouseX; }
		inline float GetY() const { return mouseY; }

#ifdef EC_DEBUG
		virtual const char* ToString() override
		{
			std::stringstream ss;
			ss << "MouseMovedEvent: " << mouseX << " " << mouseY;
			return ss.str().c_str();
		}
#endif // EC_DEBUG

	private:
		float mouseX, mouseY;
	};

	class EC_API MouseScrollEvent : public Event
	{
	public:
		EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryInput)
		EVENT_CLASS_TYPE(MouseScroll)

		MouseScrollEvent(float xOffset, float yOffset) : xOffset(xOffset), yOffset(yOffset) {}

#ifdef EC_DEBUG
		virtual const char* ToString() override
		{
			std::stringstream ss;
			ss << "MouseScrollEvent: " << xOffset << " " << yOffset;
			return ss.str().c_str();
		}
#endif // EC_DEBUG
	private:
		float xOffset, yOffset;
	};

	class EC_API MouseButtonEvent : public Event
	{
	public:
		EVENT_CLASS_CATEGORY(EventCategoryMouse | EventCategoryMouseButton | EventCategoryInput)
		inline int32 GetButton() const { return button; }
	protected:
		MouseButtonEvent(int32 button) : button(button) {}
	private:
		int32 button;
	};

	class EC_API MouseButtonPressedEvent : public MouseButtonEvent
	{
	public:
		EVENT_CLASS_TYPE(MouseButtonPressed)
		MouseButtonPressedEvent(int32 button) : MouseButtonEvent(button) {}

#ifdef EC_DEBUG
		virtual const char* ToString() override
		{
			std::stringstream ss;
			ss << "MouseButtonPressedEvent: " << GetButton();
			return ss.str().c_str();
		}
#endif // EC_DEBUG
	};

	class EC_API MouseButtonReleasedEvent : public MouseButtonEvent
	{
	public:
		EVENT_CLASS_TYPE(MouseButtonReleased)
		MouseButtonReleasedEvent(int32 button) : MouseButtonEvent(button) {}

#ifdef EC_DEBUG
		virtual const char* ToString() override
		{
			std::stringstream ss;
			ss << "MouseButtonReleasedEvent: " << GetButton();
			return ss.str().c_str();
		}
#endif // EC_DEBUG
	};
}
#endif // ENGINE_CORE_MOUSE_EVENT

