#pragma once
#ifndef ENGINE_CORE_KEY_EVENT
#define ENGINE_CORE_KEY_EVENT

#include "PCH.h"
#include "Event.h"
#include "EngineCore/Core.h"
namespace EngineCore
{
	class EC_API KeyEvent : public Event
	{
	public:
		EVENT_CLASS_CATEGORY(EventCategoryKeyboard | EventCategoryInput)
		inline int32 GetKeyCode() { return keyKode; }
	protected:
		KeyEvent(int32 keyCode) : keyKode(keyCode) {}
	private:
		int32 keyKode;
	};

	class EC_API KeyPressedEvent : public KeyEvent
	{
	public:
		EVENT_CLASS_TYPE(KeyPressed)

		KeyPressedEvent(int32 keyKode, int32 repeatCount) : KeyEvent(keyKode), repeatCount(repeatCount) {}
		inline int32 GetRepeatCount() { return repeatCount; }

#ifdef EC_DEBUG
		virtual const char* ToString() override
		{
			std::stringstream ss;
			ss << "KeyPressedEvent: " << GetKeyCode() << " (" << repeatCount << ")";
			return ss.str().c_str();
		}
#endif // EC_DEBUG

	private:
		int32 repeatCount;
	};

	class EC_API KeyReleasedEvent : public KeyEvent
	{
	public:
		EVENT_CLASS_TYPE(KeyReleased)

		KeyReleasedEvent(int32 keyCode) : KeyEvent(keyCode) {}

#ifdef EC_DEBUG
		virtual const char* ToString() override
		{
			std::stringstream ss;
			ss << "KeyReleasedEvent: " << GetKeyCode();
			return ss.str().c_str();
		}
#endif // EC_DEBUG
	};
}
#endif // ENGINE_CORE_KEY_EVENT

