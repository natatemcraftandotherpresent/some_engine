#include "Application.h"
#include "Rendering.h"
#include "TestObject.h"
#include "TestObject2.h"
#include "Light/DirectionLight.h"
#include "glm/glm.hpp"
#include "Time/Time.h"
#include "EngineCore/FunctionDelay.h"
#include "Mesh/MeshRegistry.h"


void Print(int a, char b)
{
	std::cout << a << " " << b;
}

Application::Application() : renderEngine(1280, 720)
{
	engineCamera = eor.GetSingleton().CreateObject<Graphics::EngineCamera>();
	(*engineCamera)->SetProjectionMatrix(glm::perspective(glm::radians(45.f), renderEngine.GetCurrentWindow()->GetAspect(), 0.1f, 1000.0f));
	(*EngineCore::EngineObjectRegistry::GetSingleton().CreateObject<DirectionLight>())->Create(glm::vec3(0, -7, 1), glm::vec4(1, 0, 0, 1.0f), glm::vec4(0, 0, 1, 1), glm::vec4(1, 1.0, 1.0, 1));
	auto obj = EngineCore::EngineObjectRegistry::GetSingleton().CreateObject<TestObject>();
	auto obj2 = EngineCore::EngineObjectRegistry::GetSingleton().CreateObject<TestObject2>();
}

void Application::Update()
{
	while (renderEngine.Update())
	{
		auto deltaTime = Time::deltaTime();
		eor.GetSingleton().Update(deltaTime);
	}
}
