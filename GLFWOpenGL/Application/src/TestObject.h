#pragma once
#include "Object/WorldObject.h"
#include "EngineCore.h"
#include "Constant.h"

#include "Materials/DefaultMaterial.h"

class TestObject : public WorldObject
{
public:
	virtual void Start() override;
	virtual void Update(real duration) override;

private:
	Transform* transform;
	Graphics::DefaultMaterial defMat;
	//ASPhysics::Force::ParticleGravity gravity = ASPhysics::Force::ParticleGravity(Constants::Physics::GetGravity());
	//ASPhysics::Force::ParticleDrag drag = ASPhysics::Force::ParticleDrag(.2f, .5f);
};