#include "PCH.h"
#include "TestObject2.h"
#include "Vector/Vector3.h"
#include "Mesh\Mesh.h"
#include "Mesh\MeshFilter\Cube.h"
#include "Textures\Texture.h"
#include "Materials\Material.h"
#include "Mesh\MeshFilter\Sphere.h"

void TestObject2::Start()
{
	transform = GetComponent<Transform>();
	transform->SetPosition(0, 0, -2);
	auto mesh = AddComponent<Graphics::Mesh>();
	mesh->AddMeshFilter(Constants::Meshes::GetSphere());
	mesh->AddTexture(Graphics::Texture("..\\GLFWOpenGL\\tex.jpg").GetTexture());
	defMat.SetMaterialShadows(Graphics::MaterialShadows::Default);
	mesh->material = &defMat;
	auto rb = AddComponent<RigidBody>();
	transform->SetRotation(0, -90, 0);
}

void TestObject2::Update(real duration)
{
	auto position = transform->GetPosition() + Vector3(0, 0, -1) * duration;
	//std::cout << "Obj: " << transform->GetForwardVector().toString() << std::endl;
	transform->SetPosition(position);
}
