#pragma once
#include "Object/WorldObject.h"
#include "EngineCore.h"
#include "Constant.h"
#include "Materials/DefaultMaterial.h"

class TestObject2 : public WorldObject
{
public:
	virtual void Start() override;
	virtual void Update(real duration) override;

private:
	Transform* transform;
	Graphics::DefaultMaterial defMat;
};