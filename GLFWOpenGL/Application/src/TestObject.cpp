#include "PCH.h"
#include "TestObject.h"
#include "Mesh\Mesh.h"
#include "Mesh\MeshFilter\Cube.h"
#include "Textures\Texture.h"
#include "Materials\Material.h"

void TestObject::Start()
{
	transform = GetComponent<Transform>();
	transform->SetPosition(0, -2, -4);
	transform->SetScale(15, 0.2, 15);
	auto mesh = AddComponent<Graphics::Mesh>();
	mesh->AddMeshFilter(Constants::Meshes::GetCube());
	mesh->AddTexture(Graphics::Texture("..\\GLFWOpenGL\\tex.jpg").GetTexture());
	//mesh->material = Graphics::Material::GetDefaultMaterial();
	defMat.SetMaterialShadows(Graphics::MaterialShadows::Default);
	mesh->material = &defMat;
	//auto rg = AddComponent<RigidBody>();
	//rg->SetMass(0.25f);
	//ASPhysics::Force::ParticleForceRegestry::Add(rg, &gravity);
	//ASPhysics::Force::ParticleForceRegestry::Add(rg, &drag);
}

void TestObject::Update(real duration)
{
	//auto position = transform->GetPosition() + Vector3(0, 1, 0) * duration;
	//transform->SetPosition(position);
}
