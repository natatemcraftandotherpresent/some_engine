#pragma once
#include "Rendering.h"
#include "EngineCoreDLL.h"
#include "EngineCore.h"
#include "Camera/EngineCamera.h"

class Application
{
public:
	Application();

	void Update();

	~Application()
	{

	}

private:
	Graphics::Rendering renderEngine;

	EngineCore::Profiler profiler;
	EngineCore::ConsoleBuffer conBuff;
	EngineCore::Debug debug;
	EngineCore::EngineObjectRegistry eor;

	Graphics::EngineCamera** engineCamera;
};

