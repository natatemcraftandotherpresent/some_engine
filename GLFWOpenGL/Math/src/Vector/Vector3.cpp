#include "PCH.h"
#include "Vector3.h"
namespace ECMath
{
	Vector3 Vector3::Reflect(const Vector3& originVector, const Vector3& normalVector)
	{
		Vector3 odn = normalVector * ((originVector) * normalVector);
		Vector3 perpend = (originVector) - odn;

		return odn + (-perpend);
	}
	Vector3 Vector3::ComponentProduct(const Vector3 & vector, const Vector3 & vector2)
	{
		return Vector3(vector2.x * vector.x, vector2.y * vector.y, vector2.z * vector.z);
	}
	Vector3 Vector3::CrossProduct(const Vector3 & vector, const Vector3 & vector2)
	{
		return Vector3(vector.y * vector2.z - vector.z * vector2.y,
			           vector.z * vector2.x - vector.x * vector2.z,
			           vector.x * vector2.y - vector.y * vector2.x);
	}
	void Vector3::MakerOrthonormalBasis(Vector3 * a, Vector3 * b, Vector3 * c)
	{
		a->Normalize();
		*c = *a % *b;
		if (c->SquareMagnitude() == 0) { return; }

		*b = *c % *a;
	}

	const Vector3 operator*(const real value, const Vector3& vector)
	{
		return vector * value;
	}

	Vector3 operator-(const Vector3 & vector)
	{
		return Vector3(-vector.x, -vector.y, -vector.z);
	}

	Vector3 random_vector(const Vector3& vector, const Vector3& vector2)
	{
		Vector3 output;
		output.x = random_real(vector.x, vector2.x);
		output.y = random_real(vector.y, vector2.y);
		output.z = random_real(vector.z, vector2.z);

		return output;
	}
}
