#pragma  once
#ifndef VECTOR
#define VECTOR
#include "Core/Core.h"
#include "Types/Types.h"
#include "Precision/Precision.h"
namespace ECMath
{
#ifdef USE_CACHE 
	struct MATH_API Vector3Cache
	{
		real magnitudeVectorCache = 0;
		real sqrtMagnitudeVectorCache = 0;

		inline void ResetCache()
		{
			magnitudeVectorCache = 0;
			sqrtMagnitudeVectorCache = 0;
		}
	};
#endif

	struct MATH_API Vector3
	{
	public:
		Vector3() : x(0), y(0), z(0) {}
		Vector3(real value) : x(value), y(value), z(value) {};
		Vector3(real x, real y, real z) : x(x), y(y), z(z) {};

	public:
		// operators
		inline const Vector3& operator*=(const real value)
		{
			x *= value;
			y *= value;
			z *= value;
#ifdef USE_CACHE 
			cache.ResetCache();
#endif
			return (*this);
		}

		inline const Vector3 operator*(const real value) const
		{
			return Vector3(x * value, y * value, z * value);
		}
		inline const Vector3 operator*(const real value)
		{
			return static_cast<const Vector3&>(*this).operator*(value);
		}
		inline const real operator*(const Vector3& vector)
		{
			return static_cast<const Vector3&>(*this).operator*(vector);
		}
		inline const real operator*(const Vector3& vector) const
		{
			return x * vector.x + y * vector.y + z * vector.z;
		}
		friend const Vector3 operator*(const real value, const Vector3& vector);

		inline void operator/=(const real value)
		{
			x /= value;
			y /= value;
			z /= value;
#ifdef USE_CACHE 
			cache.ResetCache();
#endif
		}
		inline const Vector3 operator/(const real value)
		{
			return static_cast<const Vector3&>(*this).operator/(value);
		}
		inline const real operator/(const Vector3& vector)
		{
			return static_cast<const Vector3&>(*this).operator/(vector);
		}
		inline const Vector3 operator/(const real value) const
		{
			return Vector3(x / value, y / value, z / value);
		}
		inline const real operator/(const Vector3& vector) const
		{
			return x / vector.x + y / vector.y + z / vector.z;
		}

		inline void operator%=(const Vector3& vector)
		{
			x = y * vector.z - z * vector.y;
			y = z * vector.x - x * vector.z;
			z = x * vector.y - y * vector.x;
#ifdef USE_CACHE 
			cache.ResetCache();
#endif
		}
		inline const Vector3 operator%(const Vector3& vector)
		{
			return static_cast<const Vector3&>(*this).operator%(vector);
		}
		inline const Vector3 operator%(const Vector3& vector) const
		{
			return Vector3(y * vector.z - z * vector.y,
						   z * vector.x - x * vector.z,
						   x * vector.y - y * vector.x);
		}

		inline void operator+=(const real value)
		{
			x += value;
			y += value;
			z += value;
#ifdef USE_CACHE 
			cache.ResetCache();
#endif
		}
		inline void operator+=(const Vector3& vector)
		{
			x += vector.x;
			y += vector.y;
			z += vector.z;
#ifdef USE_CACHE 
			cache.ResetCache();
#endif
		}
		inline const Vector3 operator+(const Vector3& vector)
		{
			return static_cast<const Vector3&>(*this).operator+(vector);
		}
		inline const Vector3 operator+(const real value)
		{
			return static_cast<const Vector3&>(*this).operator+(value);
		}
		inline const Vector3 operator+(const Vector3& vector) const
		{
			return Vector3(x + vector.x, y + vector.y, z + vector.z);
		}
		inline const Vector3 operator+(const real value) const
		{
			return Vector3(x + value, y + value, z + value);
		}

		inline void operator-=(const Vector3& vector)
		{
			x -= vector.x;
			y -= vector.y;
			z -= vector.z;
#ifdef USE_CACHE 
			cache.ResetCache();
#endif
		}
		inline void operator-=(const real value) 
		{
			x -= value;
			y -= value;
			z -= value;
#ifdef USE_CACHE 
			cache.ResetCache();
#endif
		}
		friend MATH_API Vector3 operator-(const Vector3& vector);
		inline const Vector3 operator-(const real value)
		{
			return static_cast<const Vector3&>(*this).operator-(value);
		}
		inline const Vector3 operator-(const Vector3& vector)
		{
			return static_cast<const Vector3&>(*this).operator-(vector);
		}
		inline const Vector3 operator-(const real value) const
		{
			return Vector3(x - value, y - value, z - value);
		}
		inline const Vector3 operator-(const Vector3& vector) const
		{
			return Vector3(x - vector.x, y - vector.y, z - vector.z);
		}

		inline const bool operator==(const Vector3& vector)
		{
			return static_cast<const Vector3&>(*this).operator==(vector);
		}
		inline const bool operator>(const Vector3& vector)
		{
			return (static_cast<const Vector3&>(*this)).operator>(vector);
		}
		inline const bool operator<(const Vector3& vector)
		{
			return static_cast<const Vector3&>(*this).operator<(vector);
		}
		inline const bool operator==(const Vector3& vector) const
		{
			return vector.x == x && vector.y == y && vector.z == z;
		}
		inline const bool operator>(const Vector3& vector) const
		{
			return x > vector.x && y > vector.y && z > vector.z;
		}
		inline const bool operator<(const Vector3& vector) const 
		{
			return x < vector.x && y < vector.y && z < vector.z;
		}
		inline const bool operator==(const int32 value) const
		{
			return x == value && y == value && z == value;
		}
		inline const bool operator !=(const int32 value) const
		{
			return !this->operator==(value);
		}
		inline const bool operator==(const int32 value) 
		{
			return static_cast<const Vector3&>(*this).operator==(value);
		}
		inline const bool operator !=(const int32 value)
		{
			return !static_cast<const Vector3&>(*this).operator==(value);
		}

	public:
		static Vector3 Reflect(const Vector3& originVector, const Vector3& normalVector);

	public:

		static Vector3 GetGravity() { return Vector3(0, -10, 0); }
		static Vector3 ComponentProduct(const Vector3& vector, const Vector3& vector2);
		static Vector3 CrossProduct(const Vector3& vector, const Vector3& vector2);

		static void MakerOrthonormalBasis(Vector3* a, Vector3* b, Vector3* c);

		inline void Invert()
		{
			x = -x;
			y = -y;
			z = -z;
		}
		inline real Magnitude() const
		{
#ifdef USE_CACHE
			if (cache.magnitudeVectorCache == 0)
			{
				cache.magnitudeVectorCache = real_sqrt(x * x + y * y + z * z);
			}
			return cache.magnitudeVectorCache;
#endif
			return std::sqrtf(x * x + y * y + z * z);
		}
		inline real SquareMagnitude() const
		{
#ifdef USE_CACHE
			if (cache.sqrtMagnitudeVectorCache == 0)
			{
				cache.sqrtMagnitudeVectorCache = real_sqrt(x * x + y * y + z * z);
			}
			return cache.sqrtMagnitudeVectorCache;
#endif
			return x * x + y * y + z * z;
		}

		inline Vector3 normalize()
		{
			real length = Magnitude();
			if (length > 1)
			{
				return Vector3(x / length, y / length, z / length);
			}
			return *this;
		}
		inline void Normalize()
		{
			real length = Magnitude();
			if (length > 1)
			{
				x /= length;
				y /= length;
				z /= length;
			}
		}

		inline std::string toString() const
		{
			std::stringstream ss;
			ss << "X: " << x << "Y: " << y << "Z: " << z;
			return ss.str();
		}

		inline void ComponentProduct(const Vector3& vector)
		{
			x *= vector.x;
			y *= vector.y;
			z *= vector.z;
		}


		friend Vector3 random_vector(const Vector3& vector, const Vector3& vector2);

		real x;
		real y;
		real z;
		real pad;

		private:
#ifdef USE_CACHE
			mutable Vector3Cache cache;
#endif
	};
}
typedef ECMath::Vector3 Vector3;

#endif