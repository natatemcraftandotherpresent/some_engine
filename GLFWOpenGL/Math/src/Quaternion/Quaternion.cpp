#include "PCH.h"
#include "Quaternion.h"
#include "Precision/Precision.h"

namespace ECMath
{
	Quaternion::Quaternion(const real x, const real y, const real z, const real w)
	{
		CalculateData(x, y, z, w);
	}

	Quaternion::Quaternion(const Vector3& axis, const real degree)
	{
		CalculateData(axis.x, axis.y, axis.z, degree);
	}

	void Quaternion::CalculateData(const real& x, const real& y, const real& z, const real& w)
	{
		W = std::cosf(toRadians(w) / PI);

		X = x * std::sinf(toRadians(w) / PI);
		Y = y * std::sinf(toRadians(w) / PI);
		Z = z * std::sinf(toRadians(w) / PI);
	}

}
