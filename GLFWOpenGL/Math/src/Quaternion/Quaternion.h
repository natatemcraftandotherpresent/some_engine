#pragma once
#ifndef MATH_QUATERNION
#define MATH_QUATERNION
#include "PCH.h"
#include "Core/Core.h"
#include "Types/Types.h"
#include "Vector/Vector3.h"

namespace ECMath
{
#define USE_CACHE
#ifdef USE_CACHE 
	struct MATH_API QuaternionCache
	{
		real magnitudeVectorCache = 0;
		real sqrtMagnitudeVectorCache = 0;

		inline void ResetCache()
		{
			magnitudeVectorCache = 0;
			sqrtMagnitudeVectorCache = 0;
		}
	};
#endif

	class MATH_API Quaternion
	{
	public:
		// constructors
		Quaternion(const real x, const real y, const real z, const real w);
		Quaternion(const Vector3& axis, const real degree);

		//operators
		inline void operator*=(const Quaternion& quat)
		{
			const Vector3 firstVector = ToVector();
			const Vector3 secondVector = quat.ToVector();

			W = W * quat.W - secondVector * firstVector;
			const Vector3 vector = W * secondVector + quat.W * firstVector + firstVector % secondVector;
			X = vector.x;
			Y = vector.y;
			Z = vector.z;
		}
		inline Quaternion operator*(const Quaternion& quat)
		{
			const Vector3 firstVector = ToVector();
			const Vector3 secondVector = quat.ToVector();

			W = W * quat.W - secondVector * firstVector;
			const Vector3 vector = W * secondVector + quat.W * firstVector + firstVector % secondVector;
			
			return Quaternion(vector, W);
		}


		inline real Magnitude() const
		{
#ifdef USE_CACHE 
			if (cache.magnitudeVectorCache == 0)
			{
				cache.magnitudeVectorCache = real_sqrt(W * W + X * X + Y * Y + Z * Z);
			}
			return cache.magnitudeVectorCache;
#endif
			return real_sqrt(W * W + X * X + Y * Y + Z * Z);
		}
		inline real SquareMagnitude() const
		{
#ifdef USE_CACHE 
			if (cache.sqrtMagnitudeVectorCache == 0)
			{
				cache.sqrtMagnitudeVectorCache = W * W + X * X + Y * Y + Z * Z;
			}
			return cache.sqrtMagnitudeVectorCache;
#endif
			return W * W + X * X + Y * Y + Z * Z;
		}
		inline Quaternion invers() const
		{
			real magn = Magnitude();
			if (magn > 0)
			{
				Vector3 firstVector = -ToVector();
				real W = this->W / magn;
				firstVector /= magn;
				return Quaternion(firstVector, W);
			}
			return *this;
		}
		inline void Invert()
		{
			real magn = Magnitude();
			if (magn > 0)
			{
				W /= magn;
				X /= magn;
				Y /= magn;
				Z /= magn;
			}
		}

		inline const Vector3 ToVector() const
		{
			return Vector3(X, Y, Z);
		}
	private:
		inline void CalculateData(const real& x, const real& y, const real& z, const real& w);
	private:
		real W, X, Y, Z;

#ifdef USE_CACHE 
		mutable QuaternionCache cache;
#endif
	};

}
#endif // MATH_QUATERNION

