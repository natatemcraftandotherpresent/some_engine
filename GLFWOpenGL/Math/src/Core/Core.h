#pragma once
#ifndef MATH_CORE
#define MATH_CORE

#ifdef EC_PLATFORM_WINDOWS
	#ifdef EC_EXPORT
		#define MATH_API __declspec(dllexport)
	#else 
		#define MATH_API __declspec(dllimport)
	#endif // LIB_EXPORT
#else
	#error Math only supports Windows X64!
#endif

#endif