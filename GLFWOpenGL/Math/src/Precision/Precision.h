#pragma once
#include "PCH.h"
#ifndef MATH_PRECISION
#define MATH_PRECISION

#define toRadians(DEGRE) (DEGRE * 3.1415f / 180)
#define PI 3.1415f

#ifndef EC_REAL_DOUBLE
	#define real_sqrt(value) std::sqrtf(value)
	#define real_cos(value) std::cosf(value)
	#define real_exp(value) std::expf(value)
#else
	#define real_sqrt(value) std::sqrt(value)
	#define real_cos(value) std::cos(value)
	#define real_exp(value) std::exp(value)
#endif

#endif // MATH_PRECISION