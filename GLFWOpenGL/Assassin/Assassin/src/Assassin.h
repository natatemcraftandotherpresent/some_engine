#pragma once
#ifndef ASSASSIN_IMPORT_HEADERS
#define ASSASSIN_IMPORT_HEADERS

#include "Physics.h"
#include "Assassin/Core/Particle.h"

#include "Assassin/ForceGenerator/ParticleForceRegistry.h"
#include "Assassin/ForceGenerator/Force/ParticleDrag.h"
#include "Assassin/ForceGenerator/Force/ParticleGravity.h"
#include "Assassin/ForceGenerator/Force/ParticleSpring.h"
#include "Assassin/ForceGenerator/Force/ParticleAncoredSpring.h"
#include "Assassin/ForceGenerator/Force/ParticleBungee.h"
#include "Assassin/ForceGenerator/Force/ParticleBuoyancy.h"
#include "Assassin/ForceGenerator/Force/ParticleFakeSpring.h"

#include "Math/VectorCalculations.h"

#include "Components/Transform.h"

#include "Collision/SphereCollider.h"
#include "Collision/BoxCollider.h"
#include "Collision/ParticleContact.h"

#define ASMath Assassin::Math
#define ASPhysics Assassin

typedef Assassin::Particle RigidBody;
typedef Assassin::Transform Transform;
typedef Assassin::SphereCollider SphereCollider;
typedef Assassin::BoxCollider BoxCollider;

#endif