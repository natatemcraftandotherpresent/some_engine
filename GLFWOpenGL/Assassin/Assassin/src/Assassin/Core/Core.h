#pragma once
#ifndef ASSASSIN_CORE
#define ASSASSIN_CORE

#ifdef EC_PLATFORM_WINDOWS
	#ifdef EC_EXPORT
		#define ASSA_API __declspec(dllexport)
	#else 
		#define ASSA_API __declspec(dllimport)
	#endif // LIB_EXPORT
#else
	#error Assassin only supports Windows X64!
#endif

#endif