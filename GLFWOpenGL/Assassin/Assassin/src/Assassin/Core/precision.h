#pragma once
#ifndef ASSASSIN_PRECISION
#define ASSASSIN_PRECISION

#include "PCH.h"
namespace Assassin
{
#define real_pow std::powf
#define real_sqrt std::sqrtf
#define real_abs std::fabs
#define random_real(MIN, MAX) MIN + (rand() / ( RAND_MAX / (MAX-MIN) ) )
#define real_sin std::sinf
#define real_cos std::cosf
#define real_exp std::expf

	typedef float real;

#define toRadians(DEGRE) DEGRE * 3.1415 / 180

}
#endif