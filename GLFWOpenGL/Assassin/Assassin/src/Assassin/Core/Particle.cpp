#pragma once
#include "PCH.h"
#include "Particle.h"
namespace Assassin
{
	void Particle::Integrate(const real duration)
	{
		auto position = transform->GetPosition();
		if (inversMass <= 0)
		{
			return;
		}

		assert(duration > 0.0f);


		Vector3 resultingAcc = acceleration;

		resultingAcc += forceAccum;

		velocity += resultingAcc * duration;

		position += velocity * duration;

		transform->SetPosition(position);
		ClearForce();
	}

	std::string Particle::ToString() const
	{
		std::stringstream str;

		
		return str.str();
	}

}