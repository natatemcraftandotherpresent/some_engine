#pragma once
#ifndef ASSASSIN_PARTICLE
#define ASSASSIN_PARTICLE
#include "PCH.h"
#include "Vector/Vector3.h"
#include "Core.h"
#include "EngineCoreDLL.h"
#include "Components/Transform.h"
namespace Assassin
{
	struct ASSA_API Particle : public Component
	{
	public:

		void virtual Integrate(const real duration);

		virtual ~Particle() {};

		inline virtual Vector3 GetPosition() const { return transform->GetPosition(); }
		inline virtual Vector3 GetVelocity() const { return velocity; }
		inline virtual Vector3 GetAcceleration() const { return acceleration; }
		inline virtual bool HasFiniteMass() const 
		{ 
			return inversMass < 0.001f; 
		}
		inline virtual real GetMass() const { return 1.f / inversMass; }
		inline virtual real GetInversMass() const { return inversMass; }

		inline void SetMass(const real value) { inversMass = 1 / value; }
		inline void SetInversMass(const real value) { inversMass = value; }
		inline void SetVelocity(const real x, const real y, const real z) { velocity = Vector3(x, y, z); }
		inline void SetAcceleration(const real x, const real y, const real z) { acceleration = Vector3(x, y, z); }
		inline void SetVelocity(const Vector3& vector) { velocity = vector; }
		inline void SetAcceleration(const Vector3& vector) { acceleration = vector; }
		
		virtual std::string ToString() const;

		inline void AddForce(const Vector3& force) { forceAccum += force; }

		bool operator==(const Particle& part)
		{
			return inversMass == part.inversMass && velocity == part.velocity && acceleration == part.acceleration && forceAccum == part.forceAccum;
		}

		virtual void SetParent(void* parent) override 
		{
			parent = parent; 
			//transform = static_cast<EngineObject*>(parent)->GetComponent<Transform>();
		}

	private:

		inline void ClearForce() { forceAccum = 0; }
	private:
		Vector3 velocity;
		Vector3 acceleration;
		Vector3 forceAccum;


		real inversMass = 1.f;
		Transform* transform;
	};
}
#endif