#include "PCH.h"
#include "ParticleGravity.h"

namespace Assassin
{
	namespace Force
	{
		void ParticleGravity::UpdateForce(Particle * particle, real duration)
		{
			if (particle->HasFiniteMass()) return;

			particle->AddForce(gravity * particle->GetMass());
		}
	}
}
