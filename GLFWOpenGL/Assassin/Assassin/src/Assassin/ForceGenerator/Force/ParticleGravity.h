#pragma once
#ifndef ASSASSIN_FORCE_GRAVITY
#define ASSASSIN_FORCE_GRAVITY

#include "PCH.h"
#include "Assassin/ForceGenerator/ParticleForceGenerator.h"
namespace Assassin
{
	namespace Force
	{
		class ASSA_API ParticleGravity : public ParticleForceGenerator
		{
		private:
			Vector3 gravity;
		public:
			ParticleGravity(const Vector3& gravity)
			{
				this->gravity = gravity;
			}

			virtual void UpdateForce(Particle* particle, real duration) override;
		};
	}
}
#endif