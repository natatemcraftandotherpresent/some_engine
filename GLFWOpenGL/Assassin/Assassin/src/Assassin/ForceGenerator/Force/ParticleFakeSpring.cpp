#include "PCH.h"
#include "ParticleFakeSpring.h"

namespace Assassin
{
	namespace Force
	{
		void ParticleFakeSpring::UpdateForce(Particle * particle, real duration)
		{
			if (!particle->HasFiniteMass())
			{
				Vector3 position = particle->GetPosition();
				position -= *anchor;

				real gamma = 0.5f * real_sqrt(4 * springConstant - damping * damping);
				if (gamma != 0)
				{
					Vector3 c = position *(damping / (2.0f * gamma)) + particle->GetVelocity() * (1.0f / gamma);

					Vector3 target = position * real_cos(gamma * duration) + c * real_exp(-0.5f * duration * damping);

					Vector3 accel = (target - position) * (1.0f / duration * duration) - particle->GetVelocity() * duration;
					particle->AddForce(-accel * particle->GetMass());
				}
			}
		}
	}
}
