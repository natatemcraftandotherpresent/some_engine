#pragma once
#ifndef ASSASSIN_FORCE_SPRING
#define ASSASSIN_FORCE_SPRING

#include "PCH.h"
#include "Assassin/ForceGenerator/ParticleForceGenerator.h"
namespace Assassin
{
	namespace Force
	{
		class ASSA_API ParticleSpring : public ParticleForceGenerator
		{
		private:
			Particle* other;

			real springConstant;
			real restLength;

		public:
			ParticleSpring(Particle* particle, const real springConst, const real restLength) : other(particle), springConstant(springConst), restLength(restLength) {};

			virtual void UpdateForce(Particle* particle, real duration) override;
		};
	}
}
#endif