#include "PCH.h"
#include "Assassin/ForceGenerator/Force/ParticleBuoyancy.h"

namespace Assassin
{
	namespace Force
	{
		void ParticleBuoyancy::UpdateForce(Particle * particle, real duration)
		{
			// Calculate the submersion depth.
			real depth = particle->GetPosition().y;

			// Check if we're out of the water.
			if (depth - particleRadius >= waterHeight) return;
			Vector3 force(real(0.0));

			// Fully Submerged
			if (depth + particleRadius < waterHeight)
			{
				force.y = liquidDensity * (particleRadius * 2) * -Vector3::GetGravity().y; // gravity 10
			}
			else // Partially Submerged
			{
				real V = (waterHeight - (depth - particleRadius));
				force.y = liquidDensity * V * -Vector3::GetGravity().y; // gravity 10
			}

			// http://www.iforce2d.net/b2dtut/buoyancy 
			// Refer to the tutorial of buoyancy. the drag is approximation.
			// water velocity is assumed by (0, 0, 0)
			// https://box2d.org/downloads/
			// Refer to the buoyancy demo of Eric Catto
			// the water linear drag is normally 5.0, according to the demo
			float dragForce = (particle->GetMass() * linearDrag) * (-particle->GetVelocity().y);
			force.y += dragForce;

			particle->AddForce(force);
		}
	}
}