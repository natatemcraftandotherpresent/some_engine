#pragma once
#ifndef ASSASSIN_FORCE_BUOYANCY
#define ASSASSIN_FORCE_BUOYANCY

#include "PCH.h"
#include "..\ParticleForceGenerator.h"

namespace Assassin
{
	namespace Force
	{
		class ASSA_API ParticleBuoyancy : public ParticleForceGenerator
		{
			/**
			* Consider a particle as a sphere, and it has a radius
			* With this variable, the submerged part of a particle is
			* determined.
			*/
			real particleRadius;


			/**
			* The height of the water plane above y = 0. The plane will be
			* parallel to the XZ plane.
			*/
			real waterHeight;

			/**
			* The linear Drag of Water
			*/
			real linearDrag;

			/**
			* The density of the liquid.Pure water has a density of
			* 1000 kg per cubic meter.
			*/
			real liquidDensity;

		public:
			/** Creates a new buoyancy force with the given parameters */
			ParticleBuoyancy(real radius, real linearDrag, real waterHeight, real liquidDensity = 1000.0f) :
				particleRadius(radius), linearDrag(linearDrag), waterHeight(waterHeight), liquidDensity(liquidDensity) {};

			/** Applies the buoyancy force to the given particle.*/
			virtual void UpdateForce(Particle* particle, real duration) override;
		};
	}
}
#endif