#pragma once
#ifndef ASSASSIN_FORCE_FAKE_SPRING
#define ASSASSIN_FORCE_FAKE_SPRING

#include "PCH.h"
#include "..\ParticleForceGenerator.h"

namespace Assassin
{
	namespace Force
	{
		class ASSA_API ParticleFakeSpring : public ParticleForceGenerator
		{
		public:
			ParticleFakeSpring(Vector3* anchor, real springConstant, real damping) : anchor(anchor), springConstant(springConstant), damping(damping) {};

			virtual void UpdateForce(Particle* particle, real duration);

		private:

			Vector3* anchor;

			real springConstant;
			real damping;
		};
	}
}
#endif