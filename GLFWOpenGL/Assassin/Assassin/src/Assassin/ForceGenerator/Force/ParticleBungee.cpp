#include "PCH.h"
#include "ParticleBungee.h"

namespace Assassin
{
	namespace Force
	{
		void ParticleBungee::UpdateForce(Particle * particle, const real duration)
		{
			if (particle->HasFiniteMass())
			{
				Vector3 force = particle->GetPosition();

				force -= other->GetPosition();

				real magnitude = force.Magnitude();

				if (magnitude <= restLength) return;

				magnitude = springConstant * (magnitude - restLength);

				force.Normalize();

				force *= -magnitude;
				float dragForce = (particle->GetMass() * springConstant) * (-particle->GetVelocity().y);
				force.y += dragForce;
				particle->AddForce(force);
			}
		}
	}
}
