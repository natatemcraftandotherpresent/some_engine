#include "PCH.h"
#include "ParticleDrag.h"

namespace Assassin
{
	namespace Force
	{
		void ParticleDrag::UpdateForce(Particle * particle, real duration)
		{
			Vector3 force = particle->GetVelocity();
			int dragCoeff = force.Magnitude();

			dragCoeff = static_cast<int>(k1 * dragCoeff + k2 * dragCoeff * dragCoeff);

			force.Normalize();
			force *= -dragCoeff;
			particle->AddForce(force);
		}
	}
}
