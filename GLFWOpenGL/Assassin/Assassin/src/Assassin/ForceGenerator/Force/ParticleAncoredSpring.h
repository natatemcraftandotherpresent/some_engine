#pragma once
#ifndef ASSASSIN_FORCE_ANCORED_SPRING
#define ASSASSIN_FORCE_ANCORED_SPRING

#include "PCH.h"
#include "Assassin/ForceGenerator/ParticleForceGenerator.h"
namespace Assassin
{
	namespace Force
	{
		class ASSA_API ParticleAncoredSpring : public ParticleForceGenerator
		{
		public:
			ParticleAncoredSpring(const Vector3& anchorPosition, const real springC, const real length) :
				anchor(anchorPosition), springConstant(springC), restLength(length) {};

			virtual void UpdateForce(Particle* particle, real duration) override;

		private:
			Vector3 anchor;

			real springConstant;

			real restLength;
		};
	}
}
#endif