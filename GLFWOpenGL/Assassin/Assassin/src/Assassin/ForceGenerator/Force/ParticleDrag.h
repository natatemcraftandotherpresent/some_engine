#pragma once
#ifndef ASSASSIN_FORCE_DRAG
#define ASSASSIN_FORCE_DRAG

#include "PCH.h"
#include "Assassin/ForceGenerator/ParticleForceGenerator.h"
namespace Assassin
{
	namespace Force
	{
		class ASSA_API ParticleDrag : public ParticleForceGenerator
		{
		public:
			ParticleDrag(const real k1, const real k2) : k1(k1), k2(k2) {};

			virtual void UpdateForce(Particle* particle, real duration);
		private:
			real k1;
			real k2;
		};
	}
}
#endif