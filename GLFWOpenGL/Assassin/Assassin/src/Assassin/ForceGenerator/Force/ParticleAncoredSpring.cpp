#include "PCH.h"
#include "ParticleAncoredSpring.h"
namespace Assassin
{
	namespace Force
	{
		void ParticleAncoredSpring::UpdateForce(Particle * particle, const real duration)
		{
			if (!particle->HasFiniteMass())
			{
				Vector3 force = particle->GetPosition();

				force -= anchor;

				int magnitude = static_cast<int>(force.Magnitude());

				magnitude = (magnitude - static_cast<int>(restLength)) * static_cast<int>(springConstant);

				force.Normalize();
				force *= -magnitude;
				float dragForce = (particle->GetMass() * springConstant) * (-particle->GetVelocity().y);
				force.y += dragForce;
				particle->AddForce(force);
			}
		}
	}
}
