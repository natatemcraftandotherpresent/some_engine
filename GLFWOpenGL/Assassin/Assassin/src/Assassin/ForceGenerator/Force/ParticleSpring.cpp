#include "PCH.h"
#include "ParticleSpring.h"

namespace Assassin
{
	namespace Force
	{
		void ParticleSpring::UpdateForce(Particle * particle, const real duration)
		{
			if (!particle->HasFiniteMass())
			{
				Vector3 force = particle->GetPosition();
				force -= other->GetPosition();

				real magnitude = force.Magnitude();
				magnitude = magnitude - restLength;
				magnitude *= springConstant;

				force.Normalize();
				force *= -magnitude;

				float dragForce = (particle->GetMass() * springConstant) * (-particle->GetVelocity().y);
				force.y += dragForce;
				particle->AddForce(force);
			}
		}
	}
}
