#pragma once
#ifndef ASSASSIN_FORCE_BUNGEE
#define ASSASSIN_FORCE_BUNGEE

#include "PCH.h"
#include "..\ParticleForceGenerator.h"

namespace Assassin
{
	namespace Force
	{
		class ASSA_API ParticleBungee : public ParticleForceGenerator
		{
		public:
			ParticleBungee(Particle* particle, const real springC, const real length) :
				other(particle), springConstant(springC), restLength(length) {};

			virtual void UpdateForce(Particle* particle, const real duration) override;
		private:
			Particle* other;

			real springConstant;

			real restLength;
		};
	}
}
#endif