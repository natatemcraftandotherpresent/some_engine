#pragma once
#ifndef ASSASSIN_FORCE_GENERATOR
#define ASSASSIN_FORCE_GENERATOR

#include "PCH.h"
#include "Assassin/Core/Particle.h"
namespace Assassin
{
	namespace Force
	{
		class ASSA_API ParticleForceGenerator
		{
		public:
			virtual void UpdateForce(Particle* particle, real duration) = 0;
			virtual bool operator==(const ParticleForceGenerator& fg)
			{
				return typeid(*this) == typeid(fg);
			}
		};
	}
}
#endif