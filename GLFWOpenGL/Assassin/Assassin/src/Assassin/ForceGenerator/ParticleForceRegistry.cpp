#include "PCH.h"
#include "ParticleForceRegistry.h"

namespace Assassin
{
	namespace Force
	{
		void ParticleForceRegestry::Add(Particle * particle, ParticleForceGenerator * fg)
		{
			if (particle != nullptr && fg != nullptr)
			{
				registration[particle].push_back(fg);
			}
		}

		void ParticleForceRegestry::Remove(Particle * particle, ParticleForceGenerator * fg)
		{
			if (registration[particle].size() <= 0)
			{
				registration.erase(particle);
			}
			else
			{
				registration[particle].remove(fg);
			}
		}

		void ParticleForceRegestry::Clear()
		{
			registration.clear();
		}

		void ParticleForceRegestry::UpdateForce(const real duartion)
		{
			Registry::const_iterator i = registration.cbegin();
			#pragma omp parallel for
			for (; i != registration.cend(); ++i)
			{
				for (auto j : i->second)
				{
					j->UpdateForce(i->first, duartion);
				}
				i->first->Integrate(duartion);
			}
		}

		ParticleForceRegestry::Registry ParticleForceRegestry::registration;
	}
}
