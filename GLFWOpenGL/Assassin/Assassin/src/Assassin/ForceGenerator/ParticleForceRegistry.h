#pragma once
#ifndef ASSASSIN_FORCE_REGISTRY
#define ASSASSIN_FORCE_REGISTRY

#include "PCH.h"
#include "Assassin/Core/Particle.h"
#include "ParticleForceGenerator.h"
namespace Assassin
{
	namespace Force
	{
		class ASSA_API ParticleForceRegestry
		{
		private:
			typedef std::unordered_map<Particle*, std::list<ParticleForceGenerator*>> Registry;
			static Registry registration;

		public:
			static void Add(Particle* particle, ParticleForceGenerator* fg);
			static void Remove(Particle* particle, ParticleForceGenerator* fg);
			static void Clear();

			static void UpdateForce(const real duartion);
		};
	}
}

#endif