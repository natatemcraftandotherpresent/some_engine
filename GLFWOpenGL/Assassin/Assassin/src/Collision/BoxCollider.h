#pragma once
#ifndef ASSASSIN_COLLISION_BOX_COLLIDER
#define ASSASSIN_COLLISION_BOX_COLLIDER

#include "Collider.h"
#include "Vector/Vector3.h"
#include "EngineCore.h"
#include "Components\Transform.h"
namespace Assassin
{
	struct ASSA_API BoxCollider : public Component, public Collider
	{
		BoxCollider(const Vector3 minPosition = Vector3(-1.f, 0.f, 1.f), const Vector3 maxPosition = Vector3(1.f, 1.f, -1.f)) : localMinPosition(minPosition), localMaxPosition(maxPosition) {};

		inline void ChangeHeight(const real height);
		inline void ChangeWidth(const real width);
		inline void ChangeDepth(const real depth);
		virtual bool Resolve(const Collider* other) override;

		virtual void SetParent(void* parent) override
		{
			Component::SetParent(parent);
			colliderPosition = &static_cast<EngineObject*>(parent)->GetComponent<Transform>()->GetPosition();
			UpdateGlobalCoord();
		}
		inline void UpdateGlobalCoord()
		{
			globalMinPosition = *colliderPosition + this->localMinPosition;
			globalMaxPosition = *colliderPosition + this->localMaxPosition;
		}
	private:
		Vector3 localMinPosition;
		Vector3 localMaxPosition;
		Vector3 globalMinPosition;
		Vector3 globalMaxPosition;

		Vector3* colliderPosition;
	};
}
#endif