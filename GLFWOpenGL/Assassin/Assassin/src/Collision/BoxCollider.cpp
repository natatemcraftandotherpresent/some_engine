#include "PCH.h"
#include "BoxCollider.h"
#include "SphereCollider.h"
#include "Assassin.h"
namespace Assassin
{
	inline void BoxCollider::ChangeHeight(const real height)
	{
		localMinPosition.y -= height;
		localMaxPosition.y += height;
		UpdateGlobalCoord();
	}
	inline void BoxCollider::ChangeWidth(const real width)
	{
		localMinPosition.x -= width;
		localMaxPosition.x += width;
		UpdateGlobalCoord();
	}
	inline void BoxCollider::ChangeDepth(const real depth)
	{
		localMinPosition.z += depth;
		localMaxPosition.z -= depth;
		UpdateGlobalCoord();
	}
	bool BoxCollider::Resolve(const Collider * other)
	{
		UpdateGlobalCoord();
		if (typeid(*other) == typeid(BoxCollider))
		{
			const BoxCollider* otherBoxCollider = static_cast<const BoxCollider*>(other);
			auto rb = static_cast<EngineObject*>(parent)->GetComponent<RigidBody>();

			if (((globalMaxPosition.y >= otherBoxCollider->globalMinPosition.y) && (globalMaxPosition.x >= otherBoxCollider->globalMinPosition.x) && (globalMaxPosition.z <= otherBoxCollider->globalMinPosition.z && globalMaxPosition.z >= otherBoxCollider->globalMaxPosition.z)) ||
				((globalMinPosition.y <= otherBoxCollider->globalMaxPosition.y) && (globalMinPosition.x <= otherBoxCollider->globalMaxPosition.x) && (globalMinPosition.z >= otherBoxCollider->globalMaxPosition.z && globalMinPosition.z <= otherBoxCollider->globalMinPosition.z)))
			{
				//rb->AddForce(rb->Get);
				return true;
			}
		}
		if (typeid(*other) == typeid(SphereCollider))
		{
			const SphereCollider* otherCollider = static_cast<const SphereCollider*>(other);
			auto lol = (globalMaxPosition - otherCollider->GetPosition()).Magnitude();
			return (globalMaxPosition - otherCollider->GetPosition()).Magnitude() <= otherCollider->GetRadius() || (globalMinPosition - otherCollider->GetPosition()).Magnitude() <= otherCollider->GetRadius();
		}
	}
}
