#include "PCH.h"
#include "SphereCollider.h"

namespace Assassin
{
	bool colided = false;
	bool SphereCollider::Resolve(const Collider* other)
	{
		if (typeid(*other) == typeid(SphereCollider))
		{
			auto collider = static_cast<const SphereCollider*>(other);
			real radiusDistance = radius + collider->radius;
			real sphereDistance = (*spherePosition - *collider->spherePosition).Magnitude();

			if (sphereDistance <= radiusDistance && !colided)
			{
				colided = true;
			}
			if (!(sphereDistance <= radiusDistance) && colided)
			{
				colided = false;
			}
			return sphereDistance <= radiusDistance;
		}
		return false;
	}
}
