#include "PCH.h"
#include "ParticleContact.h"

namespace Assassin
{
	ParticleContact::ParticleContact(Particle * first, Particle * second, const real restitution, const real penetration)
	{
		particles[0] = first;
		particles[1] = second;
		this->restitution = restitution;
		this->penetration = penetration;
	}
	void ParticleContact::Resolve(real duration)
	{
		ResolveVelocity(duration);
		ResolveInterpenetration(duration);
	}

	real ParticleContact::CalcualteSeparatingVelocity() const
	{
		Vector3 relativeVlocity = particles[0]->GetVelocity();
		if (particles[1]) relativeVlocity -= particles[1]->GetVelocity();
		return relativeVlocity * contactNormal;
	}
	void ParticleContact::ResolveVelocity(real duration)
	{
		real separatingVelocitu = CalcualteSeparatingVelocity();
		if (separatingVelocitu < 0)
		{
			real newSepVelocity = -separatingVelocitu * restitution;

			Vector3 accCausedVelocity = particles[0]->GetAcceleration();
			if (particles[1]) accCausedVelocity -= particles[1]->GetAcceleration();

			real accCausedSepVelocity = accCausedVelocity * contactNormal * duration;
			if (accCausedSepVelocity < 0)
			{
				newSepVelocity += restitution * accCausedSepVelocity;

				if (newSepVelocity < 0) newSepVelocity = 0;
			}

			real deltaVelocity = newSepVelocity - separatingVelocitu;
			real totalInversMass = particles[0]->GetInversMass();
			if (particles[1]) totalInversMass += particles[1]->GetInversMass();

			if (totalInversMass > 0)
			{
				real impuls = deltaVelocity / totalInversMass;
				Vector3 impulsPerIMass = contactNormal * impuls;

				particles[0]->SetVelocity(particles[0]->GetVelocity() + impulsPerIMass * particles[0]->GetInversMass());
				if (particles[1]) particles[1]->SetVelocity(particles[1]->GetVelocity() + impulsPerIMass * particles[1]->GetInversMass());
			}
		}
	}
	void ParticleContact::ResolveInterpenetration(real duration)
	{
		if (penetration > 0)
		{
			real totalInversMass = particles[0]->GetInversMass();
			if (particles[1]) totalInversMass += particles[1]->GetInversMass();

			if (totalInversMass > 0)
			{
				Vector3 movePerIMass = contactNormal * (penetration / totalInversMass);

				particles[0]->SetVelocity(particles[0]->GetVelocity() + movePerIMass * particles[0]->GetInversMass());
				if (particles[1]) particles[1]->SetVelocity(particles[1]->GetVelocity() + movePerIMass * particles[1]->GetInversMass());
			}
		}
	}
}
