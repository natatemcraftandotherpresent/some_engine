#include "PCH.h"
#include "ParticleColliderRegistry.h"
#include "Collision/Collider.h"
namespace Assassin
{
	std::vector<Collider*> ParticleColliderRegistry::colliders;
	inline void ParticleColliderRegistry::AddCollider(Collider * const collider)
	{
		if (collider)
		{
			colliders.push_back(collider);
		}
	}
	inline void ParticleColliderRegistry::RemoveCollider(Collider * const collider)
	{
		if (collider)
		{
			auto it = std::find(colliders.cbegin(), colliders.cend(), collider);
			if (it != colliders.cend())
			{
				colliders.erase(it);
			}
		}
	}
	void ParticleColliderRegistry::Update(real duration)
	{
		#pragma omp parallel for
		for (auto it = colliders.cbegin(); it != colliders.cend(); ++it)
		{
			#pragma omp parallel for
			for (auto jt = it + 1; jt != colliders.cend(); ++jt) 
			{
				if (jt != colliders.cend())
				{
					(*it)->Resolve(*jt);
				}
			}
		}
	}
}