#pragma once
#ifndef ASSASSIN_COLLISION_CONTACT
#define ASSASSIN_COLLISION_CONTACT

#include "Assassin\Core\Particle.h"
namespace Assassin
{
	class ASSA_API ParticleContact
	{
	public:
		ParticleContact(Particle* first, Particle* second, const real restitution, const real penetration = 0);
		void Resolve(real duration);

	private:
		real CalcualteSeparatingVelocity() const;
		Particle* particles[2];

		real restitution;
		real penetration;

		Vector3 contactNormal{ 0, 1, 0 };

		void ResolveVelocity(real duration);
		void ResolveInterpenetration(real duration);
	};

}
#endif