#pragma once
#ifndef ASSASSIN_COLLIDER_REGESTRY
#define ASSASSIN_COLLIDER_REGESTRY
#include "PCH.h"
#include "Assassin\Core\precision.h"
#include "Assassin\Core\Core.h"
namespace Assassin
{
	class Collider;
	class ASSA_API ParticleColliderRegistry
	{
	public:
		static inline void AddCollider(Collider* const collider);
		static inline void RemoveCollider(Collider* const collider);
		static void Update(real duration);
	private:

		static std::vector<Collider*> colliders;
	};
}
#endif