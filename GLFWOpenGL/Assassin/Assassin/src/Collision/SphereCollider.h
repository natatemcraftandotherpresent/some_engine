#pragma once
#ifndef ASSASSIN_COLLISION_SPHERE_COLLIDER
#define ASSASSIN_COLLISION_SPHERE_COLLIDER

#include "Vector/Vector3.h"
#include "Collider.h"
#include "Components\Transform.h"
namespace Assassin
{
	struct ASSA_API SphereCollider : public Component, public Collider
	{
	public:
		SphereCollider() : spherePosition(nullptr), radius(1) {};
		SphereCollider(Vector3* const spherePosition, const real radius) : spherePosition(spherePosition), radius(radius) {};
		bool Resolve(const Collider* other) override;

		virtual void SetParent(void* parent) override 
		{
			this->parent = static_cast<EngineObject*>(parent);
			spherePosition = &static_cast<EngineObject*>(parent)->GetComponent<Transform>()->GetPosition();
		}

		inline const Vector3 GetPosition() const { return *spherePosition; }
		inline const real GetRadius() const { return radius; }

		inline void SetRadius(const real radius) { this->radius = radius; }

	private:
		Vector3* spherePosition;
		real radius = 1;
	};
}

#endif