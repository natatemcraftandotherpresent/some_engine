#pragma once
#ifndef ASSASSIN_COLLISION_COLLIDER
#define ASSASSIN_COLLISION_COLLIDER

#include "ParticleColliderRegistry.h"
#include "Assassin/Core/Core.h"
#include "EngineCore.h"
namespace Assassin
{
	struct ASSA_API Collider
	{
		Collider() { ParticleColliderRegistry::AddCollider(this); }

		void Clear()
		{
			ParticleColliderRegistry::RemoveCollider(this);
		}
		virtual bool Resolve(const Collider* other) = 0;
		virtual ~Collider() { Clear(); }
	};
}
#endif