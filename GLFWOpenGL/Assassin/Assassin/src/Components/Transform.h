#pragma once
#ifndef ASSASSIN_TRANSFORM
#define ASSASSIN_TRANSFORM

#include "PCH.h"
#include "EngineCoreDLL.h"
#include "Vector/Vector3.h"
#include "Precision/Precision.h"

namespace Assassin
{
	struct ASSA_API Transform : public Component
	{
	public:
		Transform() : scale(Vector3(1)) {};
		// transforms
		inline Vector3& GetPosition() noexcept { return position; }
		inline Vector3& GetRotation() noexcept { return rotation; }
		inline Vector3& GetScale() noexcept { return scale; }

		// local axis
		inline Vector3& GetRightVector() noexcept { return right; }
		inline Vector3& GetUpVector() noexcept { return up; }
		inline Vector3 GetForwardVector()
		{
			Vector3 answer(std::cosf(toRadians(rotation.y)) * std::cosf(toRadians(rotation.x)),
				std::sinf(toRadians(rotation.x)),
				std::sin(toRadians(rotation.y)) * std::cosf(toRadians(rotation.x)));
			return answer.normalize();
		}

		// world axis
		inline static Vector3 GetWorldUpVector() noexcept { return Vector3(0, 1, 0); }

		inline void SetPosition(const Vector3& position) { this->position = position; }
		inline void SetRotation(const Vector3& rotation) { this->rotation = rotation; }
		inline void SetScale(const Vector3& scale) { this->scale = scale; }

		inline void SetPosition(const real x, const real y, const real z) { position = Vector3(x, y, z); }
		inline void SetRotation(const real x, const real y, const real z) { rotation = Vector3(x, y, z); }
		inline void SetScale(const real x, const real y, const real z) { scale = Vector3(x, y, z); }

	private:
		Vector3 position;
		Vector3 rotation;
		Vector3 scale;

		Vector3 right;
		Vector3 up;
	};
}
#endif