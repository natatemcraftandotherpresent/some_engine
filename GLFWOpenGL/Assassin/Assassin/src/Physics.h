#pragma once
#ifndef ASSASSIN_PHYSICS
#define ASSASSIN_PHYSICS
#include "PCH.h"
#include "Assassin/ForceGenerator/ParticleForceRegistry.h"
#include "Collision/ParticleColliderRegistry.h"
class ASSA_API Physics
{
public:
	void static Update(const Assassin::real time)
	{
		Assassin::Force::ParticleForceRegestry::UpdateForce(time);
		Assassin::ParticleColliderRegistry::Update(time);
	}
};
#endif

