#pragma once

#include <iostream>
#include <functional>
#include <cassert>

#include <cmath>

#include <thread>
#include <sstream>
#include <condition_variable>
#include <mutex>

#include <unordered_map>