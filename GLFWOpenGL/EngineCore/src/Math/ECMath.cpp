#include "PCH.h"
#include "ECMath.h"

namespace EngineCore
{
	namespace Math
	{
		int Random(const int min, const int max, int seed)
		{
			int second = min * 2;
			seed = seed == 0 ? static_cast<int>((second + min) * 0.5f) : seed;

			int value = (seed * min) + second;
			value %= max;
			return value;
		}
		float EaseOutDivideInterpolation(const float from, const float to, const float divisor)
		{
			if (divisor > 0)
			{
				return (from *(divisor - 1.0f) + to) / divisor;
			}
			return -1;
		}
		int EaseOutShiftInterpolation(const int from, const int to, const int shift)
		{
			if (shift > 0)
			{
				return (from * ((1 << shift) - 1) + to) >> shift;
			}
			return -1;
		}
		float LinearInterpolation(const float from, const float to, const float time)
		{
			if (time > 0)
			{
				return (to - from) * time;
			}
			return to;
		}
	}
}
