#pragma once
#ifndef ENGINE_CORE_MATH
#define ENGINE_CORE_MATH

namespace EngineCore
{
	namespace Math
	{
		// fibonacci
		template<unsigned int N>
		struct Fib
		{
			enum
			{
				value = Fib<N - 2>::value + Fib<N - 1>::value
			};
		};

		template<> struct Fib<0> { enum { value = 0 }; };
		template<> struct Fib<1> { enum { value = 1 }; };

		// factarial
		template<unsigned int N>
		struct Fac
		{
			enum
			{
				value = N * Fac<N - 1>::value
			};
		};

		template<> struct Fac<1> {enum{value = 1}; };

		// sin
		//template<const double &R, int I, int MaxTerms>
		//struct Series
		//{
		//	enum
		//	{
		//		Continue = I + 1 != MaxTerms
		//		NxtI = (I + 1) * Continue
		//		NxtMaxTerms = MaxTerms * Continue
		//	};
		//
		//	static inline double val()
		//	{
		//		return 1 - R * R(2.0 * I + 2.0) / (2.0 * I + 3.0) * Series<R * Continue, NxtI, NxtMaxTerms>::val();
		//	}
		//};
		//
		//const double zero = 0;
		//template<> struct Series<zero, 0, 0>
		//{
		//	static inline double val() { return 1.0; }
		//};
		//
		//
		//template<const double &R> 
		//struct Sine
		//{
		//	enum {MaxTerms = 10};
		//	static inline double sin()
		//	{
		//		return R * Series<R, 0, MaxTerms>::val();
		//	}
		//};

		//template<const double &R>
		//struct Sine 
		//{
		//	static inline double sin()
		//	{
		//		double rsqrt = R * R;
		//		return R * (1.0 - rsqrt / 2.0 / 3.0
		//				 * (1.0 - rsqrt / 4.0 / 5.0
		//				 * (1.0 - rsqrt / 6.0 / 7.0
		//				 * (1.0 - rsqrt / 8.0 / 9.0
		//				 * (1.0 - rsqrt / 10.0 / 11.0
		//				 * (1.0 - rsqrt / 12.0 / 13.0
		//				 * (1.0 - rsqrt / 14.0 / 15.0
		//				 * (1.0 - rsqrt / 16.0 / 17.0
		//				 * (1.0 - rsqrt / 18.0 / 19.0
		//				 * (1.0 - rsqrt / 20.0 / 21.0
		//					 ))))))))));
		//	}
		//};

		int Random(const int min, const int max, int seed = 0);

		float EaseOutDivideInterpolation(const float from, const float to, const float divisor);
		int EaseOutShiftInterpolation(const int from, const int to, const int shift);
		float LinearInterpolation(const float from, const float to, const float time);
	}
}

#endif