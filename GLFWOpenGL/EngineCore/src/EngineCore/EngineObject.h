#pragma once
#ifndef ENGINE_CORE_ENGINE_OBJECT
#define ENGINE_CORE_ENGINE_OBJECT

#include "PCH.h"
#include "..\..\EngineCore\src\Memory\ResourceMgr.h"
#include "EngineCoreDLL\Component.h"
#include "EngineCoreDLL.h"

namespace EngineCore
{
#define MAX_OBJECT_COMPONENTS 25
	class EngineObject : public Memory::BaseResource
	{
	public:

		EngineObject()
		{
			
		}

		template<class COMPONENT = EngineCore::Component>
		COMPONENT* AddComponent()
		{
			auto it = std::find_if(components.cbegin(), components.cend(), [](Component** component)
			{
				if (typeid(component) == typeid(COMPONENT))
				{
					return true;
				}
				return false;
			});
			if (it == components.cend())
			{
				auto handler = componentManager.CreateResource<COMPONENT>();
				COMPONENT** component = componentManager.GetResource<COMPONENT>(handler);
				components.push_back((Component**)component);
				auto retu = componentManager.Lock<COMPONENT>(handler);
				componentManager.Unlock<COMPONENT>(retu);
				(*component)->SetParent(this);
				(*component)->SetData(this, handler, retu);
				return *component;
			}
			return (COMPONENT*)**it;
		}

		template<class COMPONENT = EngineCore::Component>
		COMPONENT* const GetComponent() const
		{
			auto it = std::find_if(components.cbegin(), components.cend(), [](Component** comp)
			{
				if (typeid(**comp) == typeid(COMPONENT))
				{
					return true;
				}
				return false;
			});
			if (it != components.cend())
			{
				return (COMPONENT*)**it;
			}
			return nullptr;
		}
		template<class COMPONENT = EngineCore::Component>
		Memory::MemoryStorage* const GetComponentPoint()
		{
			auto it = std::find_if(components.cbegin(), components.cend(), [](Component** comp)
				{
					if (typeid(**comp) == typeid(COMPONENT))
					{
						return true;
					}
					return false;
				});
			if (it != components.cend())
			{
				auto find = componentManager.FindResourcehandler(*it);
				auto retu = componentManager.Lock<COMPONENT>(find);
				componentManager.Unlock<COMPONENT>(retu);
				return retu;
			}
			return nullptr;
		}
		virtual void Dispose() override 
		{ 
			Clear(); 
		}
		void Clear()
		{
			componentManager.Clear();
		}

		virtual void Start() {};
		virtual void Update(real duration) {};
		virtual ~EngineObject()
		{
			
		}
	private:
		std::vector<Component**> components;
		Memory::ResourceMgr<2048> componentManager;
	};
}
#endif