#pragma once
#ifndef ENGINE_CORE_DEBUG
#define ENGINE_CORE_DEBUG
#include "PCH.h"
#include "Addons\Singleton.h"
#include "EngineCore\ConsoleBuffer.h"
namespace EngineCore
{
#undef ERROR
#define LOG_DEFAULT_INDEX -1
	enum class Severity
	{
		INFO = 0,
		WARNING = 1,
		ERROR = 2,
		FATAL = 3
	};
	class Debug : public Singleton<Debug>
	{
	public:
		template<typename ...Args>
		void LogMessage(Severity serverty, const char* msg, const char* flineName = " ", const char* functionName = " ", int line = 1,int index = LOG_DEFAULT_INDEX, Args&&... args)
		{
			//st << functionName << " " << line << ": " << msg << "\n" << flineName;
			//printf(st.str().c_str(), std::forward<Args>(args)...);
			//st.str(std::string());
			//st << msg << (std::forward<Args>(args)...);
			ConsoleBuffer::GetSingleton().AddToBuffer(msg, index, std::forward<Args>(args)...);
		}
	private:
		std::stringstream st;
	};


#ifdef EC_DEBUG
	#define EC_LOG(serverty, msg, index, ...) EngineCore::Debug::GetSingleton().LogMessage(EngineCore::Severity::##serverty, \
																msg, __FILE__, __FUNCTION__, __LINE__, index, __VA_ARGS__)
#else
	#define EC_LOG(serverty, msg, index, ...)
#endif // EC_DEBUG
}


#endif