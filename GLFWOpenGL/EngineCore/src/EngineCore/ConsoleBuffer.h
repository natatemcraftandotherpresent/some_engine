#pragma once
#ifndef ENGINE_CORE_CONSOLE_BUFFER
#define ENGINE_CORE_CONSOLE_BUFFER

#include "PCH.h"
#include "Addons\Singleton.h"
#include "Types\Types.h"
namespace EngineCore
{
#define SCREEN_BUFFER_SIZE 1024
#define MAX_LINE_CHAR 50
#define LOG_PROFILER_SIZE 7

	class ConsoleBuffer final : public Singleton<ConsoleBuffer>
	{
	public:
		ConsoleBuffer() : console(CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, 0, NULL, CONSOLE_TEXTMODE_BUFFER, NULL)),
						bytesWritten(0), index(LOG_PROFILER_SIZE)
		{
			SetConsoleActiveScreenBuffer(console);
		}
		template<typename ...Args>
		void AddToBuffer(const char* txt, const uint16 index = -1, Args&&... args)
		{
			if (index == -1)
			{
				sprintf_s(&screen[80 * this->index++], MAX_LINE_CHAR, txt, std::forward<Args>(args)...);
			}
			else
			{
				sprintf_s(&screen[80 * index], MAX_LINE_CHAR, txt, std::forward<Args>(args)...);
			}
		}
		void UpdateBuffer();
	private:
		char screen[SCREEN_BUFFER_SIZE] = { ' ' };
		const HANDLE console;
		DWORD bytesWritten;
		uint16 index;
	};
}

#endif