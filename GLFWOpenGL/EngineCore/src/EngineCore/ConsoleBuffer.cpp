#include "PCH.h"
#include "ConsoleBuffer.h"

namespace EngineCore
{
	void ConsoleBuffer::UpdateBuffer()
	{
		WriteConsoleOutputCharacter(console, screen, SCREEN_BUFFER_SIZE, { 0, 0 }, &bytesWritten);
		bytesWritten = 0;
		index = LOG_PROFILER_SIZE;
	}
}
