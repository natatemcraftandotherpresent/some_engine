#pragma once
#ifndef ENGINE_CORE_FUNCTION_DELAY
#define ENGINE_CORE_FUNCTION_DELAY

#include "PCH.h"
namespace EngineCore
{
	template<typename T, typename ...Args>
	class FunctionDelay
	{
	public:
		FunctionDelay(T(*func)(Args...)) : function(func) {}

		template<typename ...Args>
		T Fetch(Args&&... params)
		{
			return function(std::forward<Args>(params)...);
		}
	private:
		T(*function)(Args...);
	};
}
#endif