#pragma once
#ifndef ENGINE_CORE_OBJECT_REGISTRY
#define ENGINE_CORE_OBJECT_REGISTRY

#include "PCH.h"
#include "EngineObject.h"
#include "Memory/ResourceMgr.h"
#include "Addons/Singleton.h"
namespace EngineCore
{
	class EngineObjectRegistry : public Singleton<EngineObjectRegistry>
	{
	public:
		inline void Add(EngineObject** object)
		{
			if (*object)
			{
				objects.push_back(object);
			}
		}
		inline void Remove(EngineObject** object)
		{
			if (object)
			{
				auto it = std::find(objects.begin(), objects.end(), object);
				if (it != objects.end())
				{
					objects.erase(it);
					objectManager.DestroyResource(object);
					//std::swap(*it, *(objects.end() - 1));
				}
			}
		}
		void Update(real duration)
		{
			for (auto it = objects.cbegin(); it != objects.cend(); ++it)
			{
				(*(*it))->Update(duration);
			}
		}

		void RemoveAll()
		{
			objectManager.Clear();
			objects.clear();
		}
		template<class ENGINE_OBJECT = EngineObject>
		ENGINE_OBJECT** CreateObject()
		{
			auto handler = objectManager.CreateResource<ENGINE_OBJECT>();
			ENGINE_OBJECT** object = objectManager.GetResource<ENGINE_OBJECT>(handler);
			
			Add((EngineObject**)object);
			(*object)->Start();
			return object;
		}
		~EngineObjectRegistry() { RemoveAll(); }
	private:
		std::vector<EngineObject**> objects;
		Memory::ResourceMgr<12048> objectManager;

	};
}
#endif