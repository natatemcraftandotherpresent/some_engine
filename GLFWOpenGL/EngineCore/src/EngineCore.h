#pragma once
#ifndef ENGINE_CORE_EXPORT_CONTENT
#define ENGINE_CORE_EXPORT_CONTENT
#include "EngineCore\EngineObject.h"

#include "EngineCore\ConsoleBuffer.h"
#include "EngineCore\Debug.h"
#include "EngineCore\FunctionDelay.h"


#include "Memory\ResourceMgr.h"
#include "Memory\FrameBasedMgr.h"
#include "Memory\Allocator\LinearAllocator.h"

#include "EngineCore\EngineObjectRegistry.h"

#include "Math\ECMath.h"

#include "Addons\Singleton.h"
#include "Addons\Profiler.h"

#include "Managers\Handle\Handle.h"
#include "Managers\Handle\HandleMgr.h"

//typedef EngineCore::byte byte;
typedef EngineCore::EngineObject EngineObject;

namespace ECMath = EngineCore::Math;

#define ProfilerMgr EngineCore::profiler.GetSingleton()
#define ConsoleBufferMgr EngineCore::conBuff.GetSingleton()
#define DebugMgr EngineCore::debug.GetSingleton()
#define ObjectsMgr EngineCore::eor.GetSingleton()


#endif