#pragma once
#ifndef ENGINE_CORE_MEMORY_ALLOCATOR_LINEAR
#define ENGINE_CORE_MEMORY_ALLOCATOR_LINEAR

#include "..\..\PCH.h"
namespace EngineCore
{
	namespace Memory
	{
		template<class T>
		class LinearAllocator
		{
#define LINEAR_BUFFER_SIZE 2048
		public:
			using value_type = T;

			using pointer = T*;
			using const_pointer = const T*;

			using void_pointer = void*;
			using const_void_pointer = const void*;

			using size_type = size_t;

			using difference_type = std::ptrdiff_t;

			typedef std::map<pointer, MemoryStorage> AllocMap;
			template<class U>
			struct rebind
			{
				using other = LinearAllocator<U>;
			};

			pointer allocate(size_type numObjects)
			{
				int oldOffset = offset;
				offset += sizeof(value_type) * numObjects;
				auto lol = (pointer)(arr + oldOffset);
				return (pointer)(arr + oldOffset);
			}

			pointer allocate(size_type numObjects, const_void_pointer hint)
			{
				return allocate(numObjects);
			}

			void deallocate(pointer p, size_type numObjects)
			{

			}

			size_type max_size() const
			{
				return LINEAR_BUFFER_SIZE;
			}

			template<class U, class... Args>
			void construct(U* p, Args&& ...args)
			{
				new (p) U(std::forward<Args>(args)...);
			}
			template<class U>
			void destroy(U*p)
			{
				//p->~U();
			}

			LinearAllocator() = default;

			template<class U>
			LinearAllocator(const LinearAllocator<U>& other) {}

			~LinearAllocator() = default;

		private:
			byte arr[LINEAR_BUFFER_SIZE];
			AllocMap pointMap;
			int offset = 0;
		};
	}
}
#endif