#pragma once
#ifndef ENGINE_CORE_MEMORY_FRAME_BASED_MANAGER
#define ENGINE_CORE_MEMORY_FRAME_BASED_MANAGER

#include "PCH.h"

namespace EngineCore
{
	namespace Memory
	{
		typedef byte u8;
#define ALIGNUP(nAddress, nBytes) (((nAddress) + ((nBytes) -1)) & (~((nBytes)-1)))

#define HEAP_BOTTOM_POINTER 0
#define HEAP_UPPER_POINTER 1
		class FrameBasedMgr
		{
			struct Frame_t
			{
				friend FrameBasedMgr;
			private:
				u8* mem;
				unsigned heapPoistion;
			};
		private:
			static u8 byteAlignment;
			static u8* memoryBlock;
			static u8* baseAndCap[2];
			static u8* frames[2];
		public:
			/// must be called exactly once at game initialization time
			/// "alignment" must be a power-of-2
			static int InitFrameMemory(unsigned sizeInBytes, u8 alignment);
			/// returns a Memory Heap Pointer to the data
			/// "heapPosition" must be HEAP_BOTTOM_POINTER or HEAP_UPPER_POINTER
			static void* AllockFrameMemory(size_t bytes, unsigned heapPosition = HEAP_BOTTOM_POINTER);

			static const Frame_t GetFrameHeapHandle(unsigned heapPosition = HEAP_BOTTOM_POINTER);

			static void FreeFrameMemory(const Frame_t frame_t);
			/// clears all the data
			static void ShutdownFrameMemorySystem();
			/// get buffer byte's alignment
			static inline const unsigned GetByteAlignment() { return byteAlignment; }
		};

	}
}
#endif