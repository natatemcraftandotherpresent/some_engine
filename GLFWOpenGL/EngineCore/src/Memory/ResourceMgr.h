#pragma once
#ifndef ENGINE_CORE_MEMORY_RESOURCE_MANAGER
#define ENGINE_CORE_MEMORY_RESOURCE_MANAGER

#include "PCH.h"
#include "Managers\Handle\Handle.h"
#include "EngineCoreDLL.h"
namespace EngineCore
{
	namespace Memory
	{
#define TIME_TO_CLEAN 5

		typedef Handle<BaseResource::tagType> HResource;
		template<size_t ARRAY_SIZE>
		class ResourceMgr
		{
			typedef std::vector<MemoryStorage*> ResMap;
			typedef ResMap::iterator iterator;

			template<class F, class S, class K>
			struct value_compare
			{
			private:
				K sample;
			public:
				value_compare(K s) : sample(s) {}
				bool operator()(const std::pair<F, S>& par)
				{
					return sample == static_cast<K>(par.second->GetMemoryPointer());
				}
			};

		public:
			ResourceMgr() : buffer(arr, ARRAY_SIZE)
			{ 
				Create();
			}
			virtual ~ResourceMgr() { Destroy(); }
			virtual void Clear()
			{
				for (auto it = resourceMap.rbegin(); it != resourceMap.rend(); ++it)
				{
					LinearBufferFree(&buffer, *it);
				}
				resourceMap.clear();
			}

			bool ReservMemory(size_t mem) { return buffer.IsFreeMem(mem); };

			// creates a new resource and returns the Handler for it
			template<class RESOURCE>
			HResource const CreateResource()
			{
				if (ReservMemory(sizeof(RESOURCE)))
				{
					auto memory = LinearBufferAlloc(&buffer, sizeof(RESOURCE));
					if (magicNumbers.size() == 0)
					{
						memory->GetHandle().Init(resourceMap.size());
						resourceMap.push_back(memory);
					}
					else
					{
						unsigned index = magicNumbers.back();
						magicNumbers.pop_back();
						resourceMap[index] = memory;
					}
					new (memory->GetMemoryPointer()) RESOURCE();
					return memory->GetHandle();
				}
				return HResource();
			}
			// creates a new resource for the Handler
			template<class RESOURCE>
			void const CreateResource(HResource& resource)
			{
				if (ReservMemory(sizeof(RESOURCE)))
				{
					auto& it = resourceMap[resource.GetIndex()];
					if (!it.second)
					{
						auto memory = LinearBufferAlloc(&buffer, sizeof(RESOURCE));
						if (magicNumbers.size() == 0)
						{

							resource.Init(resourceMap.size());
							resourceMap.push_back(memory);
						}
						else
						{
							unsigned index = magicNumbers.back();
							magicNumbers.pop_back();
							resourceMap[index] = memory;
							resource = memory->GetHandle();
						}
						new (memory->GetMemoryPointer()) RESOURCE();
					}
				}
			}
			// clears all resource data
			template<class RESOURCE>
			bool DestroyResource(RESOURCE** resource)
			{
				iterator it = std::find_if(resourceMap.begin(), resourceMap.end(), [&resource](MemoryStorage* pair)
					{
						return (RESOURCE**)(pair->GetPointerMemoryPointer()) == resource;
					});
				if (it != resourceMap.end())
				{
					magicNumbers.push_back((*it)->GetHandle().GetIndex());
					LinearBufferFree(&buffer, (*it));
					resourceMap.erase(it);
					return true;
				}
				return false;
			}
			// clears all resource data
			bool DestroyResource(HResource& resource) 
			{
				auto it = resourceMap[resource.GetIndex()];
				if (it->second && !static_cast<BaseResource*>(it->second->GetMemoryPointer())->IsLocked())
				{
					LinearBufferFree(&buffer, it->second);
					magicNumbers.push_back(it->first.GetIndex());
					resourceMap.erase(it);
					return true;
				}
				return false;
			}
			// returns the pointer to the resource pointer
			template<class RESOURCE>
			RESOURCE** GetResource(HResource& rhUniqueID) 
			{
				auto it = resourceMap[rhUniqueID.GetIndex()];
				if (it)
				{
					return (RESOURCE**)(it->GetPointerMemoryPointer());
				}
				return nullptr;
			}
			// locks the object and gives the private access to it
			template<class RESOURCE>
			MemoryStorage* Lock(HResource& rhUniqueID) 
			{
				auto it = resourceMap[rhUniqueID.GetIndex()];
				if (it)
				{
					auto resource = (RESOURCE*)(it->GetMemoryPointer());
					resource->Lock();
					return it;
				}
				return nullptr;
			}

			// unlocks the object and discards private access to it
			template<class RESOURCE>
			unsigned Unlock(HResource& rhUniqueID)
			{
				auto it = resourceMap.find(rhUniqueID);
				if (it != resourceMap.cend())
				{
					auto resource = static_cast<RESOURCE*>(it->second->GetMemoryPointer());
					resource->Unlock();
					return resource->GetReferenceCount();
				}
				return 0;
			}
			// unlocks the object and discards private access to it
			template<class RESOURCE>
			unsigned Unlock(MemoryStorage* resource)
			{
				iterator it = std::find_if(resourceMap.begin(), resourceMap.end(), [&resource](MemoryStorage* pair)
					{
						return pair == resource;
					});
				if (it != resourceMap.cend())
				{
					static_cast<RESOURCE*>(resource->GetMemoryPointer())->Unlock();
					return static_cast<RESOURCE*>(resource->GetMemoryPointer())->GetReferenceCount();
				}
				return 0;
			}
			// finds and returns the handler for the object
			template<class RESOURCE>
			HResource FindResourcehandler(RESOURCE** resource) 
			{
				auto it = std::find_if(resourceMap.cbegin(), resourceMap.cend(), value_compare<HResource, MemoryStorage*, RESOURCE*>(*resource));
				if (it != resourceMap.cend())
				{
					return it->first;
				}
				return HResource();
			}

		protected:
			inline void AddMemory(unsigned mem)
			{
				currentUsedMemory += mem;
			}
			inline void RemoveMemory(unsigned mem)
			{
				currentUsedMemory -= mem;
			}

			template<class RESOURCE>
			bool CheckForOverallocation() 
			{
				if (static_cast<RESOURCE*>((*resourceMap.begin())->GetMemoryPointer())->GetLastAccess() - time_t() == 3600 * TIME_TO_CLEAN)
				{
					LinearBufferFree(&buffer, (*resourceMap.begin()));
					resourceMap.erase(resourceMap.begin());
					return true;
				}
				return false;
			}
		private:
			bool Create()
			{
				//buffer.Create(arr, ARRAY_SIZE);
				maximumMemory = ARRAY_SIZE;
				return true;
			}

			void Destroy()
			{
				Clear();
				magicNumbers.clear();
			}

			unsigned currentUsedMemory;
			unsigned maximumMemory;
			ResMap resourceMap;

			byte arr[ARRAY_SIZE];
			LinearBuffer buffer;

			std::vector<unsigned> magicNumbers;
		};
	}
}
#endif