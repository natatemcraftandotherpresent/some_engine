#include "PCH.h"
#include "FrameBasedMgr.h"
#include "EngineCoreDLL\Precision.h"
namespace EngineCore
{
	namespace Memory
	{
		u8 FrameBasedMgr::byteAlignment;
		u8* FrameBasedMgr::memoryBlock;
		u8* FrameBasedMgr::baseAndCap[2];
		u8* FrameBasedMgr::frames[2];

		int FrameBasedMgr::InitFrameMemory(unsigned sizeInBytes, u8 alignment)
		{
			sizeInBytes = ALIGNUP(sizeInBytes, alignment);

			memoryBlock = static_cast<u8*>(malloc(sizeInBytes + alignment));
			if (memoryBlock)
			{
				byteAlignment = alignment;
				baseAndCap[0] = (u8*)(ALIGNUP((u8)memoryBlock, byteAlignment));
				baseAndCap[1] = (u8*)(ALIGNUP((u8)memoryBlock + sizeInBytes, byteAlignment));

				frames[0] = baseAndCap[0];
				frames[1] = baseAndCap[1];

				return EXIT_SUCCESS;
			}
			return EXIT_FAILURE;
		}

		void * FrameBasedMgr::AllockFrameMemory(size_t bytes, unsigned heapPosition)
		{
			void* mem = nullptr;
			bytes = (ALIGNUP(bytes, byteAlignment));

			if (frames[0] + bytes <= frames[1])
			{
				switch (heapPosition)
				{
				case HEAP_BOTTOM_POINTER:
					mem = frames[0];
					frames[0] += bytes;
					break;
				case HEAP_UPPER_POINTER:
					frames[1] -= bytes;
					mem = frames[1];
					break;
				default:
					break;
				}
			}
			return mem;
		}

		const FrameBasedMgr::Frame_t FrameBasedMgr::GetFrameHeapHandle(unsigned heapPosition)
		{
			Frame_t frame_t;
			frame_t.heapPoistion = heapPosition;
			frame_t.mem = frames[heapPosition];
			return frame_t;
		}

		void FrameBasedMgr::FreeFrameMemory(const Frame_t frame_t)
		{
			Assert(frame_t.heapPoistion == 0 && frame_t.mem <= frames[1], "FrameBasedMgr::FreeFrameMemory: corrupted heap");
			Assert(frame_t.heapPoistion == 1 && frame_t.mem >= frames[0], "FrameBasedMgr::FreeFrameMemory: corrupted heap");
			frames[frame_t.heapPoistion] = frame_t.mem;
		}

		void FrameBasedMgr::ShutdownFrameMemorySystem()
		{
			if (memoryBlock)
			{
				free(memoryBlock);
			}
		}
	}
}