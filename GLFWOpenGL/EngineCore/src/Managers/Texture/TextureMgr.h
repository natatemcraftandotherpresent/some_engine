#pragma once
#ifndef ENGINE_CORE_MANAGERS_TEXTURE_MANAGER
#define ENGINE_CORE_MANAGERS_TEXTURE_MANAGER

#include "..\..\PCH.h"
#include <ddraw.h>
#include "..\Handle\Handle.h"
#include "..\Handle\HandleMgr.h"
#include "..\..\EngineCore\Precision.h"
namespace EngineCore
{
	typedef LPDIRECTDRAWSURFACE7 OsHandle;

	struct tagTexture {};
	typedef Handle<tagTexture> HTexture;

	class TextureMgr
	{
		struct Texture
		{
			typedef std::vector<OsHandle> HandleVec;

			std::string name;
			unsigned width;
			unsigned height;
			HandleVec handles;

			OsHandle GetOsHandle(unsigned mip) const
			{
				Assert(mip < handles.size(), "");
				return handles[mip];
			}
			bool Load(const std::string& name)
			{

			}
			void Unload()
			{

			}
		};
		typedef HandleMgr <Texture, HTexture> HTextureMrg;

		struct istring_less
		{
			bool operator() (const std::string& lef, const std::string& rig) const
			{
				return (stricmp(lef.c_str(), rig.c_str()));
			}
		};

		typedef std::map<std::string, HTexture, istring_less> NameIndex;
		typedef std::pair <NameIndex::iterator, bool> NameIndexInsertRc;

		HTextureMrg textures;
		NameIndex nameIndex;

	public:
		TextureMgr() = default;
		~TextureMgr()
		{
			NameIndex::iterator i, begin = nameIndex.begin(), end = nameIndex.end();
			for (i = begin; i != end; ++i)
			{
				textures.Dereference(i->second)->Unload();
			}
		}

		HTexture GetTexture(const char* name)
		{
			NameIndexInsertRc rc = nameIndex.insert(std::make_pair(name, HTexture()));
			if (rc.second)
			{
				Texture* tex = textures.Acquire(rc.first->second);
				if (!tex->Load(rc.first->first))
				{
					DeleteTexture(rc.first->second);
					rc.first->second = HTexture();
				}
			}
			return rc.first->second;
		}
		void DeleteTexture(HTexture htex)
		{
			Texture* tex = textures.Dereference(htex);
			if (tex)
			{
				nameIndex.erase(nameIndex.find(tex->name));

				tex->Unload();
				textures.Release(htex);
			}
		}

		const std::string& GetName(HTexture htex) const
		{
			return textures.Dereference(htex)->name;
		}
		const unsigned GetWidth(HTexture htex) const
		{
			return textures.Dereference(htex)->width;
		}
		const unsigned GetHeight(HTexture htex) const
		{
			return textures.Dereference(htex)->height;
		}

		OsHandle GetTexture(HTexture htex, unsigned mip = 0) const
		{
			return textures.Dereference(htex)->GetOsHandle(mip);
		}
	};
}
#endif