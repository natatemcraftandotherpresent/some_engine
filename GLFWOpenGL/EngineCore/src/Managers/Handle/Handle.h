#pragma once
#ifndef ENGINE_CORE_MANAGERS_HANDLE
#define ENGINE_CORE_MANAGERS_HANDLE

#include "PCH.h"
#include "EngineCoreDLL\Precision.h"
namespace EngineCore
{
	template<typename TAG>
	class Handle
	{
		union
		{
			enum
			{
				// sizes to use for bit fields
				MAX_BITS_INDEX = 16,
				MAX_BITS_MAGIC = 16,

				// sizes to compare against for asserting dereferences
				MAX_INDEX = (1 << MAX_BITS_INDEX) - 1,
				MAX_MAGIC = (1 << MAX_BITS_MAGIC) - 1
			};
			struct
			{
				unsigned index : MAX_BITS_INDEX;
				unsigned magic : MAX_BITS_MAGIC;
			};
			unsigned handle;
		};
	public:
		Handle() : handle(0) {}
		void Init(const unsigned index)
		{
			Assert(IsNull(), "");
			Assert(index <= MAX_INDEX, "");

			static unsigned autoMagic = 0;
			if (++autoMagic > MAX_MAGIC)
			{
				autoMagic = 1;
			}

			this->index = index;
			magic = autoMagic;
		}

		inline const unsigned GetIndex() const { return index ; }
		inline const unsigned GetMagic() const { return magic ; }
		inline const unsigned GetHandle() const { return handle;  }
		bool IsNull() const { return{ !handle }; }

		inline operator unsigned()const { return{ handle }; }
		inline bool operator !=(Handle<TAG>& other)
		{
			return (GetHandle() != other.GetHandle());
		}
		inline bool operator ==(Handle<TAG>& other)
		{
			return !(operator!=(other));
		}
	};
}
#endif