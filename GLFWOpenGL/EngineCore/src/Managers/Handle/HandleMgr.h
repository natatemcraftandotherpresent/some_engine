#pragma once
#ifndef ENGINE_CORE_MANAGERS_HANDLE_MANAGER
#define ENGINE_CORE_MANAGERS_HANDLE_MANAGER

#include "PCH.h"
namespace EngineCore
{
	template<typename DATA, class HANDLE>
	class HandleMgr
	{
	private:
		typedef std::vector<DATA> UserVec;
		typedef std::vector<unsigned> MagicVec;
		typedef std::vector<unsigned> FreeVec;

		UserVec userData;		// data we're going to get to
		MagicVec magicNumbers;	// corresponding magic numbers
		FreeVec freeSlots;		// keeps track of free slots in the db

	public:
		HandleMgr() = default;
		~HandleMgr() = default;

		DATA* Acquire(HANDLE& handle)
		{
			unsigned index;
			if (freeSlots.empty())
			{
				index = magicNumbers.size();
				handle.Init(index);
				userData.push_back(DATA());
				magicNumbers.push_back(handle.GetMagic());
			}
			else
			{
				index = freeSlots.back();
				handle.Init(index);
				freeSlots.pop_back();
				magicNumbers[index] = handle.GetMagic();
			}
			return (userData.begin() + index);
		}
		void Release(HANDLE& handle)
		{
			unsigned index = handle.GetIndex();

			Assert(index < userData.size(), "");
			Assert(magicNumbers[index] == handle.GetMagic(), "");

			magicNumbers[index] = 0;
			freeSlots.push_back(index);
		}

		DATA* Dereference(HANDLE handle)
		{
			if (!handle.IsNull())
			{
				unsigned index = handle.GetIndex();
				if (index < userData.size() && magicNumbers[index] == handle.GetMagic())
				{
					return (userData.begin() + index);
				}
			}
			return nullptr;
		}
		const DATA* Dereference(HANDLE handle) const
		{
			typedef HandleMgr<DATA, HANDLE> ThisType;
			return (const_cast<ThisType*>(this)->Dereference(handle));
		}

		unsigned GetUsedHandleCount() const
		{
			return magicNumbers.size() - freeSlots.size();
		}
		bool HasUsedHandles() const
		{
			return (!!GetUsedHandleCount());
		}
	};
}
#endif