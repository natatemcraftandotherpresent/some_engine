#pragma once
#ifndef ENGINE_CORE_ADDONS_GAME_DATA
#define ENGINE_CORE_ADDONS_GAME_DATA

#include "..\..\EngineCore\PCH.h"
#include "..\EngineCore\Precision.h"
namespace EngineCore
{
#define READ_GRANULARITY 2048
	class GameData
	{
	public:
		bool Save(const char* fileName)
		{
			fileDescriptor = fopen(fileName, "wb");
			if (fileDescriptor)
			{
				fwrite(this, sizeof(GameData), 1, fileDescriptor);
				fclose(fileDescriptor);
				return true;
			}
			return false;
		}
		bool Load(const char* fileName)
		{
			fileDescriptor = fopen(fileName, "rb");
			if (fileDescriptor)
			{
				fread(this, sizeof(GameData), 1, fileDescriptor);
				fclose(fileDescriptor);
				return true;
			}
			return false;
		}
		bool BufferedLoad(const char* filename)
		{
			char* tempBuffer = new char[sizeof(GameData) + READ_GRANULARITY];
			if (tempBuffer)
			{
				fileDescriptor = fopen(filename, "rb");
				if (fileDescriptor)
				{
					fread(tempBuffer, sizeof(GameData), 1, fileDescriptor);
					fclose(fileDescriptor);
					memcpy(this, tempBuffer, sizeof(GameData));
					delete[] tempBuffer;
					return true;
				}
				delete[] tempBuffer;
			}
			return false;
		}
	private:
		static FILE* fileDescriptor;
		byte data[1000];

	};
}
#endif