#pragma once
#include "..\..\..\PCH.h"

template<typename T>
class LockFreeFreeList
{
private:
	LockFreeStack<T*> freeList;
	T* objects;
	const uint32_t numObjects;
public:
	LockFreeFreeList(uint32_t numObjects) : numObjects(numObjects)
	{
		objects = new T[numObjects];
		FreeAll();
	}

	void FreeAll()
	{
		for (uint32_t ix = 0; ix < numObjects; ++ix)
		{
			freeList.Push(&objects[ix]);
		}
	}

	T* NewInstance()
	{
		T* instance = freeList.Pop();
		return new (&instance) T;
	}

	void FreeInstance(T* instance)
	{
		instance->~T();
		freeList.Push(reinterpret_cast<T*>(instance));
	}

	~LockFreeFreeList()
	{
		delete[] objects;
	}
};