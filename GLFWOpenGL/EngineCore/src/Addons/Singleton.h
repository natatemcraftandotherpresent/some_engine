#pragma once
#ifndef ENGINE_CORE_ADDONS_SINGLETON
#define ENGINE_CORE_ADDONS_SINGLETON

#include "PCH.h"
#include "EngineCoreDLL\Precision.h"
namespace EngineCore
{
	template<class SINGLETON>
	class Singleton
	{
	public:
		Singleton()
		{
			Assert(!singleton, "");
			singleton = (SINGLETON*)(this);
		}
		virtual ~Singleton()
		{
			Assert(singleton, "");
			singleton = nullptr;
		}

		static SINGLETON& GetSingleton() { return *singleton; }
		static SINGLETON* GetSingletonPtr()  { return singleton; }
	private:
		static SINGLETON* singleton;
	};

	template<class SINGLETON>
	SINGLETON* Singleton<SINGLETON>::singleton = 0;
}
#endif