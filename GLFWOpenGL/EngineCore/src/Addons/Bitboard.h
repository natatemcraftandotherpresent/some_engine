#pragma once
#include "..\PCH.h"
class Bitboard
{
public:
	Bitboard();

	void AddBit(unsigned char position);

	bool IsBit(unsigned char position) const;

	void RemoveBit(unsigned char position);

	long GetBoard() const { return bitboard; }

private:
	long bitboard;
};