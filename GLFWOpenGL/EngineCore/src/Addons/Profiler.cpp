#include "PCH.h"
#include "Profiler.h"
#include "EngineCoreDLL\Precision.h"
#include "..\EngineCore\Debug.h"

namespace EngineCore
{
	void Profiler::Init(float time)
	{
		for (size_t i = 0; i < NUM_PROFILE_SAMPLES; ++i)
		{
			samples[i].valid = false;
			history[i].valid = false;
		}
		startProfile = time > 0 ? time : 0;
	}
	void Profiler::ProfileBegin(const char* name, double(*time)())
	{
		unsigned i = 0;
		while (i < NUM_PROFILE_SAMPLES && samples[i].valid)
		{
			if (std::strcmp(samples[i].name, name) == 0)
			{
				samples[i].openProfiler++;
				samples[i].profileInstances++;
				samples[i].startTime = static_cast<float>(time());
				Assert(samples[i].openProfiler == 1, "Profiler::ProfileBegin: max 1 open at once");
			}
			i++;
		}
		if (i >= NUM_PROFILE_SAMPLES)
		{
			Assert(0, "Exceeded max acailable profile samples");
			return;
		}
		strcpy_s(samples[i].name, name);
		samples[i].valid = true;
		samples[i].openProfiler = 1;
		samples[i].profileInstances = 1;
		samples[i].accumulator = 0.0f;
		samples[i].startTime = static_cast<float>(time());
		samples[i].childrenSampleTime = 0.0f;
	}
	void Profiler::ProfileEnd(const char * name, double(*time)())
	{
		unsigned i = 0;
		unsigned numParants = 0;
		while (i < NUM_PROFILE_SAMPLES && samples[i].valid)
		{
			if (std::strcmp(samples[i].name, name) == 0)
			{
				unsigned inner = 0;
				int parent = -1;
				float endTime = static_cast<float>(time());
				samples[i].openProfiler--;

				while (samples[inner].valid)
				{
					if (samples[inner].openProfiler > 0)
					{
						numParants++;
						if (parent < 0 || samples[inner].startTime >= samples[parent].startTime)
						{
							parent = inner;
						}
					}
					inner++;
				}

				samples[i].numParents = numParants;
				if (parent >= 0)
				{
					samples[parent].childrenSampleTime += endTime - samples[i].startTime;
				}
				samples[i].accumulator += endTime - samples[i].startTime;
				return;
			}
			i++;
		}
	}

	void Profiler::ProfileDumpOutputToBuffer(double(*time)(), const float(*deltaTime)())
	{
		unsigned index = 0;
		unsigned i = 0;
		endProfile = static_cast<float>(time());
		std::string str;

		EC_LOG(INFO,"  Ave :   Min :   Max:   # : Profile Name", index++);
		//str.append("  Ave :   Min :   Max:   # : Profile Name\n");
		EC_LOG(INFO, "-----------------------------------------", index++);
		//str.append("-----------------------------------------\n");

		while (i < NUM_PROFILE_SAMPLES && samples[i].valid)
		{
			unsigned indent = 0;
			float sampleTime, percentTime, aveTime, minTime, maxTime;
			char line[256], name[256], indentedName[256];
			char ave[16], min[16], max[16], num[16];

			if (samples[i].openProfiler < 0)
			{
				Assert(0, "Profiler::ProfileBegin: ProfileEnd() called without a ProfileBegin()");
			}
			else if (samples[i].openProfiler > 0)
			{
				Assert(0, "Profiler::ProfileBegin: ProfileBegin() called without a ProfileEnd()");
			}
			sampleTime = samples[i].accumulator - samples[i].childrenSampleTime;
			percentTime = (sampleTime / (endProfile - startProfile)) * 100.f;
			aveTime = minTime = maxTime = percentTime;

			StoreProfileInHistory(samples[i].name, percentTime, time, deltaTime);
			GetProfileFromHistory(samples[i].name, aveTime, minTime, maxTime);

			sprintf_s(ave, "%3.1f", aveTime);
			sprintf_s(min, "%3.1f", minTime);
			sprintf_s(max, "%3.1f", maxTime);
			sprintf_s(num, "%d", samples[i].profileInstances);

			strcpy_s(indentedName, samples[i].name);
			for (indent = 0; indent < samples[i].numParents; ++indent)
			{
				sprintf_s(name, "   %s", indentedName);
				strcpy_s(indentedName, name);
			}

			sprintf_s(line, "%5s : %5s : %5s : %3s : %s", ave, min, max, num, indentedName);
			EC_LOG(INFO, line, index++);
			i++;
		}

		for (size_t i = 0; i < NUM_PROFILE_SAMPLES; ++i)
		{
			samples[i].valid = false;
		}
		startProfile = static_cast<float>(time());
		EC_LOG(INFO, "-----------------------------------------", index++);
	}
	void Profiler::StoreProfileInHistory(const char * name, float percent, double(*time)(), const float(*deltaTime)())
	{
		unsigned i = 0;
		float oldRatio;
		float newRatio = 0.8f * deltaTime();
		if (newRatio > 1.0f)
		{
			newRatio = 1.0f;
		}
		oldRatio = 1.0f - newRatio;

		while (i < NUM_PROFILE_SAMPLES && history[i].valid)
		{
			if (std::strcmp(history[i].name, name) == 0)
			{
				history[i].ave = (history[i].ave * oldRatio) + (percent * newRatio);
				if (percent < history[i].min)
				{
					history[i].min = percent;
				}
				else
				{
					history[i].min = (history[i].min * oldRatio) + (percent * newRatio);
				}

				if (percent > history[i].max)
				{
					history[i].max = percent;
				}
				else
				{
					history[i].max = (history[i].max * oldRatio) + (percent * newRatio);
				}
				return;
			}
			i++;
		}

		if (i < NUM_PROFILE_SAMPLES)
		{
			strcpy_s(history[i].name, name);
			history[i].valid = true;
			history[i].ave = history[i].min = history[i].max = percent;
		}
		else
		{
			Assert(0, "Profiler::ProfileBegin: Exceeded Max available profile samples!");
		}
	}
	void Profiler::GetProfileFromHistory(const char *name , float & ave, float & min, float & max)
	{
		unsigned i = 0;
		while (i < NUM_PROFILE_SAMPLES && history[i].valid)
		{
			if (std::strcmp(history[i].name, name) == 0)
			{
				ave = history[i].ave;
				min = history[i].min;
				max = history[i].max;
				return;
			}
			i++;
		}
		ave = min = max = 0.0f;
	}
}
