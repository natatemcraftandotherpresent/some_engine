#pragma once
#ifndef ENGINE_CORE_ADDONS_PROFILER
#define ENGINE_CORE_ADDONS_PROFILER

#include "PCH.h"
#include "Addons/Singleton.h"
namespace EngineCore
{
	struct ProfileSample
	{
		bool valid;
		unsigned profileInstances;
		int openProfiler;
		char name[256];
		float startTime;
		float accumulator;
		float childrenSampleTime;
		unsigned numParents;
	};

	struct ProfileSampleHistory
	{
		bool valid;
		char name[256];
		float ave;
		float min;
		float max;
	};
#define NUM_PROFILE_SAMPLES 50
	class Profiler : public Singleton<Profiler>
	{
	public:
		void Init(float time);
		void ProfileBegin(const char* name, double(*time)());
		void ProfileEnd(const char* name, double (*time)());
		void ProfileDumpOutputToBuffer(double(*time)(), const float(*deltaTime)());

	private:
		void StoreProfileInHistory(const char*, float percent, double(*time)(), const float (*deltaTime)());
		void GetProfileFromHistory(const char*, float& ave, float& min, float& max);
	private:
		ProfileSample samples[NUM_PROFILE_SAMPLES];
		ProfileSampleHistory history[NUM_PROFILE_SAMPLES];
		float startProfile;
		float endProfile;
	};
}
#endif