#include "PCH.h"
#include "Bitboard.h"

Bitboard::Bitboard()
{
	bitboard = 0;
}

void Bitboard::AddBit(unsigned char position)
{
	if (position > (sizeof(bitboard) * 8) && position <= 0)
	{
		return;
	}
	bitboard |= 1l << position;
	return;
}

bool Bitboard::IsBit(unsigned char position) const
{
	if (position > sizeof(position) * 8 && position <= 0)
	{
		return false;
	}

	long mask = 1L << position;

	return (bitboard & mask) != 0;
}

void Bitboard::RemoveBit(unsigned char position)
{
	if (position > sizeof(bitboard) * 8 && position <= 0)
	{
		return;
	}
	bitboard ^= 1l << position;
}
