#pragma once

#include <corecrt.h>
#include <stdint.h>
#include <limits>
#include <cassert>
#include <Windows.h>

#include <array>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <sstream>